# SD Card File Formats

The WaveBoy must have a FAT32 formatted SD card installed to function. The card
stores the patches as well as the module config (such as the tuning adjusment).
The config files must be in the root directory of the SD card and are called
`patches.txt` and `config.txt` respectively.

The textfiles are newline terminated (Unix style, not DOS style)

## patches.txt format

WaveBoy currently supports 16 patches which are all contained within this file.
At boot time, WaveBoy reads the file into its built-in RAM. Changes to patches
are not written to the SD card until "Save All Patches" is selected from the menu.

Patches can be editing on the WaveBoy as noted above, but they can also be edited
manually by opening the textfile into a text editor or programmatically via a 
script that can generate the patches.txt in the proper format.

Each patch in patches.txt is the following:

```
Patch Name (padded to 16 characters)
Default step sequencer setting (F = Forward, B = Backword, P = Ping-Pong, R=Random)
Next 16 rows: string of 32 *lowercase* hex values which compose the wave.
```

For example:

```
DirtySaw
F
ffeeddccbbaa99887766554433221100
ffee6dccbbaa99887766554433221100
ffee6dc4bbaa99887766554433221100
ffee6dc4bbaa99187766554433221100
ffee6dc4bbaa99187d66554433221100
ffee6dc4bbaa99187d6655c433221100
ffee6dc4bbaa99187d6655cf33221100
ffee6dc4bbaa99187d6655cf332e1100
afee6dc4bbaa99187d6655cf332e1100
afee6dc4bbea99187d6655cf332e1100
afee6dc4bbea99187d6655cf332e1106
afee6d14bbea99187d6655cf332e1106
afee6d14bbea99187d6655cf332e11f6
afee6d14bbea9918726655cf332e11f6
a2ee6d14bbea9918726655cf332e11f6
a2ee6d14bbea4918726655cf332e11f6
```

Note that the wave output is bipolar. That means the center would be between
values 8 and 9.

The firmware expects that there are 16 patches in the file.