# WaveBoy

4-bit/32-Slot Wavetable Oscillator for Eurorack inspired by wave output of the
Nintendo GameBoy and designed by Tim Soderstrom ([BitByBit Synths](https://www.bitbybitsynths.com/))
It uses a variable clocked DAC to produce the audio with the speed of the DAC \
corresponding to the pitch. This provides a chippy and aliased output.

The aliasing is by design given it mimicks the lofi chiptune sounds of the
GameBoy and there are plenty of high quality alias-free wavetable modules
already available. That's not what this is. Of note the aliasing is a 
great way to excite the Doepfer A-124 (Wasp) filter!

It also implements 16 frames within a patch (similar to LSDJ) which can be
modulated via CV and has a step sequencer with its own trigger and output 
which operates over the wave.

For more information, check the [manual](/manual/docs/index.md), also available
on the web [here](https://waveboy.bitbybitsynths.com).

