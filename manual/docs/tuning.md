# Tuning and Calibration

First I apologize for the lack of example images for the UI. This is brand new and still
may be subject to tweaks and things.

The assembled versions of WaveBoy will come pre-calibrated. DIY versions or updated firmware 
may change the tuning which may require adjustments. This can be done by going to the Tuning
option under the Config menu.

As of 0.54, each note can be individually tuned. This was to allow for precise tuning even when
there are variances within each note across the ADC range. Future WaveBoy PCB updates might not
require this level of precision, but the approach is flexible, if a bit tedious initially.

While in the tuning module, the pitch knob is deactivated. This is to avoid it from having
an influence on the tuning values so WaveBoy can be tuned to the raw input from the pitch
input.

## The Basics

The larger letters on the display show the current selected note and either the steps per
note (SPN) or ADC maximum for that particular note. To switch from SPN to ADC, press the 
left knob.

The right knob adjusts the current value up or down, and the left selects the note.

The bottom 3 lines are information about the _incomming_ note being read from V/Oct.

  * Raw ADC is the equivalent value from the analog-to-digital converter.
  * Est. Pitch is the calculated pitch value (this should closely match what your tuner says)
  * Note is the equivalent MIDI note value for the calculated pitch.

## Tuning

 1. Pick a note on the WaveBoy. Then play that same note into V/Oct using a method of choice.
 2. Pull up the ADC value for the note (press left knob) to make sure it is a at or preferably
    a bit above the Raw ADC value. If it's very high or low, you might not be playing the correct
    note into the WaveBoy. Use the right knob to adjust the value.
 3. Pull up the Steps Per Note (SPN) for the note (press left knob). This is how you tune the
    note so that it matches as close as it can the ideal value for said note. You should use
    a tuner for this as well as the Est. Pitch. Use the right knob to adjust up or down.

Be aware the lowest (D-2) and highest (C#7) notes can be a little sketchy to tune. If
you can't get them quite right, don't worry about it too much as these sit on the very
extreme of the ADC values and are where the ADC is pretty non-linear.

## Saving Your Settings

 You can save your settings by pressing the right knob which will save your values to the
 `tuning.txt` file on the SD card. You can also go to the Config menu and save from there
 which will save all settings (tunings included).

