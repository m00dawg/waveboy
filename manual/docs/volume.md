# Volume Envelopes

WaveBoy has a number of internal volume envelopes which can be used in replacement
or in addition to an external envelope and VCA. Due to the nature of how the volume 
is calculated, they have a more chiptune sound as compared to using an analog VCA.

While currently only volume envelopes are supported, the envelopes themselves
were written so that they may eventually be used for other modulation destinations
(e.g. pitch, or using the Step output for using envelopes for other modules).

## Selecting an Envelope

Envelopes currently operate over wave and noise modes.

To configure a volume envelope, from the Main Menu, navigate to Volume Env by
turning the left knob. Press the right knob when on Volume Env to bring up
the Volume Env menu. Here you can use the left knob to select the desired
envelope, which will activate immediately. To configure the selected 
envelope's parameters, press the right knob. This will bring up the
menu for that envelope.

To configure envelope settings, rotate the left knob to select the desired
setting and rotate right knob to adjust that setting.

To return to the volume menu, press the left knob.

## Triggering the Envelopes

The Gate input is used to trigger an envelope and, where relevant, can
be used for sustain.

Note that the gate will continue to be used for other functions while being
used for envelopes.

## Speed and Multiplier

Currently all envelopes have one or more speed parameters. This controls how
fast the envelope changes the volume. Higher values mean faster speeds.

Additionally there is a Multiplier value. Higher values also increase speed
at the cost of granularity. This is how you can create super snappy envelopes.

## Envelopes

### None

This is the default mode. There are no internal adjustments to volume such that
any volume modulation will need to be done externally (e.g. via an envelope and VCA).

### Decay

A simple linear decay envelope with only a speed parameter. When triggered
it will go from full volume to zero and stay there until re-triggered.

### AD

A simple linear Attack/Delay envelope with separate attack and decay speeds.

### ADSR Envelope

A full Attack/Decay/Sustain/Release envelope.

## Implementation Details

This information isn't needed just to use the envelopes, but is here for the curious.

The envelopes internally use a 16-bit range. This is so they can eventually be used
for other sources (e.g. pitch). The volume range is only 8-bit. The envelopes values
are logically shifted down to 8-bits as part of the volume interrupt routing.

These 8-bits are then used in a log lookup table which returns an 11-bit value.
This is to smooth out the volume since volume is logarithmic.

It is 11-bits vs 12-bits (which is the actual bit-depth of the DAC) because the 
output is bipolar. The attenuated (inverse) value of the volume is subtracted 
from positive parts of the wave and added to negative parts (pushing either side 
towards the center).

The weight parameter for the envelopes increases the count step when the volume
interrupt fires. Since the envelopes are 16-bit internally, snappy envelopes 
would require a very fast timer and would impact the range of slower envelopes.
The weight thus allows the user to decide and keeps the timer period within a
reasonable range.