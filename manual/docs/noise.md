# Noise Mode

![Noise](images/Noise.jpg "Noise")

WaveBoy has several noise generators available, including an approximation of
the GameBoy's buzzy LFSR noise mode. To activate noise, simply select "Noise"
from the main menu (press the left encoder if in the wave view and then turn
it clockwise until Noise is highlighted, then press the encoder again).

As soon as Noise is selected, WaveBoy switches to outputting noise on the
audio out. Likewise, the step output will now use the current noise value
at each trigger pulse.

All noise types currently implement a single 16-bit noise register that
is updated with each update to the DAC via the next() function call. This
function call manipulates the noise register in various ways.

To exit noise mode, press and hold the right encoder to go back to the
main menu. WaveBoy will continue to output noise until a Wave function
is selected.

## Controls

The noise frequency is controlled via V/Oct and the Tune knob and runs
at the same audio rates as the Wave output (namely 32 * pitch). For some 
noise types, this results in changing the perceived pitch of the noise; 
and for others is controls the speed of noise events.

Some noise types also use the CV input to modulate a parameter. The right
encoder knob can also be used to modulate a parameter as well.

Pressing the left knob will display current CV/knob values for a small period of time.
To prevent the display updates causing audio artifacts, these values are not updated
to the screen automatically. This allows using fast changing CV values.

Press and holding the left know will return to the main menu.

Pressing the right knob **or** sending the gate input high will reset the current selected noise. 
This has varying meaning depending on the noise type.

## Noise Types

### GameBoy

Derived from the explanation [here](https://aselker.github.io/gameboy-sound-chip/),
this noise type approximates the "buzzy" noise available on the GameBoy.

Pressing the right knob **or** sending the gate input high resets the LFSR seed which 
can result in different sounds.

### Modern

This uses a modern LFSR derived from [here](https://en.wikipedia.org/wiki/Linear-feedback_shift_register).

Pressing the right knob resets the LFSR seed which can result in slightly different
sounds.

### RNG

Simply uses the `random()` Arduino function.

Pressing the right knob resets the random seed. The seed comes from reading an unused pin
on the microcontroller.

### Bitflip

On each update, randomly picks a bit to flip (XOR) within the 16-bit register.

Pressing the right knob randomizes the noise register.

### Bitshift

On each update, shifts the bits to the left and randomly sets the least significant bit
to 0 or 1.

Pressing the right knob resets the random seed. The seed comes from reading an unused pin
on the microcontroller. It also randomizes the noise register.

### Drip

Sort of like drops of rain. On each update, there is a randomized change to drip onto
the noise register. This XORs the existing bit and then "splashes" to the adjoining
bits.

The CV sets the max splash range and the right encoder knob adjusts the drip chance.

Pressing the right knob resets the register to all zeroes.