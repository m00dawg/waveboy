# Digital Audio Conversion

WaveBoy uses variable clocking to the DAC, meaning there is not a fixed sample rate
like one might be used to. Instead of a common audio sample rate of, say, 44.1kHz,
the sample rate is a function of the pitch.

In wave mode, sample-rate is `32 * pitch` since the waves are made up of 32
samples. In Noise mode, the same sample-rate is used to help offer a wide range
of noise frequencies. In Sampler mode, it can vary based on the pitch multiplier
but the principle is the same.

This is similar to how the GameBoy worked as well as at least some of the Amiga
line of computers. **With this method there is no interpolation.** As the output of 
WaveBoy is also DC-coupled, the result is aliasing - by design. This results in
a notably lo-fi "chiptune" sound.

Because of the aliasing by design, the [Doepfer Wasp](https://www.doepfer.de/A1242.htm)
filter is a great pairing to the WaveBoy. It can be used to filter out the aliasing 
of course, but the aliasing is also a great way to drive the resonant "growl" of the 
Wasp if desired.