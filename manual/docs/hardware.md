# Hardware

At the heart of WaveBoy is an Adafruit [ItsyBitsy M4 Express](https://www.adafruit.com/product/3800)
which features an ATSAMD51 microcontroller. This is similar to the Arduino Nano 33. 

There were many options available, including the Teensy (which powers 
[Ornament and Crime](https://ornament-and-cri.me/)), various Arduino boards, 
the Electrosmith Daisy, etc.

The ItsyBitsy was chosen for the size plus having two 12-bit DAC outputs as well as 2 12-bit ADCs. 
This reduces the total part count for WaveBoy by not having to use an external DACs or ADCs.
It also supports an SD card (which is used for configuration and patch storage).

The rest of the module is fairly standard to other Eurorack digital modules.

The analog outputs use a TL074 and output a bipolar 9Vpp signal.

The V/Oct and CV inputs use an MCP6002. V/Oct expects a 0-5V signal whereas the CV 
input works from -5 to 5V though currently only works over the positive voltage range.

The trigger input uses an NPN transistor.

Note that pin 13 on the ItsyBitsy is used by an onboard LED. Early version of WaveBoy
used this pin for the right encoder which required removing this LED. The current board
revision uses a different pin for the encoder and no longer has this problem.