# Todo

## Bugs

* Sampler
	- Add bitcrush mode 
	- Consider 16 to 12-bit?
* Drawing to the OLED display no longer seems to change pitch (due to the 
  sample rate only changing after a full wave at least in Wave mode), but
  now it can cause a portamento effect when there's a lot of updates to
  the display
	- It's going to probably either need a new library or it's just how
	  I2C works.
	- Either way, the default screen should be a static display probably
	  showing the mode/patch, vol env setting, among other things.
  	- Perhaps consider switching to ssd1306 library
	  (https://github.com/lexus2k/ssd1306)
* Leverage const where appropriate (e.g. env names)
	- Have to touch several places for that to work
	- May be worth using const vs #define for scoping as well

## Proposed Features Documentation

* [Tracker](proposed/tracker.md)

## Other Future Plans

While Wave and Noise modes are great, there is a lot more WaveBoy can do given
the microcontroller and platform. Here are a few ideas, noting that not all of 
these may make it into WaveBoy.

* Make the pitch knob range configurable 

* Wave Generator

	LSDJ has a built-in wave generator. It would be a nice quality of life 
	feature to have something similar on WaveBoy.

* Moar Noise!

	The noise feature is reasonably pluggable and there are many more optional noise
	modes that can be made available.

* 8-bit Mode

	Though WaveBoy is primarily focused on adopting the GameBoy/LSDJ wave channel,
	being able to switch to higher quality waveforms might be useful for some folks.
