# Wave Mode

In Wave mode, WaveBoy functions like the Wave channel in LSDJ (the GameBoy tracker).

Wave patches are located on the SD card under the `waves` directory. 

Each patch contains 16 waves. Each wave is made from `32 4-bit` samples.
Flipping between frames is done by way of the CV input. While less ideal for manually 
picking a frame, it allows for modulating frames using envelopes, LFOs, S&H, MIDI CCs 
(coming from a MIDI/CV device), etc.

Press the left button to bring up the main menu.

## Show Wave

This is currently a boring display. It was originally to show the current wave
and some other parameters, and looked very cool when changing the wave frames via
CV. The problem is the OLED display is very slow to update and it causes the pitch
to slew.

So for now the display just shows the current wave patch and step mode
(F for Forward; B for Backward; P for Ping-Pong; and R for Random). 

Press the right encoder to set the mode.

Press and hold the left encoder to go to the wave menu.

## Edit Wave

Here is where you can update the waveform of each frame. The CV input is disabled
allowing you to manually change frames. To do that, press the left knob so that
the number next to "Fr" on the top is highlighted. Turn the right encoder to
cycle through the frames.

The left knob increases or decreases the value of the wave at the current position.
To change positions, press the left knob until the number next to Fr is not highlighted.
In this mode, turning the right knob now adjusts the position.

## Load Patch

![Load Patch](images/LoadPatch.jpg "Load Patch")

Load patch is where you can load patches from the SD card. Selecting a patch, using 
the left encoder, immediately loads the patch. This allows you to preview the 
different patches quickly.

Press the left encoder to jump to the wave menu.
Press the right encoder to jump back to the wave view. 

## Save Patch

![Save Patch](images/SavePatch.jpg "Save Patch")

If you like the current set of frames, you can name and save them by going to Save patch.
Use the left encoder to set the currently-selected character (which appears as a 
zoomed in version on the bottom right of the screen). Use the right encoder to switch
to other character positions in the name.

To prevent accidental saving, you must *press and hold* the right encoder to save the patch.

Press the left encoder to jump back to the wave view.