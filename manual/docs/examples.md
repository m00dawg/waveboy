# Examples

So what does it sound like? Here's a list of several videos and demos with 
WaveBoy being used in various scenarios:

* [YouTube Playlist](https://www.youtube.com/watch?v=LCqC3Gd7GWg&list=PL1NEcis11RsRJNKMQUWcUPEiZY3QV18CN)
* [Soundcloud](https://soundcloud.com/victim-cache/evening-with-modular-waveboy-wasp-and-other-stuff)
