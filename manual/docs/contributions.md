# Contributions

I would like to personally thank the following folks who have helped provide feedback, testing, and suggestions
for WaveBoy:

  * Michael Allen
      * Firmware testing, feature feedback, documentation suggestions/contributions
  * Seth Simonton
      * SAR ADC calculation and simulation, PCB routing improvements, power improvements, DAC output improvements, general advice and suggestions

