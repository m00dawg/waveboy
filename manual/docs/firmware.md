# Updating the Firmware

## UF2

This is the preferred way of updating the firmware as it does not require installing
the Arduino IDE and is just a matter of copying a .uf2 file to the microcontroller via
USB.

  * Latest Firmware: [0.54](firmware/waveboy-0.54.zip)
    * Previous Firmware:
        * [0.53](firmware/waveboy-0.53.zip)
        * [0.52](firmware/waveboy-0.52.zip)
        * [0.40](firmware/waveboy-0.40.zip)
  * Latest SD Card Image: [sdcard.zip](firmware/sdcard.zip)

The Wave patch format has changed from the previous release (0.4). It is
recommended to download the latest image. You will need to manual adjust your patches
(sorry!) until a converter is written.

The tuning process has been changed in 0.54. It is recommended to download the latest
SD card image and at least grab the tuning.txt file. This file should give most users
a good all around tuning. See [tuning.md] for more information.

You will need a micro-USB cable and a computer running Mac, Windows, or Linux. You can
do this with or without Eurorack power to the WaveBoy. 

1. Download and unzip the firmware (available up above)
1. Plug in the micro-USB cable into the WaveBoy and your computer.
1. On the ItsyBitsy (that is the module that has the micro-USB cable), there 
is a small reset button. This is on the opposite side of the micro-USB. So if
looking at the back of the WaveBoy, it is on the right. Press this button **twice**.
1. An LED on the ItsyBitsy will turn red and then stay green. The ItsyBitsy
should now show up on your computer as a mounted drive (likely called
ITSMYBOOT).
1. Open up the root directory of the mounted drive and copy the .uf2 file
onto it.
	1. If using the Pre-Release hardware, use the `-pr.uf2` file. Otherwise use the normal one.
1. If you have the WaveBoy powered via Eurorack power, if the update succeeded, 
the ItsyBitsy will unmount, the WaveBoy will restart, and the device may remount.
1. If you have the WaveVBoy powered only via USB, it will unmount the volume but
may not remount it. Remove USB before connecting it back to Eurorack power to
make sure it properly resets itself.
1. If you missed the splash screen, press the reset button on the ItsyBitsy again to
restart it. You will see WAVEBOY displayed along with a version number right below it.
You will want to confirm it is the desired version.

## Using Arduino 

This has been tested with Arduino 2.3.2 but other versions may work. 

You will need to install the following libraries:

* Adafruit_DotStar
* Adafruit_GFX
* Adafruit_SSD1306
* SimpleRotary
* SdFat
* Filters

You will also need to install the Adafruit SAMD Boards by adding 
`https://adafruit.github.io/arduino-board-index/package_adafruit_index.json` 
to the Additional Boards Manager URLs (under 
Preferences). Once that is there you can go to `Tools -> Board -> Boards Manager...` and searching
for Adafruit. Once installed, set your Board to be `Adafruit ItsyBitsy M4 (SAMD51)`.

The MCU has a micro-USB connection which can be found by removing the module from your rack and 
turning it over. Plug in a micro-USB cable into that end, and the other into your computer. WaveBoy
can be flashed with or without power from your rack.

Once the USB cable is connected, go to `Tools` and check that Port is not greyed out. On Linux,
the ItsyBitsy will show up as `/dev/ttyACM0` usually.

If you haven't yet done so, you will need to checkout the WaveBoy [repository](https://gitlab.com/m00dawg/waveboy). 
Then go to `File -> Open` and navigate to wherever you place the repository on your computer, 
then to `firmware`, `waveboy`, and then open up `waveboy.ino`. If all is well, click on the circular
checkbox on the top left of the IDE. If you see no errors, you can then flash the firmware to WaveBoy 
by clicking the `->` arrow (next to the checkbox).

## Changes

  * 0.53: Volume Envelopes, Sampler Improvements, Trigger for Some Noise Modes
  * 0.52: Sampler, Noise Modes, New Patch Format for Wave Mode
  * 0.40: Initial pre-release firmware