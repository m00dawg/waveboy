# Wave Manipulators

## Overview

This is currently an idea-board for changing how on-module wave editing works.

Instead of using the current wave view to edit waves using the knobs, a separate "Edit Wave" module will be created.

In Edit mode, the external CV will be disabled, and the wave frames will be manually set via knob interactions.
Pressing the left knob, for instance, will cycle from using the knobs to draw the wave to the frame number. This
means waves can be edited without needing to mess with CV.

In this mode, the audio will respond accordingly (so changing the wave frame will change the output to that frame).

Pressing the right knob opens up the Manipulators module.

## Manipulators

These apply operations to the waveform. The initial UI is a selection of available manipulators which are selected
via the left knob. The right not enables that manipulate and takes you to a similar wave edit view, only in this mode,
the left and right knobs adjust the manipulator parameters to see the results.

Audio will reflect the changes but the changes are not applied to the wave until pressing the right button to "commit"
the changes. This is because the unmodified version of the wave needs to be kept around to manipulate it until the
desired state.

Initially the manipulators will only work on the current wave frame.

Long Press Left Knob will go back to the normal Wave Edit view
Long Press Right Knob will apply the changes.

### Initial Planned Manipulators

#### Clip

Set the "volume" of the wave up into clipping.

Left Knob: Nothing
Right Knob: Decrease/Increasing amplification

#### Wrap

Set the "volume" of the wave but wrap instead of clip.

Left Knob: Nothing
Right Knob: Decrease/Increasing amplification

#### Wiggle

Apply random volume fluctuations to each wave step.

Left Knob: Nothing
Right Knob: Decrease/Increasing strength of wiggle

#### Shift

Shifts the entire wave left/right or up/down. 

Note shifting up/down could induce a DC bias and shifting left/right without otherwise
changing anything would just adjust the wave phase. Thus this manipulator would work
better when also using another manipulator on the wave.

Left Knob: Shift left/right
Right Knob: Shift up/down

#### Randomize

Generate a random wave

Left Knob: Nothing
Right Knob: Re-roll random seed

### More Complex Manipulators

These require more maths and will be tackled after the main feature is working.

#### Wave-Fold

Fold the wave (add harmonics)

#### Low-Pass Filter

As it sounds

#### Hi-Pass Filter

As it sounds

## Generators

Manipulators are intended to work on a single wave frame at a time. This gives the user
control over how the frames may morph, but still requires some tedium as that has to be
done across the 16 frames. A copy/paste feature can help with this, but it's still 
somewhat tedious.

Generators would take the idea of Manipulators and apply them across an entire patch
(so all 16 frames). The idea is taking a manipulator and changing it over time.
So in the case of Wrap, the wrap value might start at 0, and then go up to say 15.
This would generate a mild to wild style sequence of frames.

Related is a way to generate a starting wave-form (sine, saw, tri, square, etc.)
from which to work from.