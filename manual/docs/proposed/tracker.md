# Micro Tracker (Proposed)

**This is currently in the planning stage and is subject to change.**

There are two main ideas / use-cases for a small simple tracker for WaveBoy:

## Overview

A "song" would be composed of up to 16 patterns, each 64 (x40) in length. The CV input replaces
the conventional "order list" where the value of the next pattern is selected via CV, much like
picking a wave in Wave mode. The pattern advances with each trigger from the Gate input.

There are three output modes planned

### Internal Tracker Mode

In this mode, WaveBoy functions more like LSDJ where the tracker is manipulating the audio output
of WaveBoy itself. The main audio out is the Waveform from a patch selected from the song's metadata.

### 90's Tracker Mode

In this mode, WaveBoy functions like a super simple single channel of a MOD tracker, where it uses
samples from one of the Sampler patches (selected as part of the song's metadata).

### External Tracker Mode

In this mode, the Audio out becomes a V/Oct outputting pitch with the step output being used as a
gate, say to feed an envelope; or CV to feed a VCA.

## Pattern Layout

Two options are currently being considered. A simple mode, with only a note/octave and effect column per row;
and a more complete tracker mode that has note/octave, instrument, volume, and effect. The sequencer mode
does not really need all these columns.