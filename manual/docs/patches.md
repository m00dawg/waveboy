# Patches

Currently, the Wave and Sample modes are the only modes that store patch data. 

## Waves

Wave patches are stores in the `waves` sub-directory off the SD card. The filename is the 
name of the patch and should be 16 characters or less in length.

Each patch file contains the following:

```
step sequencer method (F, B, P, R)
wave-1
...
wave-16
```

Where `wave-#` is a sequence of 32 hexadecimal characters (0-9,a-f) indicating the value of the sample
at that position of the wave. 0 is the lowest, f is the highest. It should be noted that WaveBoy outputs a 
bi-polar signal, so the center is in-between values 7 and 8.

Here is a full example:

```       
P
ffeeddccbbaa99887766554433221100
ffee6dccbbaa99887766554433221100
ffee6dc4bbaa99887766554433221100
ffee6dc4bbaa99187766554433221100
ffee6dc4bbaa99187d66554433221100
ffee6dc4bbaa99187d6655c433221100
ffee6dc4bbaa99187d6655cf33221100
ffee6dc4bbaa99187d6655cf332e1100
afee6dc4bbaa99187d6655cf332e1100
afee6dc4bbea99187d6655cf332e1100
afee6dc4bbea99187d6655cf332e1106
afee6d14bbea99187d6655cf332e1106
afee6d14bbea99187d6655cf332e11f6
afee6d14bbea9918726655cf332e11f6
a2ee6d14bbea9918726655cf332e11f6
a2ee6d14bbea4918726655cf332e11f6
```

Patches can be edited on WaveBoy directly using the knobs while in the Wave viewing mode
(see [Wave Mode](wave.md)). In the project source there is a python script that generates the 
patches that come on the SD card by default. This can be edited to create different patches, or
patches.txt can be edited using any text-editor. Just be sure to follow the file format specification 
as there is currently no error checking on the patches.txt file.