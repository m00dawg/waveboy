# WaveBoy Manual

WaveBoy is an 8HP Eurorack module that implements the behavior of the wave channel
on the Nintendo GameBoy, specifically the behavior found in the LSDJ (one of the
GameBoy music trackers). It also has a noise mode which is able to generate a
number of bitwise noise algorithms and has a step sequencer which can be used
in wave, noise or sampler modes.

**WaveBoy is finally available for purchase!**
If you want to buy one, you can do so [here](https://www.bitbybitsynths.com/shop/waveboy/).
Doing so supports the project and helps fund future modules and things.

![Waveboy Front](images/WaveBoy.jpg "Waveboy Front")

It is available for purchase [here](https://www.bitbybitsynths.com/shop/waveboy/).
Doing so helps the project and helps fund future features, modules, etc.

In wave mode, each patch contains 16 waves ("frames") with each wave being a 
`4-bit / 32-step` cyclic waveform. You can thus think of Wave mode as a very
basic wavetable module.

What makes WaveBoy a bit unique is that it does not use a fixed sampling rate 
for the DAC. Instead it adopts the GameBoy's solution of clocking the DAC at 
the desired pitch. In wave mode, the resulting frequency is the desired pitch
multiplied by 32, since the waveform has 32 steps. In noise mode, it is the
speed that the noise value is updated at. There is no anti-aliasing (by design).

For inputs, WaveBoy has V/Oct of course, as well as additional CV and gate
inputs. The gate is used to advance the step sequencer in both Wave and Noise modes,
handles looping (if enabled) for the sampler; and is used for the volume envelopes.

The CV is for selecting the wave-frame in wave mode; and for modulating the noise
algorithms (depending on which one is used).

There are two DC-coupled outputs. The main out is for audio. The second
output is for the step sequencer. In the future it may also be used for outputting
another wave (e.g. for detune or for a sub-osc, etc.), envelopes, etc.

The WaveBoy firmware is open source. The project is hosted on Gitlab
[here](https://gitlab.com/m00dawg/waveboy).