# Sampler

## Overview

The sampler (sample player) allows from playing unsigned mono 8-bit raw PCM files off the SD card.
**It is currently an alpha level feature and subject to lots of changes.** It first appeared in 
WaveBoy 0.5.

Patches are contained within sub-directories underneath the samples directory on the SD card.
Their name is the directory name. Much like Wave-mode, each patch contains 16 samples which can 
be flipped through via the CV input. Each sample is an 8-bit mono WAV file that can contain up to 
64k of audio.

Each patch can be selected by turning the left encoder. The highlighted patch is the one immediately used.
To exit Sampler mode, press the left encoder knob.

There are three loop modes: One-shot (no loop), forward, and ping-pong. 

The trigger/gate input is used to start and stop playback. When the gate is high, playback
will start. When low, playback stops if the loop mode is One-shot. When using a looping mode, 
going low will let the sample "ring out" (**not yet implemented**).

If looping is enabled, the sample will loop over
the start and end loop points while the gate is high. In one-shot mode, the sample will play
to completion and not retrigger.

## Patch Details

Each patch lives in a sub-directory under the samples directory on the SD card. The name
of the patch is the name of the directory. Under each directory there are 17 files:

* `patch.txt`: Contains metadata for each sample within the patch
* `[0-9,a-f].wav`: An 8-bit mono WAV file

### patch.txt

This is a simple text file which contains additional information for each sample.
Each sample block is as follows:

```
Sample Name (Up to 16 characters)
Sample Rate Multiplier
Loop Mode
Loop Start Position
Loop End Position
```

For example:

```
String          
128
F
4471
27474
```

#### Name

The name of the sample which is displayed in the UI.

#### Sample Rate Multiplier

Because the main audio is coming from a variable clocked DAC, the pitch range of the
sample is determined by the V/Oct and pitch knob. Without a multiplier, the range
may be too low depending on the native sample-rate of the sample.

If 0, the default value is used (128).

#### Loop Mode

There are currently 3 loop options planned:

* `O`: One-Shot (no loop)
* `F`: Forward
* `P`: Ping-Pong (**Not yet implemented**)

The loop positions are in bytes. Since only 8-bit / mono samples are supported, 
this would be the same as the sample position. The position values can be found using
a tool such as Audacity/Tenacity.

#### Loop Start Position

The byte-position for where to start looping. Useful for samples which have a one-shot
component, such as a plucked string, where you may want to loop the stringy part but
not the pluck.

#### Loop Stop Position

The byte position for where to end the loop.

## Future Plans

These are not guaranteed but some ideas being considered

* Samples longer than 64k (by buffering data off the SD card)
* Using Step out for stereo samples