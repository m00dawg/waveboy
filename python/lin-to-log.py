#!/usr/bin/env python3

import math

print("uint16_t volumeMap[256] = {")

print("0, ", end='')
count = 1
for i in range(0,255):
    print(round(math.exp(math.log2(i+1)) * 0.6906207827260459), end='')
    if i < 254:
        print(', ', end='')
    count += 1
    if(count == 8):
        print("")
        count = 0
print("};")




#for i in range(0,256):
#    value = i * 256
#    print(round(math.exp(math.log2(value+1)) * 0.007416703267701953))

