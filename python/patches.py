#!/usr/bin/env python3
import numpy
import os

wavesteps = 32
waverange = 15
frames = 16
stringLength = 16

saw = [0xf,0xf,0xe,0xe,0xd,0xd,0xc,0xc,0xb,0xb,0xa,0xa,0x9,0x9,0x8,0x8,
				0x7,0x7,0x6,0x6,0x5,0x5,0x4,0x4,0x3,0x3,0x2,0x2,0x1,0x1,0x0,0x0]
triangle = [0,  1,  2,  3,  4,  5,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
						15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5,  4,  3,  2,  1,  0]
sine = []

# Generate Sine
for i in range(0, wavesteps):
	degree = (360 / 32 * i * 2) - 1
	float_value = numpy.sin(degree * numpy.pi / 360)
	value = round((waverange * float_value / 2) + 8)
	sine.append(value)

#np.sin(angle * np.pi / 360. )
#np.sin(np.array((22*1, 22*2, 22*3, 22*4, 22*5, 22*6, 22*7, 22*8, 22*9, 22*10, 22*11, 22*12, 22*13, 22*14, 22*15, 22*16)) * np.pi / 360. )
os.chdir('waves')

# Handmade
f = open('Dirty Saw', 'w', encoding="latin1")
f.write("""P
ffeeddccbbaa99887766554433221100
ffee6dccbbaa99887766554433221100
ffee6dc4bbaa99887766554433221100
ffee6dc4bbaa99187766554433221100
ffee6dc4bbaa99187d66554433221100
ffee6dc4bbaa99187d6655c433221100
ffee6dc4bbaa99187d6655cf33221100
ffee6dc4bbaa99187d6655cf332e1100
afee6dc4bbaa99187d6655cf332e1100
afee6dc4bbea99187d6655cf332e1100
afee6dc4bbea99187d6655cf332e1106
afee6d14bbea99187d6655cf332e1106
afee6d14bbea99187d6655cf332e11f6
afee6d14bbea9918726655cf332e11f6
a2ee6d14bbea9918726655cf332e11f6
a2ee6d14bbea4918726655cf332e11f6
""")
f.close


f = open('GB Squares', 'w', encoding="latin1")
f.write("""F
ffffffff000000000000000000000000
ffffffff000000000000000000000000
ffffffff000000000000000000000000
ffffffff000000000000000000000000
ffffffffffffffff0000000000000000
ffffffffffffffff0000000000000000
ffffffffffffffff0000000000000000
ffffffffffffffff0000000000000000
ffffffffffffffffffffffff00000000
ffffffffffffffffffffffff00000000
ffffffffffffffffffffffff00000000
ffffffffffffffffffffffff00000000
ffffffffffffffff0000000000000000
ffffffffffffffff0000000000000000
ffffffffffffffff0000000000000000
ffffffffffffffff0000000000000000
""")
f.close()

f = open('Squares', 'w', encoding="latin1")
f.write("""F
f0000000000000000000000000000000
ff000000000000000000000000000000
fff00000000000000000000000000000
ffff0000000000000000000000000000
fffff000000000000000000000000000
ffffff00000000000000000000000000
fffffff0000000000000000000000000
ffffffff000000000000000000000000
fffffffff00000000000000000000000
ffffffffff0000000000000000000000
fffffffffff000000000000000000000
ffffffffffff00000000000000000000
fffffffffffff0000000000000000000
ffffffffffffff000000000000000000
fffffffffffffff00000000000000000
ffffffffffffffff0000000000000000
""")
f.close()

# Sinedance (4)
f = open('Sinedance', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(sine)):
			value = round(sine[i])
			if value >= 8:
				result = abs(value - j) & waverange
			else:
				result = abs(value + j) & waverange
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Tridance (5)
f = open('Tridance', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(triangle)):
			value = round(triangle[i])
			if value >= 8:
				result = abs(value - j) & waverange
			else:
				result = abs(value + j) & waverange
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Sawrap (6)
f = open('Sawrap', 'w', encoding="latin1")
f.write("F\n")
for i in range(0, len(saw)):
	f.write(format(saw[i], '1x'))
f.write('\n')
for j in range(1,frames):
	for i in range(0, len(saw)):
			value = round(saw[i] * (j + 1) * 0.6)
			result = value & 15
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Sinewrap (7)
f = open('Sinewrap', 'w', encoding="latin1")
f.write("F\n")
for i in range(0, len(sine)):
	f.write(format(sine[i] & 15, '1x'))
f.write('\n')
for j in range(1,frames):
	for i in range(0, len(sine)):
			value = round(sine[i] + j)
			result = value & 15
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Triwrap (8)
f = open('Triwrap', 'w', encoding="latin1")
f.write("F\n")
for i in range(0, len(triangle)):
	f.write(format(triangle[i] & 15, '1x'))
f.write('\n')
for j in range(1,frames):
	for i in range(0, len(triangle)):
			value = round(triangle[i] + j)
			result = value & 15
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Sawdecay (9)
f = open('Sawdecay', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(saw)):
			value = round(saw[i] - (j*0.5))
			if(value < 0):
				result = 0
			else:
				result = value
			f.write(format(result, '1x'))
	f.write('\n')
f.close()


# These are bipolar which is why the weird abs and range split
# Sinedecay (10)
f = open('Sinedecay', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(sine)):
			value = round(sine[i])
			if i < 15:
				result = round((value - (j * .4)))
				if result < 8:
					result = 8
			else:
				result = round((value + (j * .4)))
				if result >= 8:
					result = 8
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Tridecay (11)
f = open('Tridecay', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(triangle)):
		result = round(triangle[i] - (j *.85))
		if result < 0:
			result = 0
		f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Sawamp (12)
f = open('Sawamp', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(saw)):
			value = round(saw[i] * ((j+4) * 0.25))
			if(value > 15):
				result = 15
			else:
				result = value
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# These are bipolar which is why the weird abs and range split
# Sineamp (13)
f = open('Sineamp', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(sine)):
			value = round(sine[i])
			if value > 7:
				result = round(value + (j * 0.5))
				if(result > 15):
					result = 15
			else:
				result = round(value - (j * 0.5))
				if(result < 0):
					result = 0
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Triamp (14)
f = open('Triamp', 'w', encoding="latin1")
f.write("F\n")
for j in range(0,frames):
	for i in range(0, len(triangle)):
			value = round(triangle[i])
			if value > 7:
				result = round(value + (j * 0.5))
				if(result > 15):
					result = 15
			else:
				result = round(value - (j * 0.5))
				if(result < 0):
					result = 0
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Sawxor (15)
f = open('Sawxor', 'w', encoding="latin1")
f.write("F\n")
for i in range(0, len(sine)):
	f.write(format(saw[i] & 15, '1x'))
f.write('\n')
for j in range(1,frames):
	for i in range(0, len(saw)):
			value = round(saw[i] + j)
			result = value ^ round(j * .25)
			result = result & 15
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Sinexor (16)
f = open('Sinexor', 'w', encoding="latin1")
f.write("F\n")
for i in range(0, len(sine)):
	f.write(format(sine[i] & 15, '1x'))
f.write('\n')
for j in range(1,frames):
	for i in range(0, len(sine)):
			value = round(sine[i] + j)
			result = value ^ round(j * .25)
			result = result & 15
			f.write(format(result, '1x'))
	f.write('\n')
f.close()

# Trixor (17)
f = open('Trixor', 'w', encoding="latin1")
f.write("F\n")
for i in range(0, len(triangle)):
	f.write(format(triangle[i] & 15, '1x'))
f.write('\n')
for j in range(1,frames):
	for i in range(0, len(sine)):
			value = round(triangle[i] + j)
			result = value ^ round(j * .25)
			result = result & 15
			f.write(format(result, '1x'))
	f.write('\n')
f.close()
