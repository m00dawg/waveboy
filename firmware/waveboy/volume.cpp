/* Volume Module */

/* Overall idea
 * TBD
 */ 
#include "volume.h"

Volume::Volume() {
	/*
	   This doesn't work as they can't be referenced properly within the
	   enelvopes array in other objects. We have to use new.
	   Or it seems to work using functions in Volume to access data within the array.
	   But that won't work when needing to pass arround refences to these into the UI
	   modules.

	   This is fine as these are long lived objects anyway.
	/*
	None none;
	Decay decay;
	this->none = &none;
	this->decay = &decay;
	this->envelopes[NONE_ENVELOPE] = &none;
 	this->envelopes[DECAY_ENVELOPE] = &decay;
	*/
	this->none = new None();
	this->decay = new Decay(VOLUME_ENV_DEFAULT_SPEED_MULTIPLIER);
	this->ad = new AD(VOLUME_ENV_DEFAULT_SPEED_MULTIPLIER);
	this->adsr = new ADSR(VOLUME_ENV_DEFAULT_SPEED_MULTIPLIER);
	this->envelopes[NONE_ENVELOPE] = none;
 	this->envelopes[DECAY_ENVELOPE] = decay;
	this->envelopes[AD_ENVELOPE] = ad;
	this->envelopes[ADSR_ENVELOPE] = adsr;
	speed = envelopes[selectedEnvelope]->getSpeed();
}

// Start/Restart envelope when gate goes high
void Volume::start() {
	volume = envelopes[selectedEnvelope]->start();
}

// Interrupt service routine. When the timer expires,
// this function fires and advances the volume depending
// on the selected envelope as well as setting the speed, 
// if necessary.
// Returns the speed which is useful in the outer ISR
// to reset the timer's interval
uint16_t Volume::isr() {
	// Reduce the envelope range, which is 16-bits, down to the
	// volume range, which is 8-bit.
	volume = envelopes[selectedEnvelope]->next() >> ENV_TO_VOL_RANGE;
	
	speed = envelopes[selectedEnvelope]->getSpeed();
	return speed;
}

// Apply lin to log conversion and get the 
// attenuated version of the volume
// (Basically the inverse)
uint16_t Volume::getAttenuation() {
	return VOLUME_ENVELOPE_MAX - volumeMap[volume];
}

// Gets the name of the active envelope
char* Volume::getEnvName() {
	return envelopes[selectedEnvelope]->getName();
}

// Get the index of the current envelope
// This is mostly for the Volume UI module.
byte Volume::getEnvIndex() {
	return selectedEnvelope;
}

// Switch to another envelope type.
// This is mostly for the Volume UI module.
void Volume::switchEnvelope(byte envelope) {
	selectedEnvelope = envelope;
	start();
}

// Set the value of the gate for the current envelope
// (This is typically called from the Gate object)
void Volume::setGate(bool state) {
	envelopes[selectedEnvelope]->setGate(state);
}