/* Sampler Module  */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include <SdFat.h>
#include "defines.h"
#include "functions.h"

#define SAMPLE_SIZE          65536   // 64k
//#define SAMPLE_SIZE          131070000   // 64k

#define SAMPLE_CENTER        127
#define DAC_RESOLUTION_SHIFT 4    // From 8 to 12 bits

// Struct for a sample metadata
// The actual sample data is loaded when the CV
// is updated since there isn't enough RAM to buffer everything
struct Sample {
  //char name[SAMPLE_NAME_LENGTH];
  String name;
  byte bitrate;
  uint16_t rateMultiplier;
  char loopMode;
  uint16_t loopStart;
  uint16_t loopEnd;
};

class Sampler {
  private:
    void reset();
    volatile uint16_t samplePosition = 0;
    Sample* sample;

  public:
    Sampler();

    // These from from the Sample struct but are copies
    // for quick reference
    byte bitrate;
    uint16_t rateMultiplier;
    char loopMode;
    uint16_t loopStart;
    uint16_t loopEnd;
    uint16_t sampleLength = 0;

    // Public for reading
    bool running = false;

    // Public becuase we need to save RAM and write to this thing fast.
    uint8_t sampleData[SAMPLE_SIZE];
    //volatile uint16_t sampleData[SAMPLE_SIZE];

    void init(Sample* sample);
    void start();
    void stop();
    uint16_t next();
};