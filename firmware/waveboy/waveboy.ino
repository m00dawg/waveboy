  // Title: WaveBoy
// Description: Implements a GameBoy WAVE style 4-bit/32 slot single wavetable and ohter chiptune features
// Hardware: Adafruit ItsyBitsy M4
// Author: Tim Soderstrom

/* 
 *  Links:
 * https://learn.adafruit.com/introducing-adafruit-itsybitsy-m4/pinouts
 * https://github.com/ManojBR105/Teensy-4.0_digital_parallel_read-write
 * https://www.falstad.com/circuit/index.html
 * https://www.inspireAUDIO_DACoustics.com/en/MIDI_note_numbers_and_center_frequencies
 */

/* Pins Used:
 * See defines.h
 */

// INCLUDES

#include <Arduino.h>
// Lib for turning off that awful RGB LED
#include <Adafruit_DotStar.h>
// Display Libs
#include <SPI.h>
#include <Wire.h>
//#include <U8g2lib.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// File Storage
#include <SdFat.h>

// Encoders & Buttons
#include <SimpleRotary.h>

// DEFINES
#include "defines.h"

// Timers
#include "src/timers/audioTimer.h"
#include "src/timers/volumeTimer.h"
#include "src/timers/updateTimer.h"

// Local Includes
#include "functions.h"
#include "patches.h"
#include "gate.h"
#include "audio.h"

// Envelopes
#include "src/envelopes/envelope.h"
#include "src/envelopes/none.h"
#include "src/envelopes/decay.h"
#include "src/envelopes/ad.h"
#include "src/envelopes/adsr.h"
#include "volume.h"

// Manipulators
#include "src/manipulators/manipulator.h"
#include "src/manipulators/clip.h"

// UI Modules
#include "src/ui/ui.h"
#include "src/ui/tuning.h"
#include "src/ui/config.h"
#include "src/ui/mainmenu.h"
#include "src/ui/volumeui.h"
#include "src/ui/blank.h"
#include "src/ui/sampler/samplerui.h"
#include "src/ui/sampler/samplepatch.h"
#include "src/ui/tracker.h"

// Wave UI Modules
#include "src/ui/wave/wavemenu.h"
#include "src/ui/wave/showwave.h"
#include "src/ui/wave/editwave.h"
#include "src/ui/wave/loadpatch.h"
#include "src/ui/wave/savepatch.h"
#include "src/ui/wave/manipulators.h"
#include "src/ui/wave/manipulate.h"

// Env UI Modules
#include "src/ui/envelopes/noneui.h"
#include "src/ui/envelopes/decayui.h"
#include "src/ui/envelopes/adui.h"
#include "src/ui/envelopes/adsrui.h"

// Noise
#include "src/noisetypes/noisetype.h"
#include "src/noisetypes/gbuzz.h"
#include "src/noisetypes/modern.h"
#include "src/noisetypes/rng.h"
#include "src/noisetypes/flip.h"
#include "src/noisetypes/shift.h"
#include "src/noisetypes/drip.h"
#include "src/ui/noise.h"

//#include "SAMD_AnalogCorrection.h"


// GLOBAL VARIABLES

// For serial output debugging mostly
int prevMillis = 0;

// Byte to track which module we are in
byte currentModuleId = SHOW_WAVE;
// Byte to track previous module (so we can go "backwards" in the UI)
byte previousModuleId = MAIN_MENU;

volatile bool update = false;

// OBJECTS

// Display
Adafruit_SSD1306 display(OLED_SCREEN_X, OLED_SCREEN_Y, &Wire, OLED_RESET, OLED_CLOCK);

// That horrendously bright LED
Adafruit_DotStar rgbled(1, RGB_LED_DATAPIN, RGB_LED_CLOCKPIN, DOTSTAR_BRG);

// Control Surface
// upDown is the left one, leftRight is the right one.
SimpleRotary upDownEncoder(ENCODER_UP, ENCODER_DOWN, BUTTON_MENU);
SimpleRotary leftRightEncoder(ENCODER_RIGHT, ENCODER_LEFT, BUTTON_BACK);

// SD Card
SdFs sd;

// Local Objects
// "The most vexing problem", defining object without params requires no parenthesis
// Pointer to store the current activated module
UI* currentModule;

// Timers
AudioTimer audioTimer;
VolumeTimer volumeTimer;
UpdateTimer updateTimer;

// Rest of all them objects
Patches patches(&sd);
Noise noise(&display, &upDownEncoder, &leftRightEncoder, &patches);
Sampler sampler;

Tracker tracker(&display, &upDownEncoder, &leftRightEncoder, &sd);
Volume volume;
Audio audio(&patches, &noise, &sampler, &volume);
Gate gate(&patches, &noise, &sampler, &volume);

// UI Objects
Config config(&display, &sd, &upDownEncoder, &leftRightEncoder, &patches, &audio);  // Config is both a module and does other stuff
Tuning tuning(&display, &sd, &upDownEncoder, &leftRightEncoder, &audio);
Blank blank(&display, &upDownEncoder, &leftRightEncoder);
MainMenu mainMenu(&display, &upDownEncoder, &leftRightEncoder);
WaveMenu waveMenu(&display, &upDownEncoder, &leftRightEncoder, &patches);
ShowWave showWave(&display, &upDownEncoder, &leftRightEncoder, &patches, &gate);
EditWave editWave(&display, &upDownEncoder, &leftRightEncoder, &patches, &gate);
LoadPatch loadPatch(&display, &upDownEncoder, &leftRightEncoder, &patches, &sd);
SavePatch savePatch(&display, &upDownEncoder, &leftRightEncoder, &patches, &sd);
VolumeUI volumeUI(&display, &upDownEncoder, &leftRightEncoder, &volume);
Manipulate manipulate(&display, &upDownEncoder, &leftRightEncoder, &patches);
Manipulators manipulators(&display, &upDownEncoder, &leftRightEncoder, &manipulate);
SamplePatch samplePatch(&display, &upDownEncoder, &leftRightEncoder, &sd, &sampler);
SamplerUI samplerUI(&display, &upDownEncoder, &leftRightEncoder, &sd, &samplePatch);

// Under a different name since another envelope could reuse the NoneUI but
//those settings would be specific to taht envelope
None* volNone = volume.none;
NoneUI volNoneUI(&display, &upDownEncoder, &leftRightEncoder, volNone);
Decay* volDecay = volume.decay;
DecayUI volDecayUI(&display, &upDownEncoder, &leftRightEncoder, volDecay);
AD* volAD = volume.ad;
ADUI volADUI(&display, &upDownEncoder, &leftRightEncoder, volAD);
ADSR* volADSR = volume.adsr;
ADSRUI volADSRUI(&display, &upDownEncoder, &leftRightEncoder, volADSR);

// Kinda like a jump table. The module IDs correspond to the index
// of this array.
// Thus when adding a module, it's super duper important to make sure it is
// in the right spot, with the right id else things go kind of bananas.
// This however keeps things efficient when switching modules and lets us
// have lots of UI modules (which makes each UI module simpler).
// See defines.h for index to module mapping.
UI* modules[NUM_UI_MODULES] = 
{ 
  &showWave,      //SHOW_WAVE
  &mainMenu,      //MAIN_MENU
  &waveMenu,      //WAVE_MENU
  &loadPatch,     //WAVE_LOAD_PATCH
  &savePatch,     //WAVE_SAVE_PATCH
  &blank,         //BLANK
  &config,        //CONFIG_MENU
  &noise,         //NOISE_MODULE
  &samplerUI,       //SAMPLER_MODULE
  &tracker,       //TRACKER_MODULE
  &volumeUI,      //VOLUME_MODULE
  &volNoneUI,     //VOLUME_NONE_MODULE
  &volDecayUI,    //VOLUME_DECAY_MODULE
  &volADUI,       //VOLUME_AD_MODULE
  &volADSRUI,     //VOLUME_ADSR_MODULE
  &editWave,      //EDIT_WAVE
  &tuning,        //TUNING_MODULE
  &manipulators,  //WAVE_MANIPULATORS
  &manipulate,     //MANIPULATE_WAVE
  &samplePatch    //SAMPLER_PATCH_MODULE
};

// FUNCTIONS
void setup() {
  Serial.begin(115200);
  delay(1000);

  Serial.print("Waveboy Version ");
  Serial.println(VERSION);

  //Serial.println("Setup Pins");

  // Pins
  //pinMode(VOCT, INPUT_PULLUP);
  pinMode(VOCT, INPUT);
  //pinMode(TUNE, INPUT_PULLUP);
  pinMode(TUNE, INPUT);
  pinMode(TRIGGER, INPUT_PULLUP);

  // Switch Encoders
  //leftRightEncoder.setDebounceDelay(ENCODER_DEBOUNCE);
  //leftRightEncoder.setErrorDelay(ENCODER_ERROR_DELAY);
  upDownEncoder.setDebounceDelay(ENCODER_DEBOUNCE);
  upDownEncoder.setErrorDelay(ENCODER_ERROR_DELAY);

  // Turn off the freakishly bright RGB LED
  rgbled.begin();
  rgbled.show();  // Initialize all pixels to 'off'

  // DAC Setup
  //Serial.println("Setup DAC");
  // Set DAC resolution
  analogWriteResolution(DAC_RESOLUTION);
  analogReadResolution(ADC_RESOLUTION);

  // ADC Setup
  initADC0();
  //initADC1();

  // Roll random seed
  randomSeed(readADC0(RANDOM_SOURCE));

  // Magic https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
  analogWrite(AUDIO_DAC, 128);
  analogWrite(STEP_DAC, 128);

  // Init SD Card
  sd.begin(SdSpiConfig(SD_CARD, SHARED_SPI, SD_SCK_MHZ(16)));

  // Setup the display
  //Serial.println("Init Display");
  initDisplay();
  delay(UI_PAUSE);

    // Load config and if it bombs, flag an error and spin
  // Typically this is because the SD card is missing (or blank)
  if (!config.loadConfig()) {
    display.clearDisplay();
    display.setTextColor(WHITE, BLACK);
    display.setTextSize(1);
    display.println("Missing SD Card");
    display.display();
    Serial.println("Missing SD Card");
    while (true) {}
  }
  Serial.println("Loaded Confing");


  // Initially set pitch
  /*
  while (audio.sampleRate == 0)
    audio.updatePitch();
  */


  //currentModule = &wave;
  // Setup default module
//  showWave.activate();
//  currentModule = &showWave;
//  currentModuleId = SHOW_WAVE;

  // Set sampler
  samplerUI.activate();
  currentModule = &samplerUI;
  currentModuleId = SAMPLER_MODULE;


  //config.activate();
  //audio.tuneKnob = false;
  //currentModule = &config;
  //currentModuleId = CONFIG_MENU;
  


  enableWaveStepCallback();
  enableGateInterrupt();
  enableVolumeInterrupt();
  //enableUpdateInterrupt();

  /*
  enableGateInterrupt();
  //disableVolumeInterrupt();
  enableSamplerCallback();
  currentModule = &sampler;
  currentModuleId = SAMPLER_MODULE;
  gate.mode = GATE_MODE_SAMPLER;
  sampler.activate();
*/

  //currentModule = &sampler;


  /*
  noise.activate();
  step.mode = STEP_MODE_NOISE;
  currentModule = &noise;
  currentModuleId = NOISE_MODULE;
  enableNoiseCallback();
  */

  // Debug
  /*
  currentModule = &savePatch;
  currentModuleId = SAVE_PATCH;
  currentModule->activate();
  */
  // Read V/Oct and Tune knob and set sample_rate
}

void loop() {
  //int start;
  //int end;
  // Since this is more hefty, do this on an interval
  //if(update) {
    audio.readADCs();
;   //start = micros();
    audio.updatePitch();
    //end = micros();
    if (currentModuleId == SAMPLER_PATCH_MODULE)
      samplePatch.checkCV();
    // Do not read Patch CV when editing the wave
    else if (currentModuleId != EDIT_WAVE)
      patches.checkCV();  
    //update = false;
  //}
  // Run current module
  //Serial.printf("Start: %d, End: %d, Diff: %d\n", start, end , end-start);
  runModule();




}

void runModule() {
  byte returnedModuleId = currentModule->run();
  if (returnedModuleId != currentModuleId) {
    previousModuleId = currentModuleId;
    currentModuleId = returnedModuleId;

    // Change to root as most modules need root
    sd.chdir();

    // Some modes change the gate and callbacks so we have to
    // still do if checks for them
    if(currentModuleId == WAVE_MENU) {
      gate.mode = GATE_MODE_WAVE;
      sampler.stop();
      enableWaveStepCallback();
    }
    if(currentModuleId == NOISE_MODULE) {
      gate.mode = GATE_MODE_NOISE;
      sampler.stop();
      enableNoiseCallback();
    }
    if(currentModuleId == SAMPLER_PATCH_MODULE) {
      gate.mode = GATE_MODE_SAMPLER;
      enableSamplerCallback();
    }

    if(currentModuleId == CONFIG_MENU) {
      audio.tuneKnob = false;
    }
    else {
      audio.tuneKnob = true;
    }

    // The Edit Wave module will switch frames via UI.
    if(currentModuleId == EDIT_WAVE) {
      gate.mode = GATE_MODE_OFF;
    }

    // Kinda simulating a 6502 jump table in a way.
    // Way better than a huge switch statement.
    currentModule = modules[currentModuleId];
    currentModule->activate();
  }
}

// Bootstrap the display
void initDisplay() {
  display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS);
  display.setRotation(0);
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.cp437(true);                  // Use full 256 char 'Code Page 437' font
  //display.clearDisplay();
  //display.display();

  display.clearDisplay();
  display.setCursor(0, 16);  // Start at top-left corner
  display.setTextColor(WHITE, BLACK);
  display.setTextSize(3);
  display.println("WaveBoy");
  display.setTextSize(1);
  display.print(VERSION);
  #if PRERELEASE
    display.println(" PR PCB");
  #else
    display.println();
  #endif
  display.println("By Tim Soderstrom");
  
  display.display();
}

// Interrupts and Callbacks
// These sit outside objects and instead call into objects when needed.
// This is because I couldn't get in-Object interrupt stuff working
// without doing it this way.

/* Gate Interrupts/Timers */
void enableGateInterrupt() {
  attachInterrupt(digitalPinToInterrupt(TRIGGER), gateISR, CHANGE);
}

void gateISR() {
  gate.isr();
}

/* Volume Interrupts/Timers */
void enableVolumeInterrupt() {
  Serial.println("Enabling Volume Interrupt");
  volumeTimer.startTimer(volume.speed, volumeInterrupt);
}

void disableVolumeInterrupt() {
  Serial.println("Disabling Volume Interrupt");
  volumeTimer.stopTimer();
}

void volumeInterrupt() {
  volumeTimer.setVolumeRate(volume.isr());
}

/* Timer to poll things */
void enableUpdateInterrupt() {
  Serial.println("Enabling Update Interrupt");
  updateTimer.startTimer(UPDATE_FREQUENCY, updateInterrupt);
}

void updateInterrupt() {
  update = true;
}


/* Audio Interrupts/Timers */
void waveStepCallback() {
  noInterrupts();
  // Only update the sample rate when we've finished a wave cycle
  // Otherwise this causes pitch tuning inaccuracies and artifacts
  if(audio.waveStepCallback())
    audioTimer.setSampleRate(audio.sampleRate);
  interrupts();
}

void noiseCallback() {
  noInterrupts();
  audio.noiseCallback();
  audioTimer.setSampleRate(audio.sampleRate);
  interrupts();
}

void samplerCallback() {
  noInterrupts();
  // No zero-cross detection here at the moment because it's been wonky
  // getting that perfectly right.
  audio.samplerCallback();
  audioTimer.setSampleRate(audio.sampleRate);
  interrupts();
}

void enableWaveStepCallback() {
  Serial.println("Enabling Wave Callback");
  audio.mode = AUDIO_MODE_WAVE;
  audioTimer.startTimer(audio.sampleRate, waveStepCallback);
}

void enableNoiseCallback() {
  Serial.println("Enabling Noise Callback");
  audio.mode = AUDIO_MODE_NOISE;
  audioTimer.startTimer(audio.sampleRate, noiseCallback);
}

void enableSamplerCallback() {
  Serial.println("Enabling Sampler Callback");
  audio.mode = AUDIO_MODE_SAMPLER;
  audioTimer.startTimer(audio.sampleRate, samplerCallback);
}


// Init ADC0
// This is mostly to get settings that benefit V/Oct stability
void initADC0() {
  ADC0->CTRLA.bit.ENABLE = 0;

  // Enable free-run
  ADC0->CTRLB.bit.FREERUN = 0;

  // Adafruit Default for M4
  ADC0->CTRLA.bit.PRESCALER = ADC_CTRLA_PRESCALER_DIV32_Val;
  ADC0->CTRLA.bit.ONDEMAND = 0;
  
  // Offset compensation enable
  ADC0->SAMPCTRL.reg = 0b10000000;

  // Comp, NC, NC, NC, REFSEL
  // Comp, Internal VREF
  ADC0->REFCTRL.reg = 0b10000011;
  //ADC0->REFCTRL.reg = 0b10000101;

  // Averaging
  ADC0->AVGCTRL.reg = 0b01000100; //  x16
  //ADC0->AVGCTRL.reg = 0b01000101; //  x32
  //ADC0->AVGCTRL.reg = 0b01000110; //  x64
  //ADC0->AVGCTRL.reg = 0b01000111; //  x128
  ADC0->CTRLB.bit.RESSEL = ADC_CTRLB_RESSEL_16BIT_Val;

  // Oversampling 
  //ADC0->AVGCTRL.reg = 0b00001000;
  //ADC0->CTRLB.bit.RESSEL = ADC_CTRLB_RESSEL_16BIT_Val;




  while( ADC0->SYNCBUSY.reg);

  ADC0->CTRLA.bit.ENABLE = 0x01;             // Enable ADC
  while( ADC0->SYNCBUSY.reg);
  ADC0->SWTRIG.bit.START = 1;
  while( ADC0->SYNCBUSY.reg);
}

/*
// Init ADC1
// Used by other things (not V/Oct)
void initADC1() {
  ADC1->CTRLA.bit.ENABLE = 0;
  // Adafruit Default for M4
  ADC1->CTRLA.bit.PRESCALER = ADC_CTRLA_PRESCALER_DIV32_Val;
  //ADC0->CTRLA.bit.PRESCALER = ADC_CTRLA_PRESCALER_DIV64_Val;
  //ADC0->CTRLA.bit.PRESCALER = ADC_CTRLA_PRESCALER_DIV16_Val;

  // Offset compensation enable
  ADC1->SAMPCTRL.reg = 0b10000000;

  // Comp, NC, NC, NC, REFSEL
  // Comp, Internal VREF
  ADC1->REFCTRL.reg = 0b10000011;
  //ADC0->REFCTRL.reg = 0b10000101;

  // 1024 sample averaging
  ADC1->AVGCTRL.reg = 0b01001010; //  x1024
  ADC1->CTRLB.bit.RESSEL = ADC_CTRLB_RESSEL_16BIT_Val;
  while( ADC0->SYNCBUSY.reg);
}
*/


