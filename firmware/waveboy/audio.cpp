#include "audio.h"

// Audio Stuff
Audio::Audio(Patches* patches, Noise* noise, Sampler* sampler, Volume* volume) {
  this->patches = patches;
  this->noise = noise;
  this->sampler = sampler;
  this->volume = volume;
  this->filter = FilterTwoPole();
  this->filter.setAsFilter(LOWPASS_BESSEL, FILTER_CUTOFF);
}


void Audio::readADCs() {
  uint16_t sample = (readADC0(VOCT) ^ 0x0FFF);
  
  // Rolling Average
  // Old sample "falls off"
  /*
  rollingAverageSample -= rollingAverageSample / ADC_OVERSAMPLING;
  // New sample gets added into average
  rollingAverageSample += float(sample / ADC_OVERSAMPLING);
  voctADC = rollingAverageSample;
  */

  // Two Pole Filter
  
  #ifdef TWO_POLE
    filter.input(sample);
    voctADC = filter.output();
  #endif

  // Running Median
  #ifdef MEDIAN
    median.add(sample);
    voctADC = median.getMedian();
  #endif

  // Noise testing
  #ifdef NO_FILTER
    voctADC = sample;
  #endif

  // Use the tune knob to adjust pitch up from base
  if(tuneKnob)
    tuneNote = (readADC0(TUNE) / float(TUNE_RESOLUTION)) + TUNE_NOTE_OFFSET;
  else
    // Used for tuning so the knob doesn't impact the results.
    tuneNote = 36.0;

}

/* Calculate the pitch based on V/Oct and Tune using the MIDI formula which is:
 *  
 *  pitch = 440 * 2^((note - 69) / 12)
 *  
 *  And can be found here:
 *  https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
 */
void Audio::updatePitch() {
  //volatile double stepsPerNote;
  volatile float stepsPerNote;

  for(byte count = 0; count < NUMBER_OF_TUNE_RANGES; ++count) {
    if(voctADC <= tuneRanges[count].rangeHigh) {
      stepsPerNote = tuneRanges[count].stepsPerNote;
      break;
    }
  }

  //voctNote = double(double(voctADC) / double(stepsPerNote));
  voctNote = float(float(voctADC) / float(stepsPerNote));



  note = voctNote + tuneNote;

  // Finally generate the pitch using the magical formula and multiply it by the number of
  // wave slots to get our actual sample rate.
  //  pitch = 440 * 2^( (note - 69) / 12)
  //pitch = double(440.0 * pow(2.0,( (note - 69.0) / 12.0 )));
  pitch = float(440.0 * pow(2.0,( (note - 69.0) / 12.0 )));

  if(mode == AUDIO_MODE_SAMPLER)
    sampleRate = (sampler->rateMultiplier * pitch);
  else
    sampleRate = (WAVE_SLOTS * pitch);    

  // Debug
  #if DEBUG
  if(timeToUpdate < millis()) {
    Serial.printf("%2.4f, %2.4f, %2.4f, %2.4f, %2.4f\n", voctADC, voctNote, pitch, sampleRate, float(MICROSECONDS / sampleRate));
    timeToUpdate = millis() + 250;
  }
  #endif
}

bool Audio::waveStepCallback() {
  uint16_t value;

  // Step through our wave index, wrapping around when we reach the end.
  dacWaveIndex = ++dacWaveIndex % (WAVE_SLOTS);


  // Because we have a bipolar wave, offset into an unsigned int space, 
  // we have to either subtract or add the volume value across the
  // virtual center.
  if(patches->currentWave[dacWaveIndex] > AUDIO_DAC_CENTER) {
    value = patches->currentWave[dacWaveIndex] - volume->getAttenuation();
    if(value < AUDIO_DAC_CENTER)
      value = AUDIO_DAC_CENTER;
  }
  else {
    value = patches->currentWave[dacWaveIndex] + volume->getAttenuation();
    if(value > AUDIO_DAC_CENTER)
      value = AUDIO_DAC_CENTER;
  }

  // Magic writing to the DAC
  // See https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
  while (DAC->SYNCBUSY.bit.DATA0);
  DAC->DATA[0].reg = value;

  // Return true when we have completed writing out a single waveform
  if(dacWaveIndex == 0)
    return true;
  return false;
}

void Audio::noiseCallback() {
  // The DAC is only 12-bits, but the noise modes work on 16-bits. So it needs to get mapped down to 12.
  // Otherwise artifacts happen (which is fine, it's noise, but that messes up the volume envs)
  uint16_t value = map(noise->next(), 0, 65535, 0, AUDIO_DAC_MAX);
  //uint16_t value = noise->next();
  
  //if(value > AUDIO_DAC_MAX)
  //  value = AUDIO_DAC_MAX;


  // Even with noise we have a bipolar wave, so adjust volume as appropriate. (See above)
  if(value > AUDIO_DAC_CENTER) {
    value = value - volume->getAttenuation();
    if(value < AUDIO_DAC_CENTER)
      value = AUDIO_DAC_CENTER;
  }
  else {
    value = value + volume->getAttenuation();
    if(value > AUDIO_DAC_CENTER)
      value = AUDIO_DAC_CENTER;
  }

  while (DAC->SYNCBUSY.bit.DATA0);
  DAC->DATA[0].reg = value;
}

bool Audio::samplerCallback() {
  uint16_t sample;
  if(sampler->running) {
    sample = sampler->next();
    while (DAC->SYNCBUSY.bit.DATA0);
    DAC->DATA[0].reg = sampler->next();
    // If we zero-cross, return true (for evaluating pitch)
    if(sample == AUDIO_DAC_CENTER)
      return true;
    return false;
  }

  // Sample is not running
  while (DAC->SYNCBUSY.bit.DATA0);
  DAC->DATA[0].reg = AUDIO_DAC_CENTER;
  return true;
}

uint16_t Audio::getVoctADC() {
  return voctADC;
}

float Audio::getPitch() {
  return pitch;
}

float Audio::getNote() {
  return note;
}