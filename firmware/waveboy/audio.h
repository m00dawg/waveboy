// Audio Stuff
#pragma once
#include <Arduino.h>
#include <Filters.h>
//#include <RunningMedian.h>
#include "defines.h"
#include "patches.h"
#include "functions.h"
#include "sampler.h"
#include "src/ui/noise.h"
#include "volume.h"

#define NUMBER_OF_TUNE_RANGES 60
//#define ADC_OVERSAMPLING 16384
//#define ADC_OVERSAMPLING 128
//#define ADC_OVERSAMPLING 64
//#define ADC_OVERSAMPLING 4

#define USE_FILTER 1
#define FILTER_CUTOFF 60.0  //Hz

#define AUDIO_MODE_WAVE 0
#define AUDIO_MODE_NOISE 1
#define AUDIO_MODE_SAMPLER 2

// Struct for a single patch
struct TuneRange {
  String name;
  uint16_t rangeHigh;
  float stepsPerNote;
};

class Audio {
  private:
    Patches* patches;
    Noise* noise;
    Sampler* sampler;
    Volume* volume;
    
    //https://github.com/JonHub/Filters.git
    FilterTwoPole filter;
    //RunningMedian median = RunningMedian(7);
    //volatile uint16_t voctADC = 0;

    volatile byte dacWaveIndex = 0;

    volatile byte currentSample = 0;

    volatile float voctADC = 0;
    volatile float voctNote = 0;
    volatile float tuneNote = 0;
    volatile float note = 0;
    volatile float pitch = 0;
  
  /*
    //volatile double voctADC = 0;
    //volatile double voctOversample =0;
    //volatile double voctNote = 0;
    //volatile double tuneNote = 0;
    //volatile double note = 0;
    //volatile double pitch = 0;
    //volatile double rollingAverageSample = 0;
    //volatile double oldSampleRate;
    */

    //uint16_t readVOct();
    //uint16_t readPitchKnob();
    #if DEBUG
    int timeToUpdate = 0;
    #endif

  public:
    //float sampleRate;
    volatile float sampleRate;
    volatile bool tuneKnob = true;  // Enable = on, Disable = off (used for pitch tuning)
    volatile bool sampleChanged = false;
    volatile byte mode = AUDIO_MODE_WAVE;

    TuneRange tuneRanges[NUMBER_OF_TUNE_RANGES];
    
    // Constructor
    Audio(Patches* patches, Noise* noise, Sampler* sampler, Volume* volume);
    
    void readADCs();
    void updatePitch();
    bool waveStepCallback();
    void noiseCallback();
    bool samplerCallback();
    uint16_t getVoctADC();
    float getPitch();
    float getNote();
};


