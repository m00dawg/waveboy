#include "functions.h"

const int static pixelSizeX = OLED_SCREEN_X / WAVE_SLOTS - 1;
const int static pixelSizeY = OLED_SCREEN_Y / WAVE_RANGE_MAX - 1;

// Convert hex character to byte
// This is bad and I feel bad.
byte charToHex(char chr) {
  switch (chr) {
    case '0':
      return 0;
    case '1':
      return 1;
    case '2':
      return 2;
    case '3':
      return 3;
    case '4':
      return 4;
    case '5':
      return 5;
    case '6':
      return 6;
    case '7':
      return 7;
    case '8':
      return 8;
    case '9':
      return 9;
    case 'a':
      return 10;
    case 'b':
      return 11;
    case 'c':
      return 12;
    case 'd':
      return 13;
    case 'e':
      return 14;
    case 'f':
      return 15;
    default:
      return 0;
  }
}

// Convert byte to hex character
char hexToChar(byte value) {
  return (char) String(value, HEX)[0];
}

// Constrain character values within ones we want to allow in things like patch names
char characterLookup(byte char_value) {
  // This is how we wrap around
  if(char_value < CHAR_SPACE)
    return CHAR_LOWER_Z;
  if(char_value > CHAR_LOWER_Z)
    return CHAR_SPACE;

  // This is how we skip characters we don't want.
  // We went up from space, so hop to zero
  if(char_value == CHAR_BANG)
    return CHAR_ZERO;
  // We came down from zero to hop to space
  if(char_value == CHAR_SLASH)
    return CHAR_SPACE;
  // We went up to colon, so hop to A
  if(char_value == CHAR_COLON)
    return CHAR_UPPER_A;
  // We went down from A so hop to 9    
  if(char_value == CHAR_AT)
    return CHAR_NINE;
  // We went up to bracket so hop to a
  if(char_value == CHAR_LEFT_BRACKET)
    return CHAR_LOWER_A;
  // We went down from a so hop to Z
  if(char_value == CHAR_BACKTICK)
    return CHAR_UPPER_Z;
  // If we made it through that guantlet, return the character.
  return char_value;
}

bool compareCharArray(char *arr1, char *arr2, byte length) {
  for(byte count = 0; count < length; ++count) {
    if(arr1[count] != arr2[count])
      return false;
  }
  return true;
}

byte checkBackToPreviousModule() {
  if(upDownEncoder.pushLong(BUTTON_LONG_PRESS))
    return previousModuleId;
  return currentModuleId;
}

byte checkGotoModule(byte module) {
  if(upDownEncoder.pushLong(BUTTON_LONG_PRESS))
    return module;
  return currentModuleId;
}

// Draw pixel sized to the display scale, based on the wave size
void drawPixel(byte x, byte y) {
  for (byte ix = 0 ; ix < pixelSizeX; ix++)
    for (byte iy = 0 ; iy < pixelSizeY; iy++) 
      // The weird OLED_SCREEN_Y - 1 - (y_pos + y) is to invert the display
      // so "up" is actually up.
      display.drawPixel(x + ix, OLED_SCREEN_Y - 1 - (y + iy), SSD1306_WHITE);
}

// Draw a pixel based on the size of the wave
void drawWave(byte wave[WAVE_SLOTS]) {
  byte displayXPos = 0;
  byte displayYPos = 0;
  for (byte i = 0; i < WAVE_SLOTS; i++) {
    displayXPos = i * pixelSizeX;    
    displayYPos = wave[i] * pixelSizeY;
    drawPixel(displayXPos, displayYPos);
  }
}

uint16_t readADC0(byte pin) {
  uint16_t sample;
  while( ADC0->SYNCBUSY.reg); //wait for sync
  ADC0->INPUTCTRL.bit.MUXPOS = g_APinDescription[pin].ulADCChannelNumber; // Selection for the positive ADC input
  while( ADC0->SYNCBUSY.reg); //wait for sync
  
  // Start conversion
  while( ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE ); //wait for sync
  ADC0->SWTRIG.bit.START = 1;

  // Store the value
  while (ADC0->INTFLAG.bit.RESRDY == 0);   // Waiting for conversion to complete
  sample = ADC0->RESULT.reg;

  while( ADC0->SYNCBUSY.reg); //wait for sync
  return sample;
}


/*
uint16_t readADC1(byte pin) {
  uint16_t sample;

  while( ADC1->SYNCBUSY.reg & ADC_SYNCBUSY_INPUTCTRL ); //wait for sync
  ADC1->INPUTCTRL.bit.MUXPOS = g_APinDescription[pin].ulADCChannelNumber; // Selection for the positive ADC input
  
  // Control A
  //
  // Bit 1 ENABLE: Enable
  //   0: The ADC is disabled.
  //   1: The ADC is enabled.
  // Due to synchronization, there is a delay from writing CTRLA.ENABLE until the peripheral is enabled/disabled. The
  // value written to CTRL.ENABLE will read back immediately and the Synchronization Busy bit in the Status register
  // (STATUS.SYNCBUSY) will be set. STATUS.SYNCBUSY will be cleared when the operation is complete.
  //
  // Before enabling the ADC, the asynchronous clock source must be selected and enabled, and the ADC reference must be
  // configured. The first conversion after the reference is changed must not be used.
  //
  while( ADC1->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE ); //wait for sync
  ADC1->CTRLA.bit.ENABLE = 0x01;             // Enable ADC

  // Start conversion
  while( ADC1->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE ); //wait for sync
  
  ADC1->SWTRIG.bit.START = 1;

  // Clear the Data Ready flag
  ADC1->INTFLAG.reg = ADC_INTFLAG_RESRDY;

  // Start conversion again, since The first conversion after the reference is changed must not be used.
  ADC1->SWTRIG.bit.START = 1;

  // Store the value
  while (ADC1->INTFLAG.bit.RESRDY == 0);   // Waiting for conversion to complete
  
  sample = ADC1->RESULT.reg;

  while( ADC1->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE ); //wait for sync
  ADC0->CTRLA.bit.ENABLE = 0x00;             // Disable ADC
  while( ADC1->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE ); //wait for sync
  return sample;
}
*/