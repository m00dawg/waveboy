#include "sampler.h"

Sampler::Sampler() {
}


void Sampler::init(Sample* sample) {
    this->sample = sample;
}

void Sampler::start() {
  samplePosition = 0;
  running = true;
}

void Sampler::stop() {
  samplePosition = 0;
  running = false;
}

uint16_t Sampler::next() {
  if(running) {
    // Shift given desired bitrate of sample (if 12, no shifts; 8, 4 shifts, etc.)
    uint16_t currentSample = sampleData[samplePosition] << (DAC_RESOLUTION - bitrate);

    // Forward Loop
    if(sample->loopMode == 'F') {
      if(samplePosition == sample->loopEnd)
        samplePosition = sample->loopStart;
      else
        ++samplePosition;
    }
    // One-Shot
    else {
      ++samplePosition;
      if(samplePosition == sampleLength) {
        running = false;
        return AUDIO_DAC_CENTER;
      }
    }
    return currentSample;
  }
  return AUDIO_DAC_CENTER;
}
