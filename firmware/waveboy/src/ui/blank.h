/* UI Module which just blanks the display */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../defines.h"
#include "ui.h"

class Blank: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
  public:
    Blank(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder);
    void activate();
    byte run();
};