#include "adsrui.h"

ADSRUI::ADSRUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, ADSR* adsr) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->adsr = adsr;
  sustainCoarse = this->adsr->sustainValue >> ENV_TO_VOL_RANGE;
}

void ADSRUI::activate () {
  drawDisplay();
}

void ADSRUI::drawDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("ADSR Env\n--------");

  /* This thing is ugly */
  if(mode == ADSR_UI_ATTACK)
    display->print(">");
  else
    display->print(" ");
  display->printf("Attack Speed:  %02X\n", adsr->attackSpeed);
  
  if(mode == ADSR_UI_DECAY)
    display->print(">");
  else
    display->print(" ");
  display->printf("Decay Speed:   %02X\n", adsr->decaySpeed);

  if(mode == ADSR_UI_RELEASE)
    display->print(">");
  else
    display->print(" ");
  display->printf("Release Speed: %02X\n", adsr->releaseSpeed);

  if(mode == ADSR_UI_SUSTAIN)
    display->print(">");
  else
    display->print(" ");
  display->printf("Sustain Vol:   %02X\n", sustainCoarse);

  if(mode == ADSR_UI_SPEED_MULT)
    display->print(">");
  else
    display->print(" ");
  display->printf("Multiplier:    %02X", adsr->speedMultiplier);
  
  display->display();
}

byte ADSRUI::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  if (udRotate == ENCODER_TURNED_LEFT) {
    if(mode == 0)
      mode = ADSR_UI_MENU_SIZE - 1;
    else
      mode = --mode % ADSR_UI_MENU_SIZE;
    drawDisplay();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    mode = ++mode % ADSR_UI_MENU_SIZE;
    drawDisplay();
  }

  if (lrRotate == ENCODER_TURNED_LEFT) {
    switch (mode) {
      case ADSR_UI_ATTACK:
        --adsr->attackSpeed;
        break;
      case ADSR_UI_DECAY:
        --adsr->decaySpeed;
        break;
      case ADSR_UI_RELEASE:
        --adsr->releaseSpeed;
        break;
      case ADSR_UI_SUSTAIN:
        --sustainCoarse;
        // The full value is greater than we set in the UI so map the values appropriately.
        adsr->sustainValue = sustainCoarse << ENV_TO_VOL_RANGE;
        break;
      // Speed Multiplier
      default:
        --adsr->speedMultiplier;
    }
    drawDisplay();
  }
  else if (lrRotate == ENCODER_TURNED_RIGHT) {
    switch (mode) {
      case ADSR_UI_ATTACK:
        ++adsr->attackSpeed;
        break;
      case ADSR_UI_DECAY:
        ++adsr->decaySpeed;
        break;
      case ADSR_UI_RELEASE:
        ++adsr->releaseSpeed;
        break;
      // Sustain
      case ADSR_UI_SUSTAIN:
        ++sustainCoarse;
        // The full value is greater than we set in the UI so map the values appropriately.
        adsr->sustainValue = sustainCoarse << ENV_TO_VOL_RANGE;
        break;
      // Speed Multiplier
      default:
        ++adsr->speedMultiplier;
    }
    drawDisplay();
  }

  // Check for long press of left encoder to go back
  return checkBackToPreviousModule();
}
