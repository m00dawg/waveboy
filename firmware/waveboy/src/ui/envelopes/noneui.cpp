#include "noneui.h"

NoneUI::NoneUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, None* none) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->none = none;
}

void NoneUI::activate () {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("None Env\n--------\nUse ext env/VCA");
  display->display();
}

byte NoneUI::run() {
  // Check for long press of left encoder to go back
  return checkBackToPreviousModule();
}