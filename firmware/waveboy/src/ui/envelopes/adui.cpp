#include "adui.h"

ADUI::ADUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, AD* ad) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->ad = ad;
}

void ADUI::activate () {
  drawDisplay();
}

void ADUI::drawDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("AD Env\n------");

  /* This thing is ugly */
  if(mode == AD_UI_ATTACK)
    display->print(">");
  else
    display->print(" ");
  display->printf(" Attack Speed: %02X\n", ad->attackSpeed);
  
  if(mode == AD_UI_DECAY)
    display->print(">");
  else
    display->print(" ");
  display->printf(" Decay Speed:  %02X\n", ad->decaySpeed);

  if(mode == AD_UI_SPEED_MULT)
    display->print(">");
  else
    display->print(" ");
  display->printf(" Multiplier:   %02X", ad->speedMultiplier);
  display->display();
}

byte ADUI::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  /* Change Option */
  if (udRotate == ENCODER_TURNED_LEFT) {
    if(mode == 0)
      mode = AD_UI_MENU_SIZE - 1;
    else
      mode = --mode % AD_UI_MENU_SIZE;
    drawDisplay();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    mode = ++mode % AD_UI_MENU_SIZE;
    drawDisplay();
  }

  /* Adjust speed */
  if (lrRotate == ENCODER_TURNED_LEFT) {
    switch (mode) {
      case AD_UI_ATTACK:
        --ad->attackSpeed;
        break;
      case AD_UI_DECAY:
        --ad->decaySpeed;
        break;
      // Speed Mult
      default:
        --ad->speedMultiplier;
    }
    drawDisplay();
  }
  else if (lrRotate == ENCODER_TURNED_RIGHT) {
    switch (mode) {
      case AD_UI_ATTACK:
        ++ad->attackSpeed;
        break;
      case AD_UI_DECAY:
        ++ad->decaySpeed;
        break;
      // Speed Mult
      default:
        ++ad->speedMultiplier;
    }
    drawDisplay();
  }

  // Check for long press of left encoder to go back
  return checkBackToPreviousModule();
}