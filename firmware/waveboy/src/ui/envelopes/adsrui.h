/* UI Module For the ADSR Envelope */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../envelopes/adsr.h"

#define ADSR_UI_ATTACK 0
#define ADSR_UI_DECAY 1
#define ADSR_UI_RELEASE 2
#define ADSR_UI_SUSTAIN 3
#define ADSR_UI_SPEED_MULT 4
#define ADSR_UI_MENU_SIZE 5

class ADSRUI: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    ADSR* adsr;
    byte lrRotate = 0;
    byte udRotate = 0;
    // False = Attack, True = Decay
    byte mode = ADSR_UI_ATTACK;
    byte sustainCoarse;
    void drawDisplay();
  public:
    ADSRUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, ADSR* adsr);
    void activate();
    byte run();
};