/* UI Module For the Decay Envelope */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../envelopes/decay.h"

class DecayUI: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    Decay* decay;
    byte lrRotate = 0;
    byte udRotate = 0;
    void drawDisplay();
  public:
    DecayUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Decay* decay);
    void activate();
    byte run();
};