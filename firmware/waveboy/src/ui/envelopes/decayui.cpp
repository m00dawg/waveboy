#include "decayui.h"

DecayUI::DecayUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Decay* decay) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->decay = decay;
}

void DecayUI::activate () {
  drawDisplay();
}

void DecayUI::drawDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("Decay Env\n---------");
  display->printf("Speed: %02X\nMultiplier: %02X", decay->speed, decay->speedMultiplier);
  //display->println("\n\n\nCoarse           Fine");
  display->display();
}

byte DecayUI::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  /* Adjust speed */
  if (udRotate == ENCODER_TURNED_RIGHT)
  {
    ++decay->speed;
    drawDisplay();
  }
  if (udRotate == ENCODER_TURNED_LEFT)
  {
    --decay->speed;
    drawDisplay();
  }

  if (lrRotate == ENCODER_TURNED_RIGHT)
  {
    ++decay->speedMultiplier;
    drawDisplay();
  }
  if (lrRotate == ENCODER_TURNED_LEFT)
  {
    --decay->speedMultiplier;
    drawDisplay();
  }

  // Check for long press of left encoder to go back
  return checkBackToPreviousModule();
}