/* UI Module For the AD Envelope */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../envelopes/ad.h"

#define AD_UI_ATTACK 0
#define AD_UI_DECAY 1
#define AD_UI_SPEED_MULT 2
#define AD_UI_MENU_SIZE 3

class ADUI: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    AD* ad;
    byte lrRotate = 0;
    byte udRotate = 0;
    byte mode = AD_UI_ATTACK;
    void drawDisplay();
  public:
    ADUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, AD* ad);
    void activate();
    byte run();
};