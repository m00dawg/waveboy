/* Tracker Module */

/* Overall idea
 * TBD
 */ 
#include "tracker.h"

Tracker::Tracker(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, SdFs* sd) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->sd = sd;
}

void Tracker::printMenu() {


  display->clearDisplay();
	display->setTextSize(1);
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->print("TRACKER C:# N:#\n---------------\n");
	display->println("00 C-4 F 47 D04");
	display->println("01 ... . .. ...");
	display->setTextSize(2);
	display->println("02 D#4 D 32");
	display->setTextSize(1);
	display->println("03 ... . .. ...");
  display->println("04 A#3 E 64 ...");
  display->display();
}

void Tracker::activate() {
  printMenu();
}

byte Tracker::run() {
  // Check for long press of left encoder to go back
  return checkBackToPreviousModule();
}