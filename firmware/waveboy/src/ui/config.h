
/* Load and save configuration options from the file on the SD card */
#pragma once
#include <Arduino.h>
#include <SdFat.h>
#include "../../defines.h"
#include "../../functions.h"
#include "ui.h"
#include "../../audio.h"

#define CONFIG_MENU_SIZE 2
#define CONFIG_MENU_TUNING 0
#define CONFIG_MENU_SAVE 1

#define CONFIG_MODE_ADC_UPDATE_MS    2000 // 2 seconds

class Config: public UI{
  private:
    Adafruit_SSD1306* display;
    SdFs* sd;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    Audio* audio;
    Patches* patches;
    FsFile configFile;
    FsFile tuningFile;
    String configMenuList[CONFIG_MENU_SIZE] = {
      "Tuning",
      "Save All"
    };
    byte menuSelection = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    void printMenu();
  
  public:
    // Constructor
    Config(Adafruit_SSD1306* display, SdFs* sd, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Audio* audio);

    bool loadConfig();
    void saveConfig();
    void activate();
    byte run();
};