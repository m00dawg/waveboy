/* Module which just blanks the display */
#include "volumeui.h"

VolumeUI::VolumeUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Volume* volume) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->volume = volume;
}

void VolumeUI::activate () {
  drawDisplay();
}

void VolumeUI::drawDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("Volume Env\n----------");
  for(byte count = 0; count < NUMBER_OF_ENVELOPE_TYPES ; ++count)
  {
    display->print(volume->envelopes[count]->getName());
    if(count == volume->getEnvIndex())
      display->print(" <--");
    display->println();
  }
  display->display();
}

byte VolumeUI::run() {
  udRotate = upDownEncoder->rotate();
  
  if (udRotate == ENCODER_TURNED_RIGHT)
  {
    if(volume->getEnvIndex() == NUMBER_OF_ENVELOPE_TYPES - 1)
      volume->switchEnvelope(0);
    else
      volume->switchEnvelope(volume->getEnvIndex() + 1);
    drawDisplay();
  }
  else if (udRotate == ENCODER_TURNED_LEFT)
  {
    if(volume->getEnvIndex() == 0)
      volume->switchEnvelope(NUMBER_OF_ENVELOPE_TYPES - 1);
    else
      volume->switchEnvelope(volume->getEnvIndex() - 1);
    drawDisplay();
  }

  /* If user presses right, switch to the currently selected module. */
  if (leftRightEncoder->push())
  {
    switch (volume->getEnvIndex()) {
      case NONE_ENVELOPE:
        return VOLUME_NONE_MODULE;
      case DECAY_ENVELOPE:
        return VOLUME_DECAY_MODULE;
      case AD_ENVELOPE:
        return VOLUME_AD_MODULE;
      case ADSR_ENVELOPE:
        return VOLUME_ADSR_MODULE;
      default:
        return VOLUME_MODULE;
    }
  }

  // Check for long press of left encoder to go back
  return checkGotoModule(MAIN_MENU);
}