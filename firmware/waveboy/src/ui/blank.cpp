/* Module which just blanks the display */
#include "blank.h"

//Blank::Blank(Adafruit_SSD1306 display, SimpleRotary upDownEncoder, SimpleRotary leftRightEncoder) {
//Blank::Blank(Adafruit_SSD1306& display, SimpleRotary& upDownEncoder, SimpleRotary& leftRightEncoder) {
Blank::Blank(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
}

void Blank::activate () {
  display->clearDisplay();
  display->display();
}

byte Blank::run() {
  /* If we get any input, swap the main menu */
  if (upDownEncoder->push() || leftRightEncoder->push())
  {
    return MAIN_MENU;
  }
  return BLANK;
}