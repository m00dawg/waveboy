#pragma once
/* UI Interface for interacting with the display and encoders */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../defines.h"

class UI {
  protected:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
  public:
    virtual void activate();
    virtual byte run();
};