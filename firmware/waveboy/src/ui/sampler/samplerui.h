/* Sampler Module  */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include <SdFat.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "samplepatch.h"

#define SAMPLER_NUM_PATCHES_IN_MENU 4

class SamplerUI: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    SdFs* sd;
    SamplePatch* samplePatch;

    void printMenu();
    void getPatchCount();

    char dirName[SAMPLE_NAME_LENGTH];

    byte lrRotate = 0;
    byte udRotate = 0;
    byte fileIndex = 0;
    byte numPatches = 0;
    byte dirStartPosition = 0;
    byte menuPosition = 0;

  public:
    SamplerUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, SdFs* sd, SamplePatch* samplePatch);
    
    void activate();
    byte run();
};