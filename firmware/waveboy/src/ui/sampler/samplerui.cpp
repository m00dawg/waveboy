/* Sampler UI Module */


#include "samplerui.h"

SamplerUI::SamplerUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, SdFs* sd, SamplePatch* samplePatch) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->sd = sd;
  this->samplePatch = samplePatch;
}

void SamplerUI::printMenu() {
  SdFile file;
  SdFile dirFile;
  byte patchCount = 0;
  byte menuCount = 0;
  char patchName[SAMPLE_NAME_LENGTH];

  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->print("SAMPLER\n-------\n");

  sd->chdir();
  if (!dirFile.open(SAMPLE_DIRECTORY, O_RDONLY)) {
    sd->errorHalt("open root failed");
  }

  //while (file.openNext(&dirFile, O_RDONLY) && count < 6) {
  while (file.openNext(&dirFile, O_RDONLY)) { 
    if(menuCount == SAMPLER_NUM_PATCHES_IN_MENU)
      break;
    if (file.isSubDir() && !file.isHidden()) {
      if(patchCount >= dirStartPosition ) {
        file.getName(patchName, 16);
        if (patchCount == fileIndex) {
          display->setTextColor(BLACK, WHITE);
          memcpy(dirName, patchName, SAMPLE_NAME_LENGTH);
        }
        else
          display->setTextColor(WHITE, BLACK);
        display->println(patchName);
        ++menuCount;
      }
      ++patchCount;
    }
  }
  display->setTextColor(WHITE, BLACK);
  display->display();
}

void SamplerUI::activate() {
  lrRotate = 0;
  udRotate = 0;
  fileIndex = 0;
  numPatches = 0;
  menuPosition = 0;
  dirStartPosition = 0;

  getPatchCount();
  printMenu();
  //reset();
}

void SamplerUI::getPatchCount() {
  SdFile file;
  SdFile dirFile;
  numPatches = 0;
  // Change to root dir
  sd->chdir();
  
  if (!dirFile.open(SAMPLE_DIRECTORY, O_RDONLY)) {
    sd->errorHalt("open root failed");
  }
  while (file.openNext(&dirFile, O_RDONLY)) {
    if (file.isSubDir() && !file.isHidden()) {
      ++numPatches;
    }
  }
  //Serial.printf("Patches found: %d\n", numPatches);
}

byte SamplerUI::run() {
  udRotate = upDownEncoder->rotate();

  if (udRotate == ENCODER_TURNED_LEFT) {
    if(fileIndex == 0)
    {
      fileIndex = numPatches - 1;
      menuPosition = SAMPLER_NUM_PATCHES_IN_MENU - 1;
      dirStartPosition = numPatches - SAMPLER_NUM_PATCHES_IN_MENU;
    }
    else {
      fileIndex = --fileIndex;
      if(menuPosition == 0)
        --dirStartPosition;
      else
        --menuPosition;
    }
    printMenu();
    //reset();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    fileIndex = ++fileIndex;
    if(menuPosition == SAMPLER_NUM_PATCHES_IN_MENU - 1)
      ++dirStartPosition;
    else
      ++menuPosition;
    if(fileIndex == numPatches)
    {
      dirStartPosition = 0;
      menuPosition = 0;
      fileIndex = 0;
    }
    printMenu();
    //reset();
  }

  if(leftRightEncoder->push())
  {
    samplePatch->changePatch(dirName);
    
    //noiseTypes[mode]->reset();
    //manipulate->setManipulator(manipulators[mode]);
    //return MANIPULATE_WAVE;
    return SAMPLER_PATCH_MODULE;
  }

  // Check for long press of left encoder to go back
  //return checkBackToPreviousModule();
  return checkGotoModule(MAIN_MENU);
}
