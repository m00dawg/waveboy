#include "samplepatch.h"

SamplePatch::SamplePatch(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, SdFs* sd, Sampler* sampler) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->sd = sd;
  this->sampler = sampler;
}

void SamplePatch::updateDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println(patchDir);
  display->println();

  if(option == SAMPLEPATCH_MENU_INDEX)
    display->print(">");
  display->printf("%x:", sampleIndex);
  display->println(samples[sampleIndex].name);

  if(option == SAMPLEPATCH_MENU_BITRATE)
    display->print(">");
  display->printf("Bitrate: %d-bits\n", samples[sampleIndex].bitrate);

  if(option == SAMPLEPATCH_MENU_RATEMULT)
    display->print(">");
  display->printf("Rate Mult: %d\n", samples[sampleIndex].rateMultiplier);

  if(option == SAMPLEPATCH_MENU_LOOP_MODE)
    display->print(">");
  display->print("Loop Mode: ");
  display->println(samples[sampleIndex].loopMode);

  if(option == SAMPLEPATCH_MENU_LOOP_START)
    display->print(">");
  display->printf("Start: %d\n", samples[sampleIndex].loopStart);

  if(option == SAMPLEPATCH_MENU_LOOP_END)
    display->print(">");
  display->printf("End: %d\n", samples[sampleIndex].loopEnd);

  display->display();
}

void SamplePatch::activate() {
  sampleIndex = 0;
  loadSample();
  updateSampleMetadata();
  checkCV();
}

byte SamplePatch::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  if (udRotate == ENCODER_TURNED_RIGHT) {
    if(option == SAMPLEPATCH_MENU_SIZE - 1)
      option = 0;
    else
      ++option;
    updateDisplay();
  }
  if (udRotate == ENCODER_TURNED_LEFT) {
    if(option == 0)
      option = SAMPLEPATCH_MENU_SIZE - 1;
    else
      --option;
    updateDisplay();
  }

  if (lrRotate == ENCODER_TURNED_RIGHT) {
    switch (option) {
      case SAMPLEPATCH_MENU_BITRATE:
        if(samples[sampleIndex].bitrate < 12)
          ++samples[sampleIndex].bitrate;
        break;
      case SAMPLEPATCH_MENU_RATEMULT:
        ++samples[sampleIndex].rateMultiplier;
        break;
      case SAMPLEPATCH_MENU_LOOP_MODE:
        if(samples[sampleIndex].loopMode == 'F')
          samples[sampleIndex].loopMode = 'O';
        else
          samples[sampleIndex].loopMode = 'F';
        break;
      // Need to add logic so changing the start/end changes both where relevant
      // (the end cannot be earlier than the start)
      case SAMPLEPATCH_MENU_LOOP_START:
        ++samples[sampleIndex].loopStart;
        break;
      case SAMPLEPATCH_MENU_LOOP_END:
        ++samples[sampleIndex].loopEnd;
        break;
      // SAMPLEPATCH_MENU_INDEX (change sample)
      default:
        if(sampleIndex == MAX_SAMPLE)
          sampleIndex = 0;
        else
          ++sampleIndex;
    }
    updateSampleMetadata();
  }

  if (lrRotate == ENCODER_TURNED_LEFT) {
    switch (option) {
      case SAMPLEPATCH_MENU_BITRATE:
        if(samples[sampleIndex].bitrate > 1)
          --samples[sampleIndex].bitrate;
        break;
      case SAMPLEPATCH_MENU_RATEMULT:
        --samples[sampleIndex].rateMultiplier;
        break;
      case SAMPLEPATCH_MENU_LOOP_MODE:
        if(samples[sampleIndex].loopMode == 'O')
          samples[sampleIndex].loopMode = 'F';
        else
          samples[sampleIndex].loopMode = 'O';
        break;
      case SAMPLEPATCH_MENU_LOOP_START:
        --samples[sampleIndex].loopStart;
        break;
      case SAMPLEPATCH_MENU_LOOP_END:
        --samples[sampleIndex].loopEnd;
        break;
      // SAMPLEPATCH_MENU_INDEX (change sample)
      default:
        if(sampleIndex == 0)
          sampleIndex = MAX_SAMPLE;
        else
          --sampleIndex;
    }
    updateSampleMetadata();
  }


  if(upDownEncoder->push())
    return SAMPLER_MODULE;
  
  if(leftRightEncoder->push())
    saveSampleMetadata();

  return checkGotoModule(SAMPLER_MODULE);
}

void SamplePatch::checkCV() {
  uint16_t cv = readADC0(FRAME);
  uint16_t length;


  if(cv > FRAME_ADC_MAX)
    cv = FRAME_ADC_MAX;
  
  cv = constrain(cv, FRAME_ADC_MIN, FRAME_ADC_MAX);
  sampleCV = map(cv, FRAME_ADC_MAX, FRAME_ADC_MIN, 0, MAX_SAMPLE);

  if(sampleCV != prevCV) {
    prevCV = sampleCV;
    sampleIndex = sampleCV;
    loadSample();
    updateSampleMetadata();
  }
}

void SamplePatch::loadSample() {
  FsFile sampleFile;
  bool ret;
  char filename[6] = SAMPLE_FILE_EXT;

  sd->chdir();
  sd->chdir(SAMPLE_DIRECTORY);
  sd->chdir(patchDir);

  filename[0] = hexToChar(sampleIndex);
  if(sd->exists(filename)) {
    sampleFile = sd->open(filename, FILE_READ);
    ret = readWave(&sampleFile);
    sampleFile.close();

  }
  else
    Serial.println("Unable to open sample file");
}

void SamplePatch::updateSampleMetadata() {
    sampler->bitrate = samples[sampleIndex].bitrate;
    sampler->rateMultiplier = samples[sampleIndex].rateMultiplier;
    sampler->loopMode = samples[sampleIndex].loopMode;
    sampler->loopStart = samples[sampleIndex].loopStart;
    sampler->loopEnd = samples[sampleIndex].loopEnd;
    updateDisplay();
}

// Read a wave
// Wave format: https://www.mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html
bool SamplePatch::readWave(FsFile* file) {
  /* The char array comparison stuff is messy but I haven't found a better way to do it. */

  char buffer[5];
  char riff[5] = "RIFF";
  char wave[5] = "WAVE";
  char fmt[5] = "fmt ";
  char data[5] = "data";
  uint32_t length;
  uint16_t value;

  length = file->available();

  // Read RIFF header
  file->read(buffer, 4);
  
  if(!compareCharArray(buffer, riff, 4))
    return false;

  // Read chksize (4 bytes)
  file->read(buffer, 4);

  // Read waveid (4 bytes)
  file->read(buffer, 4);
  if(!compareCharArray(buffer, wave, 4))
    return false;

  // Read chunk-id (4 bytes)
  file->read(buffer, 4);
  if(!compareCharArray(buffer, fmt, 4))
    return false;
  
  // Read chksize (4 bytes)
  file->read(buffer, 4);

  // Check wave format (PCM) and channels
  // read both values at once 
  file->read(buffer, 4);
  
  value = 0;
  value |= (uint16_t) buffer[0];
  value |= (uint16_t) buffer[1] << 8;
  if(value != 1)
    return false;

  value = 0;
  value |= (uint16_t) buffer[2];
  value |= (uint16_t) buffer[3] << 8;
  if(value > 1)
    return false;

  // Seek past fields we don't need
  // and to bits per sample
  file->seek(34);

  // Bits per sample (must be 8-bit)
  value = 0;
  file->read(buffer, 2);
  value |= (uint16_t) buffer[0];
  value |= (uint16_t) buffer[1] << 8;
  if(value != 8)
    return false;

  // Read chkid-id
  file->read(buffer, 4);
  if(!compareCharArray(buffer, data, 4))
    return false;

  // Read chunk-size
  file->read(buffer, 4);
  sampler->sampleLength = 0;
  sampler->sampleLength |= (uint32_t) buffer[3] << 24;
  sampler->sampleLength |= (uint32_t) buffer[2] << 16;
  sampler->sampleLength |= (uint32_t) buffer[1] << 8;
  sampler->sampleLength |= (uint32_t) buffer[0];

  // Finally let's read some sample data
  if(sampler->sampleLength < SAMPLE_SIZE) {
    file->read(sampler->sampleData, sampler->sampleLength);
  }

  return true;
}

void SamplePatch::saveSampleMetadata() {
  FsFile metadataFile;
  char filename[6] = SAMPLE_METADATA_FILE_EXT;
  filename[0] = hexToChar(sampleIndex);

  sd->chdir();
  sd->chdir(SAMPLE_DIRECTORY);
  sd->chdir(this->patchDir);

  sd->remove(filename);
  metadataFile = sd->open(filename, FILE_WRITE);
  metadataFile.print("Name:");
  metadataFile.print(samples[sampleIndex].name);
  metadataFile.print('\n');
  metadataFile.print("Bits:");
  metadataFile.print(samples[sampleIndex].bitrate);
  metadataFile.print('\n');
  metadataFile.print("Multiplier:");
  metadataFile.print(samples[sampleIndex].rateMultiplier);
  metadataFile.print('\n');
  metadataFile.print("Loop Mode:");
  metadataFile.print(samples[sampleIndex].loopMode);
  metadataFile.print('\n');
  metadataFile.print("Loop Start:");
  metadataFile.print(samples[sampleIndex].loopStart);
  metadataFile.print('\n');
  metadataFile.print("Loop End:");
  metadataFile.print(samples[sampleIndex].loopEnd);
  metadataFile.print('\n');
  metadataFile.close();
}


void SamplePatch::changePatch(char patchDir[]) {
  FsFile metadataFile;
  char buf[1];
  char filename[6] = SAMPLE_METADATA_FILE_EXT;

  memcpy(this->patchDir, patchDir, SAMPLE_NAME_LENGTH);

  sd->chdir();
  sd->chdir(SAMPLE_DIRECTORY);
  sd->chdir(this->patchDir);

  // Read metadata sidecar for all the samples
  for(byte count = 0; count < SAMPLES_PER_PATCH; ++count) {
    filename[0] = hexToChar(count);
    if(sd->exists(filename)) {
      metadataFile = sd->open(filename, FILE_READ);
      // Skip name label
      metadataFile.readStringUntil(':');
      samples[count].name = metadataFile.readStringUntil('\n');
      // Skip bit label
      metadataFile.readStringUntil(':');
      samples[count].bitrate = metadataFile.parseInt();
      // Skip rate mult label
      metadataFile.readStringUntil(':');
      samples[count].rateMultiplier = metadataFile.parseInt();
      // Skip loop mode label
      metadataFile.readStringUntil(':');
      metadataFile.read(buf, 1);
      samples[count].loopMode = buf[0];
      // Skip loop start label
      metadataFile.readStringUntil(':');
      samples[count].loopStart = metadataFile.parseInt();
      // Skip loop end label
      metadataFile.readStringUntil(':');
      samples[count].loopEnd = metadataFile.parseInt();
      metadataFile.close();
    }
    // Else set defaults
    else {
      samples[count].name = "None";
      samples[count].bitrate = 8;
      samples[count].rateMultiplier = 128;
      samples[count].loopMode = 'O';
      samples[count].loopStart = 0;
      samples[count].loopEnd = 0;
    }
  }
}

