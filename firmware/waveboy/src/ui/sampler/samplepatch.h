/* Sampler Patch UI
 * Provides info and options for the samples within the patch
 */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include <SdFat.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../../../sampler.h"
#include "../ui.h"

#define SAMPLE_DIRECTORY     "/samples"
#define SAMPLES_PER_PATCH    16
#define MAX_SAMPLE           SAMPLES_PER_PATCH - 1

//#define SAMPLER_DISPLAY_CURRENT_NAME_Y 56
#define SAMPLER_PATCH_DISPLAY_NAME_Y 8
#define SAMPLE_NAME_LENGTH 16
#define SAMPLE_FILE_EXT "x.wav"
#define SAMPLE_METADATA_FILE_EXT "x.txt"
#define SAMPLER_DEFAULT_RATE_MULTIPLIER 128

#define SAMPLEPATCH_MENU_INDEX      0
#define SAMPLEPATCH_MENU_BITRATE    1
#define SAMPLEPATCH_MENU_RATEMULT   2
#define SAMPLEPATCH_MENU_LOOP_MODE  3
#define SAMPLEPATCH_MENU_LOOP_START 4
#define SAMPLEPATCH_MENU_LOOP_END   5
#define SAMPLEPATCH_MENU_SIZE       6

class SamplePatch: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    SdFs* sd;
    Sampler* sampler;
    
    Sample samples[SAMPLES_PER_PATCH];

    void updateDisplay();
    void loadSample();
    void updateSampleMetadata();
    void saveSampleMetadata();
    bool readWave(FsFile* file);    

    byte lrRotate = 0;
    byte udRotate = 0;
    byte sampleIndex = 255;
    byte sampleCV = 0;
    byte prevCV = 0;
    byte option = 0;
   
    // Current rate multiplier of the selected sample
    // It's a copy of the one in the struct just to make it easier for the audio engine
    // to grab the value without having to pass a buncha crap around.
    uint16_t rateMultiplier = SAMPLER_DEFAULT_RATE_MULTIPLIER;

    char patchDir[SAMPLE_NAME_LENGTH];

  public:
    SamplePatch(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, SdFs* sd, Sampler* sampler);
    void activate();
    byte run();
    void checkCV();    
    void changePatch(char patchDir[]);

};