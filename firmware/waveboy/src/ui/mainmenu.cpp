/* Module which shows the main menu */
#include "mainmenu.h"

MainMenu::MainMenu(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
}

void MainMenu::printMenu() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("MENU\n----");
  for (int i = 0; i < MAIN_MENU_SIZE; i++) {
    if(menuSelection == i)
      display->setTextColor(BLACK, WHITE);
    else
      display->setTextColor(WHITE, BLACK);
    display->println(mainMenuList[i]);
  }
  display->display();
}

void MainMenu::activate () {
  printMenu();
}

byte MainMenu::run() {
  udRotate = upDownEncoder->rotate();
  if (udRotate == ENCODER_TURNED_LEFT) {
    if(menuSelection == 0)
      menuSelection = MAIN_MENU_SIZE - 1;
    else
      menuSelection = --menuSelection % MAIN_MENU_SIZE;
    printMenu();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    menuSelection = ++menuSelection % MAIN_MENU_SIZE;
    printMenu();
  }

  if(leftRightEncoder->push()) {
    switch (menuSelection) {
      case MAIN_MENU_WAVE:
        return WAVE_MENU;
      case MAIN_MENU_NOISE:
        return NOISE_MODULE;
      case MAIN_MENU_SAMPLER:
        return SAMPLER_MODULE;
      case MAIN_MENU_TRACKER:
        return TRACKER_MODULE;
      case MAIN_MENU_SCREENSAVER:
        return BLANK;
      case MAIN_MENU_CONFIG:
        return CONFIG_MENU;
      case MAIN_MENU_VOLUME:
        return VOLUME_MODULE;
      default:
        return MAIN_MENU;
    }
  }

  return MAIN_MENU;
}