#include "config.h"

Config::Config(Adafruit_SSD1306* display, SdFs* sd, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Audio* audio) {
  this->display = display;
  this->sd = sd;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
  this->audio = audio;
}

/* Loads all config params (default patch, tunings, etc.) */
bool Config::loadConfig() {
  configFile = sd->open(CONFIG_FILE_NAME, FILE_READ);
  tuningFile = sd->open(TUNING_FILE_NAME, FILE_READ);

  if (!configFile) {
    Serial.println("Error, failed to open config file for reading!");
    return false;
  }
  if (configFile.available()) {
    patches->loadPatch(configFile.readStringUntil('\n'));
  }
  configFile.close();

  if (!tuningFile) {
    Serial.println("Error, failed to open tuning file for reading!");
    return false;
  }

  for(byte count = 0; count < NUMBER_OF_TUNE_RANGES; ++count) {
    audio->tuneRanges[count].name = tuningFile.readStringUntil('\n');
    audio->tuneRanges[count].stepsPerNote = tuningFile.parseFloat();
    tuningFile.read(); // Newline    
    audio->tuneRanges[count].rangeHigh = tuningFile.parseInt();
    tuningFile.read(); // Newline

    #if DEBUG
      Serial.println(audio->tuneRanges[count].name);
      Serial.println(audio->tuneRanges[count].stepsPerNote);
      Serial.println(audio->tuneRanges[count].rangeHigh);
    #endif
  }
  tuningFile.close();

  return true;
}

void Config::saveConfig() {
  noInterrupts();
  Serial.print("Saving Config...");
  sd->chdir();
  //int return_value;

  Serial.println("Erasing Config File");
  if (!sd->remove(CONFIG_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }

  Serial.println("Erasing Tuning File");
  if (!sd->remove(TUNING_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }


  configFile = sd->open(CONFIG_FILE_NAME, FILE_WRITE);
  if (!configFile) {
    Serial.println("Error, failed to open config file for writing!");
    while(1) yield();
  }

  Serial.print("Writing initial patch ");
  Serial.println(patches->patch.name);
  configFile.print(patches->patch.name);
  configFile.print('\n');
  configFile.close();
  Serial.println("Done!");

  tuningFile = sd->open(TUNING_FILE_NAME, FILE_WRITE);
  Serial.print("Writing tuning values...");
  //configFile.println(audio->stepsPerNote, 6);

  for(byte count = 0; count < NUMBER_OF_TUNE_RANGES; ++count) {
    tuningFile.print(audio->tuneRanges[count].name);
    tuningFile.print('\n');
    //tuningFile.printf("%2.2f\n", audio->tuneRanges[count].stepsPerNote, 2);
    //tuningFile.printf("%2.2f\n", audio->tuneRanges[count].tuningAdjustment, 2);
    tuningFile.printf("%2.2f\n", audio->tuneRanges[count].stepsPerNote);
    //tuningFile.printf("%2.2f\n", audio->tuneRanges[count].tuningAdjustment);
    tuningFile.printf("%d\n", audio->tuneRanges[count].rangeHigh);
  }
  tuningFile.close();
  Serial.println("Done!");
  interrupts();
  
}

void Config::printMenu() {
  display->clearDisplay();
  display->setCursor(0, 0);
  display->setTextColor(WHITE, BLACK);
  display->println("Config\n-----");
  for (int i = 0; i < CONFIG_MENU_SIZE; i++) {
    if(menuSelection == i)
      display->setTextColor(BLACK, WHITE);
    else
      display->setTextColor(WHITE, BLACK);
    display->println(configMenuList[i]);
  }
  display->display();
}

void Config::activate () {
  loadConfig();
  printMenu();
}

byte Config::run() {
  udRotate = upDownEncoder->rotate();
  if (udRotate == ENCODER_TURNED_LEFT) {
    if(menuSelection == 0)
      menuSelection = CONFIG_MENU_SIZE - 1;
    else
      menuSelection = --menuSelection % CONFIG_MENU_SIZE;
    printMenu();
  }
  else if (udRotate == CONFIG_MENU_SIZE) {
    menuSelection = ++menuSelection % CONFIG_MENU_SIZE;
    printMenu();
  }

  if(leftRightEncoder->push()) {
    switch (menuSelection) {
      case CONFIG_MENU_TUNING:
        return TUNING_MODULE;
      default:
        display->clearDisplay();
        display->setCursor(0, 0);
        display->setTextColor(WHITE, BLACK);
        display->print("Saving config...");
        saveConfig();
        display->println("Saved");
        display->display();
        delay(UI_PAUSE);
        activate();
        return CONFIG_MENU;
    }
  }
  // Check for long press of left encoder to go back
  return checkGotoModule(MAIN_MENU);
}