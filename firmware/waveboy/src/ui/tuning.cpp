#include "tuning.h"

Tuning::Tuning(Adafruit_SSD1306* display, SdFs* sd, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Audio* audio) {
  this->display = display;
  this->sd = sd;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->audio = audio;
  tuneCount = 0;
}

bool Tuning::loadTunings() {
  tuningFile = sd->open(TUNING_FILE_NAME, FILE_READ);

  if (!tuningFile) {
    Serial.println("Error, failed to open tuning file for reading!");
    return false;
  }

  for(byte count = 0; count < NUMBER_OF_TUNE_RANGES; ++count) {
    audio->tuneRanges[count].name = tuningFile.readStringUntil('\n');
    
    audio->tuneRanges[count].stepsPerNote = tuningFile.parseFloat();
    tuningFile.read(); // Newline
    
    audio->tuneRanges[count].rangeHigh = tuningFile.parseInt();
    tuningFile.read(); // Newline

    #if DEBUG
      Serial.println(audio->tuneRanges[count].stepsPerNote);
      Serial.println(audio->tuneRanges[count].name);
      Serial.println(audio->tuneRanges[count].rangeHigh);
    #endif
  }
  tuningFile.close();

  return true;
}

void Tuning::saveTunings() {
  noInterrupts();
  Serial.print("Saving Config...");
  sd->chdir();
  //int return_value;

  Serial.println("Erasing Tuning File");
  if (!sd->remove(TUNING_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }

  tuningFile = sd->open(TUNING_FILE_NAME, FILE_WRITE);
  Serial.print("Writing tuning values...");
  //configFile.println(audio->stepsPerNote, 6);

  for(byte count = 0; count < NUMBER_OF_TUNE_RANGES; ++count) {
    tuningFile.print(audio->tuneRanges[count].name);
    tuningFile.print('\n');
    tuningFile.printf("%2.2f\n", audio->tuneRanges[count].stepsPerNote);
    tuningFile.printf("%d\n", audio->tuneRanges[count].rangeHigh);
  }
  tuningFile.close();
  Serial.println("Done!");
  interrupts();
}

void Tuning::printMenu() {
  display->clearDisplay();
  display->setCursor(0, 0);
  display->setTextColor(WHITE, BLACK);

  if(mode == TUNING_MODE_RANGE) 
    display->print("Tuning : Range\n--------------\n");
  else
    display->print("Tuning : Steps\n--------------\n");
  display->setTextSize(2);
  display->print(audio->tuneRanges[tuneCount].name.c_str());
  if(mode == TUNING_MODE_RANGE) 
    display->printf(":%d\n", audio->tuneRanges[tuneCount].rangeHigh);
  else
    display->printf(":%1.2f\n", audio->tuneRanges[tuneCount].stepsPerNote);

  display->setTextSize(1);
  display->printf("\nRaw ADC: %d\nEst. Pitch: %2.2f\nNote: %2.2f", 
    audio->getVoctADC(), audio->getPitch(), audio->getNote());
  display->display();
  adcDisplayTimeout = millis() + TUNING_MODE_ADC_UPDATE_MS;
}

void Tuning::activate () {
  loadTunings();
  printMenu();
}

byte Tuning::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  if(adcDisplayTimeout < millis())
    printMenu();

  if (udRotate == ENCODER_TURNED_LEFT) {
    if(tuneCount == 0)
      tuneCount = NUMBER_OF_TUNE_RANGES - 1;
    else
      tuneCount = --tuneCount % NUMBER_OF_TUNE_RANGES;
    printMenu();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    tuneCount = ++tuneCount % NUMBER_OF_TUNE_RANGES;
    printMenu();
  }

/*
  lrRotate = leftRightEncoder->rotate();
  if (lrRotate == ENCODER_TURNED_LEFT) {
    //audio->stepsPerNote = audio->stepsPerNote - TUNING_ADJUSTMENT_INTERVAL;
    printMenu();
  }
  else if (lrRotate == ENCODER_TURNED_RIGHT) {
    //audio->stepsPerNote = audio->stepsPerNote + TUNING_ADJUSTMENT_INTERVAL;
    printMenu();
  }
*/

  if (lrRotate == ENCODER_TURNED_LEFT) {
    switch (mode) {
      case TUNING_MODE_RANGE:
        audio->tuneRanges[tuneCount].rangeHigh = --audio->tuneRanges[tuneCount].rangeHigh;
        break;
      default:
        audio->tuneRanges[tuneCount].stepsPerNote = audio->tuneRanges[tuneCount].stepsPerNote - STEPS_PER_NOTE_ADJUSTMENT_INTERVAL;
    }
    printMenu();   
  }
  else if (lrRotate == ENCODER_TURNED_RIGHT) {
    switch (mode) {
      case TUNING_MODE_RANGE:
        audio->tuneRanges[tuneCount].rangeHigh = ++audio->tuneRanges[tuneCount].rangeHigh;
        break;
      default:
        audio->tuneRanges[tuneCount].stepsPerNote = audio->tuneRanges[tuneCount].stepsPerNote + STEPS_PER_NOTE_ADJUSTMENT_INTERVAL;
    }
    printMenu();    
  }

  if(upDownEncoder->push()) {
    if(mode == TUNING_MODE_COUNT - 1)
      mode = 0;
    else
      ++mode;
    printMenu();
  }

  if(leftRightEncoder->push()) {
    display->clearDisplay();
    display->setCursor(0, 0);
    display->setTextColor(WHITE, BLACK);
    display->print("Saving tunings...");
    saveTunings();
    display->println("Saved");
    display->display();
    delay(UI_PAUSE);
    return TUNING_MODULE;
  }
  // Check for long press of left encoder to go back to config
  return checkGotoModule(CONFIG_MENU);
}