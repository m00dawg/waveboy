/* UI Module which shows the main menu */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../defines.h"
#include "ui.h"

#define MAIN_MENU_WAVE 0
#define MAIN_MENU_NOISE 1
#define MAIN_MENU_SAMPLER 2
//#define MAIN_MENU_TRACKER 3
#define MAIN_MENU_VOLUME 3
#define MAIN_MENU_SCREENSAVER 4
#define MAIN_MENU_CONFIG 5
#define MAIN_MENU_TRACKER 99
#define MAIN_MENU_SIZE 6

class MainMenu: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    String mainMenuList[MAIN_MENU_SIZE] = {
      "Wave Mode",
      "Noise Mode",
      "Sampler",
      "Volume Env",
      //"Tracker",
      "Screensaver",
      "Config"
    };
    byte menuSelection = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    void printMenu();
  public:
    MainMenu(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder);
    void activate();
    byte run();
};