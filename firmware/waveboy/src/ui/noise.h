/* Noise Module */
#pragma once

#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../defines.h"
#include "../../functions.h"
#include "ui.h"
#include "../../patches.h"
#include "../noisetypes/noisetype.h"
#include "../noisetypes/gbuzz.h"
#include "../noisetypes/modern.h"
#include "../noisetypes/rng.h"
#include "../noisetypes/flip.h"
#include "../noisetypes/shift.h"
#include "../noisetypes/drip.h"

#define NUMBER_OF_NOISE_TYPES 6
#define NOISE_MODE_GB         0
#define NOISE_MODE_MODERN     1
#define NOISE_MODE_RNG        2
#define NOISE_MODE_FLIP       3
#define NOISE_MODE_SHIFT      4
#define NOISE_MODE_DRIP       5

#define CV_PARAM_X            112
#define CV_PARAM_Y            48
#define CV_LABEL_X            CV_PARAM_X - (2*8)
#define CV_LABEL_Y            48

#define KNOB_PARAM_X          112
#define KNOB_PARAM_Y          56
#define KNOB_LABEL_X          KNOB_PARAM_X - (4*8)
#define KNOB_LABEL_Y          56

#define DISPLAY_TIMEOUT_MS    3000

class Noise: public UI {
  private:
    Patches* patches;
    NoiseType* noiseTypes[NUMBER_OF_NOISE_TYPES];
    byte lrRotate = 0;
    byte udRotate = 0;
    byte mode = 0;
    byte cvValue = 0;
    byte cvRead = 0;
    int cvRaw = 0;
    unsigned long displayTimeout = 0;
    void drawDisplay();
    void updateDisplayValues();
    void readCV();

  public:
    Noise(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches);
    
    void activate();
    byte run();
    uint16_t next();
    uint16_t current();
    void gate();
};