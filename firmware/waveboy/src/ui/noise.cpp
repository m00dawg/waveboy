/* Noise Module 
 *
 * Left Knob Turn: Change noise generator
 * Left Knob Press: Reset noise generator
 * Right Knob Short Press: Display/update CV/knob values
 * Right Knob Long Press: Go back to main menu
 */
#include "noise.h"

Noise::Noise(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;

  /*
   * This took a while to figure out.
   * We're using new with a pointer here because we wan't
   * the damn object to exist beyond the scope. This means
   * That we have to delete these (but in the context of how this works
   * we won't ever need to do that)
   * 
   * See: https://stackoverflow.com/questions/2988273/c-pointer-to-objects
   */
  GBuzz *gb = new GBuzz();
  Modern *modern = new Modern();
  RNG *rng = new RNG();
  Flip *flip = new Flip();
  Shift *shift = new Shift();
  Drip *drip = new Drip();
  this->noiseTypes[NOISE_MODE_GB] = gb;
  this->noiseTypes[NOISE_MODE_MODERN] = modern;
  this->noiseTypes[NOISE_MODE_RNG] = rng;
  this->noiseTypes[NOISE_MODE_FLIP] = flip;
  this->noiseTypes[NOISE_MODE_SHIFT] = shift;
  this->noiseTypes[NOISE_MODE_DRIP] = drip;
}

void Noise::drawDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("Noise\n-----");
  //display->setTextSize(2);
  for(byte i = 0; i < NUMBER_OF_NOISE_TYPES ; ++i)
  {
    /*
    if(i == mode)
      display->setTextColor(BLACK, WHITE);
    else
      display->setTextColor(WHITE, BLACK);
    */
    display->print(noiseTypes[i]->getName());
    if(i == mode)
      display->print(" <--");
    display->println();
  }
  //display->setTextSize(1);
  display->display();
  displayTimeout = millis() + DISPLAY_TIMEOUT_MS;
}

void Noise::updateDisplayValues() {
  char hexNumber[2];

  display->setCursor(CV_LABEL_X, CV_LABEL_Y);
  display->println("CV:");
  display->setCursor(KNOB_LABEL_X, KNOB_LABEL_Y);
  display->println("Knob:");
  display->setCursor(KNOB_PARAM_X, KNOB_PARAM_Y);
  sprintf(hexNumber, "%02X", noiseTypes[mode]->getKnobParam());
  display->print(hexNumber);
  display->setCursor(CV_PARAM_X, CV_PARAM_Y);
  sprintf(hexNumber, "%02X", noiseTypes[mode]->getCVParam());
  display->print(hexNumber);
  display->display();
}

void Noise::activate () {
  readCV();
  drawDisplay();
}

byte Noise::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  if (udRotate == ENCODER_TURNED_RIGHT)
  {
    if(mode == NUMBER_OF_NOISE_TYPES - 1)
      mode = 0;
    else
      ++mode;
    drawDisplay();
    return NOISE_MODULE;
  }
  if (udRotate == ENCODER_TURNED_LEFT)
  {
    if(mode == 0)
      mode = NUMBER_OF_NOISE_TYPES - 1;
    else
      --mode;
    drawDisplay();
    return NOISE_MODULE;
  }

  if (lrRotate == ENCODER_TURNED_RIGHT)
  {
    noiseTypes[mode]->incKnobParam();
    updateDisplayValues();
    return NOISE_MODULE;
  }
  if (lrRotate == ENCODER_TURNED_LEFT)
  {
    noiseTypes[mode]->decKnobParam();
    updateDisplayValues();
    return NOISE_MODULE;
  }

  if(upDownEncoder->push())
  {
    updateDisplayValues();
    return NOISE_MODULE;
  }

  if(leftRightEncoder->push())
  {
    noiseTypes[mode]->reset();
    return NOISE_MODULE;
  }
  
  // Remove CV/Knob params after timeout
  if(displayTimeout < millis())
    drawDisplay();

  // Finally if nothing else happened, read CV
  readCV();

  // Check for long press of left encoder to go back
  return checkBackToPreviousModule();
}

uint16_t Noise::next() {
  return noiseTypes[mode]->next();
}

// Grab current value (without updating lfsr)
// Used for the step sequencer
uint16_t Noise::current() {
  return noiseTypes[mode]->current();
}

// Send the current noise type a gate event.
void Noise::gate() {
  noiseTypes[mode]->gate();
}

void Noise::readCV() {
  cvRaw = readADC0(FRAME);

  cvRaw = constrain(cvRaw, FRAME_ADC_MIN, FRAME_ADC_MAX);
  // Inverted because incoming CV value is inverted
  cvRead = map(cvRaw, FRAME_ADC_MIN, FRAME_ADC_MAX, 255, 0);
  if(cvRead != cvValue)
  {
    cvValue = cvRead;
    noiseTypes[mode]->setCVParam(cvValue);
  }
}
