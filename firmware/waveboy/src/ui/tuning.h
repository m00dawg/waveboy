
/* Load and save configuration options from the file on the SD card */
#pragma once
#include <Arduino.h>
#include <SdFat.h>
#include "../../defines.h"
#include "../../functions.h"
#include "ui.h"
#include "../../audio.h"

#define TUNING_MODE_COUNT 2
#define TUNING_MODE_RANGE 0
#define TUNING_MODE_STEPS 1

#define TUNING_MODE_ADC_UPDATE_MS    2000 // 2 seconds

class Tuning: public UI{
  private:
    Adafruit_SSD1306* display;
    SdFs* sd;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    Audio* audio;
    FsFile tuningFile;
    byte mode = 1;
    byte tuneCount = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    unsigned long adcDisplayTimeout = 0;
    void printMenu();
  
  public:
    // Constructor
    Tuning(Adafruit_SSD1306* display, SdFs* sd, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Audio* audio);

    bool loadTunings();
    void saveTunings();
    void activate();
    byte run();
};