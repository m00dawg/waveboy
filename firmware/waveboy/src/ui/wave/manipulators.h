/* Module which lists Manipulators */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "manipulate.h"
#include "../../manipulators/manipulator.h"
#include "../../manipulators/clip.h"
#include "../../manipulators/wrap.h"
#include "../../manipulators/move.h"
#include "../../manipulators/jitter.h"

#define NUMBER_OF_MANIPULATORS 4
#define CLIP_MANIPULATOR 0
#define WRAP_MANIPULATOR 1
#define MOVE_MANIPULATOR 2
#define JITTER_MANIPULATOR 3

class Manipulators: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    Manipulate* manipulate;
    Manipulator* manipulators[NUMBER_OF_MANIPULATORS];
    byte mode = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    void drawDisplay();
  public:
    Manipulators(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Manipulate* manipulate);
    void activate();
    byte run();
};