/* Module which is used to navigate and load patches */
#include "loadpatch.h"

LoadPatch::LoadPatch(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, SdFs* sd) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
  this->sd = sd;
}

void LoadPatch::getPatchCount() {
  SdFile file;
  SdFile dirFile;
  numPatches = 0;

  if (!dirFile.open(WAVES_DIRECTORY, O_RDONLY)) {
    sd->errorHalt("open root failed");
    Serial.println("Open directory failed");
  }
  while (file.openNext(&dirFile, O_RDONLY)) {
    if (!file.isSubDir() && !file.isHidden()) {
      ++numPatches;
    }
  }
  Serial.println("Patches found: ");
  Serial.print(numPatches);
}

void LoadPatch::listPatches() {
  SdFile file;
  SdFile dirFile;
  char filename[16];
  byte patchCount = 0;
  byte menuCount = 0;

  display->clearDisplay();
  display->setCursor(0, 0);
  display->setTextColor(WHITE, BLACK);
  display->println("Load Patch\n----------");
  if (!dirFile.open(WAVES_DIRECTORY, O_RDONLY)) {
    sd->errorHalt("open failed");
    Serial.println("Open directory failed");
  }

  while (file.openNext(&dirFile, O_RDONLY)) { 
    if(menuCount == WAVE_NUM_PATCHES_IN_MENU)
      break;
    if (!file.isSubDir() && !file.isHidden()) {
      if(patchCount >= dirStartPosition ) {
        file.getName(filename, 16);
        if (patchCount == fileIndex) {
          display->setTextColor(BLACK, WHITE);
          patches->loadPatch(filename);
        }
        else
          display->setTextColor(WHITE, BLACK);
        display->println(filename);
        ++menuCount;
      }
      ++patchCount;
    }
  }
  display->setTextColor(WHITE, BLACK);
  display->display();
}

void LoadPatch::checkUDKnob() {
  udRotate = upDownEncoder->rotate();

  if (udRotate == ENCODER_TURNED_LEFT) {
    if(fileIndex == 0)
    {
      fileIndex = numPatches - 1;
      menuPosition = WAVE_NUM_PATCHES_IN_MENU - 1;
      dirStartPosition = numPatches - WAVE_NUM_PATCHES_IN_MENU;
    }
    else {
      fileIndex = --fileIndex;
      if(menuPosition == 0)
        --dirStartPosition;
      else
        --menuPosition;
    }
    listPatches();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    fileIndex = ++fileIndex;
    if(menuPosition == WAVE_NUM_PATCHES_IN_MENU - 1)
      ++dirStartPosition;
    else
      ++menuPosition;
    if(fileIndex == numPatches)
    {
      dirStartPosition = 0;
      menuPosition = 0;
      fileIndex = 0;
    }
    listPatches();
  }
}

void LoadPatch::activate() {
  getPatchCount();
  listPatches();  
}


byte LoadPatch::run() {
  checkUDKnob();

  if(upDownEncoder->push())
    return WAVE_MENU;

  if(leftRightEncoder->push())
    return SHOW_WAVE;    

  // Check for long press of left encoder to go back
  return checkGotoModule(WAVE_MENU);
}
