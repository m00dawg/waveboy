/* Module which shows the wave menu */
#include "wavemenu.h"

WaveMenu::WaveMenu(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
}

void WaveMenu::printMenu() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("WAVE MENU\n----");
  for (int i = 0; i < WAVE_MENU_SIZE; i++) {
    if(menuSelection == i)
      display->setTextColor(BLACK, WHITE);
    else
      display->setTextColor(WHITE, BLACK);
    display->println(waveMenuList[i]);
  }
  display->display();
}

void WaveMenu::activate () {
  printMenu();
}

byte WaveMenu::run() {
  udRotate = upDownEncoder->rotate();
  if (udRotate == ENCODER_TURNED_LEFT) {
    if(menuSelection == 0)
      menuSelection = WAVE_MENU_SIZE - 1;
    else
      menuSelection = --menuSelection % WAVE_MENU_SIZE;
    printMenu();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    menuSelection = ++menuSelection % WAVE_MENU_SIZE;
    printMenu();
  }

  if(upDownEncoder->push())
    return MAIN_MENU;

  if(leftRightEncoder->push()) {
    switch (menuSelection) {
      case WAVE_MENU_SHOW_WAVE:
        return SHOW_WAVE;
      case WAVE_MENU_EDIT_WAVE:
        return EDIT_WAVE;
      case WAVE_MENU_LOAD_PATCH:
        return WAVE_LOAD_PATCH;
      case WAVE_MENU_SAVE_PATCH:
        return WAVE_SAVE_PATCH;
      case WAVE_MENU_MANIPULATE_WAVE:
        return WAVE_MANIPULATORS;
      default:
        return MAIN_MENU;
    }
  }
  
  // Check for long press of left encoder to go back
  return checkGotoModule(MAIN_MENU);
}