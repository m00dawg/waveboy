/* Manipulators Module 
 *
 */
#include "manipulators.h"

Manipulators::Manipulators(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Manipulate* manipulate) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->manipulate = manipulate;

  /*
   * This took a while to figure out.
   * We're using new with a pointer here because we wan't
   * the damn object to exist beyond the scope. This means
   * That we have to delete these (but in the context of how this works
   * we won't ever need to do that)
   * 
   * See: https://stackoverflow.com/questions/2988273/c-pointer-to-objects
   */
  Clip *clip = new Clip();
  Wrap *wrap = new Wrap();
  Move *move = new Move();
  Jitter *jitter = new Jitter();
  this->manipulators[CLIP_MANIPULATOR] = clip;
  this->manipulators[WRAP_MANIPULATOR] = wrap;
  this->manipulators[MOVE_MANIPULATOR] = move;
  this->manipulators[JITTER_MANIPULATOR] = jitter;
}

void Manipulators::drawDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("Manipulators\n------------");
  //display->setTextSize(2);
  for(byte i = 0; i < NUMBER_OF_MANIPULATORS ; ++i)
  {
    /*
    if(i == mode)
      display->setTextColor(BLACK, WHITE);
    else
      display->setTextColor(WHITE, BLACK);
    */
    display->printf("%s", manipulators[i]->getName());
    if(i == mode)
      display->print(" <--");
    display->println();
  }
  //display->setTextSize(1);
  display->display();
}

void Manipulators::activate () {
  drawDisplay();
}

byte Manipulators::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  if (udRotate == ENCODER_TURNED_RIGHT)
  {
    if(mode == NUMBER_OF_MANIPULATORS - 1)
      mode = 0;
    else
      ++mode;
    drawDisplay();
  }
  if (udRotate == ENCODER_TURNED_LEFT)
  {
    if(mode == 0)
      mode = NUMBER_OF_MANIPULATORS - 1;
    else
      --mode;
    drawDisplay();
  }

  if(upDownEncoder->push())
  {
    //updateDisplayValues();
    return WAVE_MENU;
  }

  if(leftRightEncoder->push())
  {
    //noiseTypes[mode]->reset();
    manipulate->setManipulator(manipulators[mode]);
    return MANIPULATE_WAVE;
  }
  
  // Check for long press of left encoder to go back
  return checkGotoModule(WAVE_MENU);
}

