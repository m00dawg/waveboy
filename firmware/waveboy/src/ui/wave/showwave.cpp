#include "showwave.h"

#define WAVE_CURSOR_Y 52
#define STEP_CURSOR_Y 48

const int static pixelSizeX = OLED_SCREEN_X / WAVE_SLOTS - 1;
const int static pixelSizeY = OLED_SCREEN_Y / WAVE_RANGE_MAX - 1;

ShowWave::ShowWave(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Gate* gate) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
  this->gate = gate;
}

// This is the fun wave display which is disabled for now because of the OLED display updates 
// being so damn slow.
/*
void ShowWave::updateDisplay() {
  // The current values are to check if they were changed outside of this module.
  // Namely, if CV/Trigger updates happen which changes the gate/patch.
  // This lets us know if we need to update the display to reflect said changes.
  currentWave = patches->currentWaveNumber;
  currentStep = gate->currentStep;
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->clearDisplay();
  display->printf("Fr: %02d | St: %02d %c\n", patches->currentWaveNumber, gate->currentStep, gate->direction);  
  //drawWaveCursor();
  drawStepCursor();
  drawWave(patches->patch.waves[patches->currentWaveNumber]);
  display->display();
}
*/

void ShowWave::updateDisplay() {
  // The current values are to check if they were changed outside of this module.
  // Namely, if CV/Trigger updates happen which changes the gate/patch.
  // This lets us know if we need to update the display to reflect said changes.
  currentWave = patches->currentWaveNumber;
  currentStep = gate->currentStep;
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->clearDisplay();
  display->println("Wave Mode\n---------");
  display->setTextSize(2);
  display->println(patches->patch.name);
  display->setTextSize(1);
  display->printf("Step Dir: %c", gate->direction);
  display->display();
}

void ShowWave::activate() {
  updateDisplay();
}

byte ShowWave::run() {
  // If something external changed, update the display
  /* Display too slow
  if(currentWave != patches->currentWaveNumber || 
     currentStep != gate->currentStep) {
    //updateDisplay();
    updateDisplayFlag = true;
  }
  */

  // Pressing the LR encoder (the right one) iterates through the step modes
  if(leftRightEncoder->push()) {
    gate->cycleStepDirection();
    //updateDisplay();
    updateDisplayFlag = true;
  }

  if (updateDisplayFlag) {
    updateDisplayFlag = false;
    updateDisplay();
  }

  // Check for long press of left encoder to go back
  return checkGotoModule(WAVE_MENU);
}

// Draw the step cursor above the corresponding wave position
// This is the position of the stepped output
void ShowWave::drawStepCursor() {
  int startX = currentStep * pixelSizeX;
  int startY = STEP_CURSOR_Y;
  drawPixel(startX , startY);
}

