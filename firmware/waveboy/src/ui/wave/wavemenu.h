/* Module which shows the main menu */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../../patches.h"

#define WAVE_MENU_SHOW_WAVE 0
#define WAVE_MENU_EDIT_WAVE 1
#define WAVE_MENU_MANIPULATE_WAVE 2
#define WAVE_MENU_LOAD_PATCH 3
#define WAVE_MENU_SAVE_PATCH 4
#define WAVE_MENU_SIZE 5

class WaveMenu: public UI {
  private:
    Adafruit_SSD1306* display;
    Patches* patches;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    String waveMenuList[WAVE_MENU_SIZE] = {
      "Show Wave",
      "Edit Wave",
      "Manipulate Wave",
      "Load Patch",
      "Save Patch",
    };
    byte menuSelection = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    void printMenu();
  public:
    WaveMenu(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches);
    void activate();
    byte run();
};