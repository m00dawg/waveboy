/* Module to Manipulate wave by selected manipulator */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../../../patches.h"
#include "../ui.h"
#include "../../manipulators/manipulator.h"
#include "../../manipulators/clip.h"

class Manipulate: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    Patches* patches;
    Manipulator* manipulator;
    byte originalWave[WAVE_SLOTS];
    bool mode = false;          // false = Manipulate, true = Edit Frame
    byte lrRotate = 0;
    byte udRotate = 0;
    //int8_t value1 = 0;
    //int8_t value2 = 0;
    void drawDisplay();
    void reset();
  public:
    Manipulate(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches);
    void setManipulator(Manipulator* manipulator);
    void activate();
    byte run();
};