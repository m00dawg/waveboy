/* Manipulate Wave Module 
 *
 */
#include "manipulate.h"

Manipulate::Manipulate(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
}

void Manipulate::setManipulator(Manipulator* manipulator) {
    this->manipulator = manipulator;
}

void Manipulate::drawDisplay() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->printf("%s / Fr:", (manipulator->getName()));
  if(mode)
    display->setTextColor(BLACK, WHITE);
  display->printf("%02d\n", patches->currentWaveNumber);
  display->setTextColor(WHITE, BLACK);
  display->println("----------------");
  drawWave(patches->patch.waves[patches->currentWaveNumber]);
  display->display();
}

void Manipulate::activate () {
  // Copy the current wave to the original
  memcpy(originalWave, patches->patch.waves[patches->currentWaveNumber], WAVE_SLOTS);
  manipulator->reset();
  //value1 = 0;
  //value2 = 0;
  drawDisplay();
}

/* Turn back to the original wave and zero out manipulator values */
void Manipulate::reset() {
  // Copy the original wave back over to the current.
  memcpy(patches->patch.waves[patches->currentWaveNumber], originalWave, WAVE_SLOTS);
  patches->updateWave();
  manipulator->reset();
  //value1 = 0;
  //value2 = 0;
  drawDisplay();
}

byte Manipulate::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  if (udRotate == ENCODER_TURNED_RIGHT)
  {
    //++value1;
    //manipulator->manipulate(1, 0, patches->patch.waves[patches->currentWaveNumber]);
    manipulator->increment1(patches->patch.waves[patches->currentWaveNumber]);
    patches->updateWave();
    drawDisplay();
  }
  else if (udRotate == ENCODER_TURNED_LEFT)
  {
    //--value1;
    manipulator->decrement1(patches->patch.waves[patches->currentWaveNumber]);
    patches->updateWave();
    drawDisplay();
  }
  else if (lrRotate == ENCODER_TURNED_RIGHT)
  {
    if(mode) {
      patches->changeFrame(++patches->currentWaveNumber % NUM_WAVES);
      activate();
    }
    else {
      manipulator->increment2(patches->patch.waves[patches->currentWaveNumber]);
      patches->updateWave();
      drawDisplay();
    }
  }
  else if (lrRotate == ENCODER_TURNED_LEFT)
  {
    if(mode) {
      patches->changeFrame(--patches->currentWaveNumber % NUM_WAVES);
      activate();
    }
    else {
      manipulator->decrement2(patches->patch.waves[patches->currentWaveNumber]);
      patches->updateWave();
      drawDisplay();
    }
  }

  if(upDownEncoder->push()) {
    mode = !mode;
    drawDisplay();
  }


  if(leftRightEncoder->push())
  {
    reset();
  }
  

  // Check for long press of left encoder to go back
  return checkGotoModule(WAVE_MANIPULATORS);
}


