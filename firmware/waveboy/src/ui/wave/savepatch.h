/* Module which is used to name and save a patch */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../../patches.h"


class SavePatch: public UI {
  private:
    Patches* patches;
    SdFs* sd;
    byte lrRotate = 0;
    byte udRotate = 0;
    byte startPatch = 0;
    byte selectedPatch = 0;
    byte namePosition = 0;
    const byte numPatchRows = 6;
    void drawDisplay();
    void updatePatchName();
  public:
    SavePatch(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, SdFs* sd);
    void activate();
    byte run();
};