/* Module which is used to navigate and load patches */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../../patches.h"

#define WAVE_NUM_PATCHES_IN_MENU 6

class LoadPatch: public UI {
  private:
    Patches* patches;
    SdFs* sd;
    byte lrRotate = 0;
    byte udRotate = 0;
    byte startPatch = 0;
    byte selectedPatch = 0;
    byte numPatches = 0;
    byte menuPosition = 0;
    byte fileIndex = 0;
    byte dirStartPosition = 0;
    const byte numPatchRows = 6;
    void getPatchCount();
    void listPatches();
    void checkUDKnob();
  public:
    LoadPatch(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, SdFs* sd);
    void activate();
    byte run();
};