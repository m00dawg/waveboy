/* UI Module for editing waves */
// Main difference from Show Wave is this module 
// doesn't read CV in so the user can flip waves manually
// It's also how to access the Manipulators
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../../patches.h"
#include "../../../gate.h"

class EditWave: public UI {
  private:
    Patches* patches;
    Gate* gate;
    byte currentWave = 0;
    byte waveIndex = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    bool updateDisplayFlag = false;
    bool mode = false;          // false = Edit Wave, true = Edit Frame
    void updateDisplay();
    void updateWave();
    void drawWaveCursor();

  public:
    EditWave(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Gate* gate);
    void activate();
    byte run();
};