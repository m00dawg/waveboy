#include "editwave.h"

#define WAVE_CURSOR_Y 52
#define STEP_CURSOR_Y 48

// Not quite sure why I did it this way vs Definese but there was a rason.
const int static pixelSizeX = OLED_SCREEN_X / WAVE_SLOTS - 1;
const int static pixelSizeY = OLED_SCREEN_Y / WAVE_RANGE_MAX - 1;

EditWave::EditWave(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Gate* gate) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
  this->gate = gate;
}

void EditWave::updateDisplay() {
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->clearDisplay();
  display->print("Edit Mode - Fr: ");
  if(mode)
    display->setTextColor(BLACK, WHITE);
  display->printf("%02d\n", patches->currentWaveNumber);
  display->setTextColor(WHITE, BLACK);
  drawWaveCursor();
  drawWave(patches->patch.waves[patches->currentWaveNumber]);
  display->display();
}

void EditWave::updateWave() {
  // Update wave index (cursor)
  lrRotate = leftRightEncoder->rotate();
  if (lrRotate == ENCODER_TURNED_LEFT) {
    // Edit frame mode
    if(mode)
      patches->changeFrame(--patches->currentWaveNumber % NUM_WAVES);
    else
      waveIndex = --waveIndex % WAVE_SLOTS;
    updateDisplayFlag = true;
  }
  else if (lrRotate == ENCODER_TURNED_RIGHT) {
    if(mode)
      patches->changeFrame(++patches->currentWaveNumber % NUM_WAVES);
    else
      waveIndex = ++waveIndex % WAVE_SLOTS;
    updateDisplayFlag = true;
  }

  // Update wave value
  udRotate = upDownEncoder->rotate();
  // Down
  if (udRotate == ENCODER_TURNED_LEFT) {
    patches->decWaveValue(waveIndex);
    updateDisplayFlag = true;
  }
  // Up
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    patches->incWaveValue(waveIndex);
    updateDisplayFlag = true;
  }
}

void EditWave::activate() {
  updateDisplay();
}

byte EditWave::run() {
  updateWave();

  // Pressing UD switches the edit mode from frame to wave
  if(upDownEncoder->push()) {
    mode = !mode;
    updateDisplayFlag = true;
  }

  if (updateDisplayFlag) {
    updateDisplayFlag = !updateDisplayFlag;
    updateDisplay();
  }

  // Check for long press of left encoder to go back
  return checkGotoModule(WAVE_MENU);
}

// Draw the wave cursor above the corresponding wave position
// This is the position where we can update the wave via the LR knob
void EditWave::drawWaveCursor() {
  int startX = waveIndex * pixelSizeX;
  int startY = WAVE_CURSOR_Y;
  drawPixel(startX , startY);
}

