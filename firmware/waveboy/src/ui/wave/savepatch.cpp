/* Module which is used to navigate and load patches */
#include "../../../functions.h"  
#include "savepatch.h"


SavePatch::SavePatch(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, SdFs* sd) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
  this->sd = sd;
}

void SavePatch::drawDisplay() {  
  display->clearDisplay();
  display->setCursor(0, 0);
  display->setTextColor(WHITE, BLACK);
  display->println("Save Patch\n----------");
  for (byte i = 0 ; i < PATCH_NAME_LENGTH; ++i)
  {
    if(namePosition == i)
    {
      display->setTextColor(BLACK, WHITE);
    }
    else
      display->setTextColor(WHITE, BLACK);
    if(patches->patch.name[i] != '\0' && patches->patch.name[i] != '\n')
      display->print(patches->patch.name[i]);
    else
      break;
  }

  display->println();
  display->println();
  display->println("Press and hold");
  display->println("right knob");
  display->println("to save patch.");

  /*
  display->setTextColor(WHITE, BLACK);
  display->println();
  display->print("Dest: ");
  sprintf(hexNumber, "%02X", patches->currentPatch);
  display->println(hexNumber);
  */

  // Show the current selected char in larger size
  // x, y, width, height, color
  display->drawRect(OLED_SCREEN_X - 33, OLED_SCREEN_Y - 33, 20, 26, WHITE);
  display->setCursor(OLED_SCREEN_X - 32, OLED_SCREEN_Y - 32);
  display->setTextSize(3);
  display->print(patches->patch.name[namePosition]);
  display->setTextSize(1);

  display->display();
}


void SavePatch::activate()
{
  drawDisplay();  
}

void SavePatch::updatePatchName()
{
    // Up/Down (Left Knob)
  if (udRotate == ENCODER_TURNED_RIGHT)
  {
    patches->patch.name[namePosition] 
      = characterLookup(++patches->patch.name[namePosition]);
    drawDisplay();
  }
  else if (udRotate == ENCODER_TURNED_LEFT)
  {
    patches->patch.name[namePosition] 
      = characterLookup(--patches->patch.name[namePosition]);
    drawDisplay();
  }
  // Left/Right (Right Knob)
  if (lrRotate == ENCODER_TURNED_RIGHT)
  {
    ++namePosition;
    if(namePosition == PATCH_NAME_LENGTH)
      namePosition=0;
    drawDisplay();
  }
  else if (lrRotate == ENCODER_TURNED_LEFT)
  {
    if(namePosition == 0)
      namePosition = PATCH_NAME_LENGTH - 1;
    else
      --namePosition;
    drawDisplay();
  }
}


byte SavePatch::run() {
  udRotate = upDownEncoder->rotate();
  lrRotate = leftRightEncoder->rotate();

  updatePatchName();

  //   byte i = rotary.pushLong(1000);
  // Long push is save
  if(leftRightEncoder->pushLong(BUTTON_LONG_PRESS))
  {
    display->clearDisplay();
    display->setCursor(0, 0);
    display->setTextColor(WHITE, BLACK);
    display->print("Saving patch...");
    if(patches->savePatch())
      display->println("Saved");
    else
      display->println("Error!");
    display->display();
    delay(UI_PAUSE);
    return SHOW_WAVE;
  }

  // Check for long press of left encoder to go back
  return checkGotoModule(WAVE_MENU);
}

