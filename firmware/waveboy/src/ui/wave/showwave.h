/* UI Module for displaying and manipulating waves */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../../defines.h"
#include "../../../functions.h"
#include "../ui.h"
#include "../../../patches.h"
#include "../../../gate.h"

class ShowWave: public UI {
  private:
    Patches* patches;
    Gate* gate;
    byte currentWave = 0;
    byte currentStep = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    bool updateDisplayFlag = false;
    void updateDisplay();
    void drawStepCursor();

  public:
    ShowWave(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Gate* gate);
    void activate();
    byte run();
};