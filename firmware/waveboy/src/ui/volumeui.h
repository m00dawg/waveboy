/* Module which shows the main menu */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "../../defines.h"
#include "../../functions.h"
#include "ui.h"
#include "../../volume.h"
#include "../envelopes/none.h"

#define VOL_UI_ADSR_ATTACK 0
#define VOL_UI_ADSR_DECAY 1
#define VOL_UI_ADSR_RELEASE 2
#define VOL_UI_ADSR_SUSTAIN 3

class VolumeUI: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    Volume* volume;
    byte udRotate = 0;
    void drawDisplay();
  public:
    VolumeUI(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Volume* volume);
    void activate();
    byte run();
};