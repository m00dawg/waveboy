/* Tracker Module */
#pragma once
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include <SdFat.h>
#include "../../defines.h"
#include "../../functions.h"
#include "ui.h"

struct Row {
  uint8_t note;
  uint8_t instrument;
  uint8_t volume;
  uint8_t effect_param;
  uint8_t effect_value;
};

struct Pattern {
  Row rows[64];
};

class Tracker: public UI {
  private:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
    SdFs* sd;
    Pattern song[16];
    void printMenu();

  public:
    Tracker(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, SdFs* sd);

    void activate();
		byte run();
};