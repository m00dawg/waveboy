/* Basic ADSR */
#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "envelope.h"
#include "decay.h"

#define ENV_ADSR_ATTACK 0
#define ENV_ADSR_DECAY 1
#define ENV_ADSR_SUSTAIN 2
#define ENV_ADSR_RELEASE 3

class ADSR: public Envelope {
  private:
    char name[16] = "ADSR"; 
    byte state = ENV_ADSR_ATTACK;
    uint16_t value;
    bool sustain = false;

  public:
    // Speed is as it sounds - higher numbers mean faster envelopes
    // (shorter timer)
    byte attackSpeed = 0xF0;
    byte decaySpeed = 0x80;
    byte releaseSpeed = 0x80;
    // Coarser than the actual env range (8-bit vs 16-bit)
    uint16_t sustainValue = 0xA000;
    byte speedMultiplier;

    ADSR(byte speedMultiplier);
    uint16_t start();
    uint16_t next();
    byte getSpeed();
    char* getName();
    void setGate(bool state);
};