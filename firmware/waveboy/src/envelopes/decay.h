/* Basic Decay */
#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "envelope.h"
#include "decay.h"

class Decay: public Envelope {
  private:
    char name[16] = "Decay"; 
		uint16_t value = ENV_MAX;
    

  public:
    // Speed is as it sounds - higher numbers mean faster envelopes
    // (shorter timer)
    byte speed = 0xFF;
    byte speedMultiplier;

    //Decay(byte speedMultiplier);
    Decay(byte speedMultiplier);
		uint16_t start();
    uint16_t next();
    byte getSpeed();
    char* getName();
    void setGate(bool state);
};