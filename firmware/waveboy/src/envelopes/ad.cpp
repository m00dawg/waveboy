/* Basic Attack/Decay */
#include "ad.h"

AD::AD(byte speedMultiplier) { 
	this->speedMultiplier = speedMultiplier;
}

// Start the envelope over from the beginning
uint16_t AD::start() {
	state = AD_ATTACK;
	value = 0;
	return 0;
}

// Return the next value of the envelope
uint16_t AD::next() {
	uint16_t previousValue = value;

	switch (state) {
		case AD_ATTACK:
			value = value + speedMultiplier;
			// Catch rollover
			if(value < previousValue) {
				value = ENV_MAX;
				state = AD_DECAY;
			}
			return value;
		case AD_DECAY:
			value = value - speedMultiplier;
			// Catch rollover
			if(value > previousValue) {
				state = AD_STOP;
				value = 0;
			}
			return value;
		// Stop
		default:
			return 0;
	}
}

// Returns the current speed setting. For some
// envelopes this can change dynamically (and/or can be set by the user)
byte AD::getSpeed() {
	if(state == AD_DECAY)
		return decaySpeed;
	return attackSpeed;
}

char* AD::getName() {
  return name;
}

// Gate isn't used by AD
void AD::setGate(bool state) { }