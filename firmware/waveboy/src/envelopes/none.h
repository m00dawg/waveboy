/* None (External) */
// This just returns zero (no attenuation)
#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "envelope.h"
#include "decay.h"

class None: public Envelope {
  private:
    char name[16] = "None"; 
  public:
    None();
		uint16_t start();
    uint16_t next();
    byte getSpeed();
    char* getName();
    void setGate(bool state);
};