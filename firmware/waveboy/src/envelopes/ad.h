/* Basic AD */
#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "envelope.h"
#include "decay.h"

#define AD_ATTACK 0
#define AD_DECAY 1
#define AD_STOP 2

class AD: public Envelope {
  private:
    char name[16] = "AD"; 
    byte state = AD_ATTACK;
    uint16_t value = 0;

  public:
    // Speed is as it sounds - higher numbers mean faster envelopes
    // (shorter timer)
    byte attackSpeed = 0xFF;
    byte decaySpeed = 0x80;
    byte speedMultiplier;

    AD(byte speedMultiplier);
    uint16_t start();
    uint16_t next();
    byte getSpeed();
    char* getName();
    void setGate(bool state);
};