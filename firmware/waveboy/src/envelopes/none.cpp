/* None */
#include "none.h"

None::None() { }

// Start the envelope over from the beginning
uint16_t None::start() {
	return next();
}

// Return the next value of the envelope
// Since this is the None envelope, return 0
// meaning no attenuation is applied.
uint16_t None::next() {
	return ENV_MAX;
}

// Returns the current speed setting. 
// For none, we return 0 (the slowest speed) since there
// isn't anything for this envelope to do anyway.
byte None::getSpeed() {
	return 0;
}

char* None::getName() {
  return name;
}

// Gate isn't used by None
void None::setGate(bool state) { }