/* Basic ADSR */
#include "adsr.h"

ADSR::ADSR(byte speedMultiplier) {  
	this->speedMultiplier = speedMultiplier;
}

// Start the envelope over from the beginning
uint16_t ADSR::start() {
	state = ENV_ADSR_ATTACK;
	value = 0;
	return value;
}

// Return the next value of the envelope
uint16_t ADSR::next() {
	uint16_t previousValue = value;

	switch (state) {
		case ENV_ADSR_ATTACK:
			// If attack is set to max, skip the attack phase entirely
			if(attackSpeed == ENV_MAX_SPEED) {
				value = ENV_MAX;
				state = ENV_ADSR_DECAY;
				return value;
			}
			value = value + speedMultiplier;
			// Rollover detected
			if(value < previousValue) {
				value = ENV_MAX;
				state = ENV_ADSR_DECAY;
			}
			return value;
		case ENV_ADSR_DECAY:
			value = value - speedMultiplier;
			// Rollover detected
			if(value >= previousValue) {
				value = 0;
			}

			if(value <= sustainValue) {
				state = ENV_ADSR_SUSTAIN;
				value = sustainValue;
				return value;
			}
		case ENV_ADSR_SUSTAIN:
			if(sustain)
				return value;
			state = ENV_ADSR_RELEASE;
			return value;
		// Release
		default:
			if(value == 0)
				return 0;
			value = value - speedMultiplier;
			// Rollover check
			if(value >= previousValue) {
				value = 0;
			}
			return value;
	}
}

// Returns the current speed setting. For some
// envelopes this can change dynamically (and/or can be set by the user)
byte ADSR::getSpeed() {
	switch (state) {
		case ENV_ADSR_ATTACK:
			return attackSpeed;
		case ENV_ADSR_DECAY:
			return decaySpeed;
		case ENV_ADSR_SUSTAIN:
			// Max speed so we can act on trigger quickly
			return ENV_MAX_SPEED;
		// Release
		default:
			return releaseSpeed;
	}
}

char* ADSR::getName() {
  return name;
}

void ADSR::setGate(bool state) { 
	sustain = state;
}