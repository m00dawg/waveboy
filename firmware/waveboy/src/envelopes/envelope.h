/* Interface for Envelopes Types */



#pragma once

#include <Arduino.h>
#include "../../defines.h"

class Envelope {
  public:
    Envelope(){}
    virtual ~Envelope(){}
    virtual char* getName() = 0;
		virtual uint16_t start() = 0;
    virtual uint16_t next() = 0;
    virtual byte getSpeed() = 0;
    virtual void setGate(bool state) = 0;
};
