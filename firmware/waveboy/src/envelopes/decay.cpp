/* Basic Decay */
#include "decay.h"

Decay::Decay(byte speedMultiplier) {  
	this->speedMultiplier = speedMultiplier;
}

// Start the envelope over from the beginning
uint16_t Decay::start() {
	value = ENV_MAX;
	return value;
}

// Return the next value of the envelope
uint16_t Decay::next() {
	// Once we hit zero, stay there.
	if(value == 0)
		return 0;

	uint16_t previousValue = value;
	value = value - speedMultiplier;

	// Stop at zero (and catch a rollover)
	if(value > previousValue)
		value = 0;

	return value;
}

// Returns the current speed setting. For some
// envelopes this can change dynamically (and/or can be set by the user)
byte Decay::getSpeed() {
	return speed;
}

char* Decay::getName() {
  return name;
}

// Gate isn't used by Decay
void Decay::setGate(bool state) { }