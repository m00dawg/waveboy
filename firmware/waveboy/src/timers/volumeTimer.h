/*
	Based on SAMD51_InterruptTimer.h by Dennis van Gils
		(https://github.com/Dennis-van-Gils/SAMD51_InterruptTimer)

  See also: https://emalliab.wordpress.com/2021/04/16/comparing-timers-on-samd21-and-samd51-microcontrollers/

  Uses the TC3 timer of the SAMD51 which calls a interrupt service routine/function (ISR)
  While this library is fairly generic, this one is intended to be used with the Audio output
  module.

  Why not a more generic timer library? Figuring out where the TC#, TCC, etc. objects come from
  has proven rather difficult.
*/

#pragma once
#include <Arduino.h>
#include "../../defines.h"

class VolumeTimer {
  private:
    void waitForSync();
  public:
    void startTimer(byte volumeRate, void (*f)());
    void stopTimer();
    void setVolumeRate(byte volumeRate);
};