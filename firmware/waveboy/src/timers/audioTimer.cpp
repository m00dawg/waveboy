#include "audioTimer.h"

// ISR Pointer
void (*audioISR)();

// Set the next timer from current sampleRate
void AudioTimer::setSampleRate(float sampleRate) {
  int prescaler;
  uint32_t TC_CTRLA_PRESCALER_DIVN;
  //unsigned long period = MICROSECONDS / sampleRate;
  float period = MICROSECONDS / sampleRate;
  //double period = MICROSECONDS / sampleRate;
  //float period = sampleRate;

  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;
  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV1024;
  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV256;
  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV64;
  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV16;
  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV4;
  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV2;
  TC2->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV1;
  waitForSync();

  // Set prescaler to zero in one go.
  /*
  TC2->COUNT16.CTRLA.reg = TC2->COUNT16.CTRLA.reg & 0b11111111111111111111100011111111;
    waitForSync();
    */

  // We know the lowest we can reasonably go, so we can ignore the other scales.
/*
  if (period > 300000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV1024;
    prescaler = 1024;
  } else if (80000 < period && period <= 300000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV256;
    prescaler = 256;
  } else if (20000 < period && period <= 80000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV64;
    prescaler = 64;
  } else if (10000 < period && period <= 20000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV16;
    prescaler = 16;
  } else if (5000 < period && period <= 10000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV8;
    prescaler = 8;
  } else if (2500 < period && period <= 5000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV4;
    prescaler = 4;
    */

  if (period > 2500) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV4;
    prescaler = 4;
  }
  else if (1000 < period && period <= 2500) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV2;
    prescaler = 2;
  } else if (period <= 1000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV1;
    prescaler = 1;
  }
  TC2->COUNT16.CTRLA.reg |= TC_CTRLA_PRESCALER_DIVN;
  //waitForSync();

  // Period itself is samplerate / MICROSECONDS
  // So it's really samplerate / MICROSECONDS /MICROSECONDS ?
  //int compareValue = (int)(GCLK1_HZ / (prescaler/((float)period / MICROSECONDS))) - 1;
  uint16_t compareValue = (uint16_t)(GCLK1_HZ / (prescaler/((float)period / MICROSECONDS))) - 1;

  //int compareValue = (int)(GCLK1_HZ / (1/((float)period / MICROSECONDS))) - 1;
  
  //uint32_t compareValue = (uint32_t)(GCLK1_HZ / (1/((float)period / MICROSECONDS)));
  

  // Make sure the count is in a proportional position to where it was
  // to prevent any jitter or disconnect when changing the compare value.
  TC2->COUNT16.COUNT.reg = map(TC2->COUNT16.COUNT.reg, 0,
                               TC2->COUNT16.CC[0].reg, 0, compareValue);
  TC2->COUNT16.CC[0].reg = compareValue;



  //waitForSync();

  TC2->COUNT16.CTRLA.bit.ENABLE = 1;
  
  waitForSync();
}

void AudioTimer::startTimer(float sampleRate, void (*f)()) {
  // Enable the TC bus clock, use clock generator 3
  GCLK->PCHCTRL[TC2_GCLK_ID].reg = GCLK_PCHCTRL_GEN_GCLK1_Val |
                                   (1 << GCLK_PCHCTRL_CHEN_Pos);
  while (GCLK->SYNCBUSY.reg > 0);

  TC2->COUNT16.CTRLA.bit.ENABLE = 0;

  //TC2->COUNT16.CTRLA.reg |= TC_CTRLA_PRESCALER_DIV1;
  //TC2->COUNT16.CTRLA.reg |= TC_CTRLA_MODE_COUNT16;
  TC2->COUNT16.CTRLA.reg |= TC_CTRLA_MODE_COUNT16;

  // Use match mode so that the timer counter resets when the count matches the
  // compare register
  TC2->COUNT16.WAVE.bit.WAVEGEN = TC_WAVE_WAVEGEN_MFRQ;
  waitForSync();
  
   // Enable the compare interrupt
  TC2->COUNT16.INTENSET.reg = 0;
  TC2->COUNT16.INTENSET.bit.MC0 = 1;

  // Enable IRQ
  NVIC_EnableIRQ(TC2_IRQn);
  //NVIC_EnableIRQ(85);

  audioISR = f;

  setSampleRate(sampleRate);
}

void AudioTimer::stopTimer() {
  TC2->COUNT16.CTRLA.bit.ENABLE = 0;
}

void AudioTimer::waitForSync() {
  while (TC2->COUNT16.SYNCBUSY.reg != 0) {}
}

// WTF this this?! It's magic. That's what.
// This is how the ISR ends up getting called and it seems to be some low-level
// thing that is overloading a function or something. If this isn't here, named
// as TC2_Handler, things will crash. This also isn't actually part of the AudioTimer object.
// This makes me wish I was using RISCV and writing this in assembly...
void TC2_Handler() {
  // If this interrupt is due to the compare register matching the timer count
  if (TC2->COUNT16.INTFLAG.bit.MC0 == 1) {
    TC2->COUNT16.INTFLAG.bit.MC0 = 1;
    (*audioISR)();
  }
}