#include "volumeTimer.h"

// ISR Pointer
void (*volumeISR)();

// Set the next timer from current volumeRate
void VolumeTimer::setVolumeRate(byte volumeRate) {
  int prescaler;
  uint32_t TC_CTRLA_PRESCALER_DIVN;
  //unsigned long period = MICROSECONDS / volumeRate;
  uint16_t period = VOLUME_TIMER_DIVISOR / (volumeRate + 1);

  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;
  waitForSync();
  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV1024;
  waitForSync();
  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV256;
  waitForSync();
  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV64;
  waitForSync();
  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV16;
  waitForSync();
  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV4;
  waitForSync();
  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV2;
  waitForSync();
  TC0->COUNT16.CTRLA.reg &= ~TC_CTRLA_PRESCALER_DIV1;
  waitForSync();

  if (period > 300000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV1024;
    prescaler = 1024;
  } else if (80000 < period && period <= 300000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV256;
    prescaler = 256;
  } else if (20000 < period && period <= 80000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV64;
    prescaler = 64;
  } else if (10000 < period && period <= 20000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV16;
    prescaler = 16;
  } else if (5000 < period && period <= 10000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV8;
    prescaler = 8;
  } else if (2500 < period && period <= 5000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV4;
    prescaler = 4;
  } else if (1000 < period && period <= 2500) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV2;
    prescaler = 2;
  } else if (period <= 1000) {
    TC_CTRLA_PRESCALER_DIVN = TC_CTRLA_PRESCALER_DIV1;
    prescaler = 1;
  }
  TC0->COUNT16.CTRLA.reg |= TC_CTRLA_PRESCALER_DIVN;
  waitForSync();

  int compareValue = (int)(GCLK1_HZ / (prescaler/((float)period / 1000000))) - 1;

  // Make sure the count is in a proportional position to where it was
  // to prevent any jitter or disconnect when changing the compare value.
  TC0->COUNT16.COUNT.reg = map(TC0->COUNT16.COUNT.reg, 0,
                               TC0->COUNT16.CC[0].reg, 0, compareValue);
  TC0->COUNT16.CC[0].reg = compareValue;
  waitForSync();

  TC0->COUNT16.CTRLA.bit.ENABLE = 1;
  waitForSync();
}

void VolumeTimer::startTimer(byte volumeRate, void (*f)()) {
  // Enable the TC bus clock, use clock generator 1
  GCLK->PCHCTRL[TC0_GCLK_ID].reg = GCLK_PCHCTRL_GEN_GCLK1_Val |
                                   (1 << GCLK_PCHCTRL_CHEN_Pos);
  while (GCLK->SYNCBUSY.reg > 0);

  TC0->COUNT16.CTRLA.bit.ENABLE = 0;
  
  // Use match mode so that the timer counter resets when the count matches the
  // compare register
  TC0->COUNT16.WAVE.bit.WAVEGEN = TC_WAVE_WAVEGEN_MFRQ;
  waitForSync();
  
   // Enable the compare interrupt
  TC0->COUNT16.INTENSET.reg = 0;
  TC0->COUNT16.INTENSET.bit.MC0 = 1;

  // Enable IRQ
  NVIC_EnableIRQ(TC0_IRQn);

  volumeISR = f;

  setVolumeRate(volumeRate);
}

void VolumeTimer::stopTimer() {
  TC0->COUNT16.CTRLA.bit.ENABLE = 0;
}

void VolumeTimer::waitForSync() {
  while (TC0->COUNT16.SYNCBUSY.reg != 0) {}
}

// WTF this this?! It's magic. That's what.
// This is how the ISR ends up getting called and it seems to be some low-level
// thing that is overloading a function or something. If this isn't here, named
// as TC0_Handler, things will crash. This also isn't actually part of the VolumeTimer object.
// This makes me wish I was using RISCV and writing this in assembly...
void TC0_Handler() {
  // If this interrupt is due to the compare register matching the timer count
  if (TC0->COUNT16.INTFLAG.bit.MC0 == 1) {
    TC0->COUNT16.INTFLAG.bit.MC0 = 1;
    (*volumeISR)();
  }
}