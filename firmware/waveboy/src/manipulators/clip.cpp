/* Clip */
#include "clip.h"

Clip::Clip() { }

const char* Clip::getName() {
    return name;
}

void Clip::reset() {
    // Noop
}


void Clip::increment1(byte wave[]) {
    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        if(wave[count] < WAVE_CENTER) {
            --wave[count];
            if(wave[count] > WAVE_RANGE_MAX)
                wave[count] = 0;
        }
        else {
            ++wave[count];
            if(wave[count] > WAVE_RANGE_MAX)
                wave[count] = WAVE_RANGE_MAX;
        }
    }
}

void Clip::decrement1(byte wave[]) {
    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        if(wave[count] < WAVE_CENTER) {
            ++wave[count];
            if(wave[count] > WAVE_CENTER)
                wave[count] = WAVE_CENTER;
        }
        else {
            --wave[count];
            if(wave[count] < WAVE_CENTER)
                wave[count] = WAVE_CENTER;
        }
    }
}

void Clip::increment2(byte wave[]) {
    increment1(wave);
}

void Clip::decrement2(byte wave[]) {
    decrement1(wave);
}

/*
void Clip::manipulate(int8_t value1, int8_t value2, byte wave[]) {
    Serial.println(value1);
    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        Serial.printf("Before: %d ", wave[count]);
        //previousValue = wave[count];
        if(wave[count] < WAVE_CENTER) {
            wave[count] = wave[count] - value1;
            if(wave[count] > WAVE_RANGE_MAX)
                wave[count] = 0;
        }
        else {
            wave[count] = wave[count] + value1;
            if(wave[count] < WAVE_CENTER)
                wave[count] = WAVE_RANGE_MAX;
        }
        if[]
        Serial.printf("After: %d\n", wave[count]);
    }
}
*/