/* Amplify-Wrap Manipulator */
// Attenuate or amplify wave. For amplify, wrap when at the wave edges.

#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "manipulator.h"

class Wrap: public Manipulator {
  private:
    const char name[5] = "Wrap";
    bool direction[WAVE_SLOTS];
    bool initial = true;
    void evaluateDirection(byte wave[]);
  public:
    Wrap();

    /* Name of the Manipulator (used by the UI) */
    const char* getName();

    /* Reset the direction */
    void reset();

    /* Manipulate given wave */
    // void manipulate(int8_t value1, int8_t value2, byte wave[]);
    
    void increment1(byte wave[]);
    void decrement1(byte wave[]);
    void increment2(byte wave[]);
    void decrement2(byte wave[]);
};
