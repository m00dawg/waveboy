/* Interface for Manipulators */
// Manipulators are things that mutate a single wave frame

#pragma once

#include <Arduino.h>
#include "../../defines.h"


class Manipulator {
  public:
    Manipulator(){}
    virtual ~Manipulator(){}

    /* Name of the Manipulator (used by the UI) */
    virtual const char* getName() = 0;

    /* Reset to default state, clearing history if needed */
    virtual void reset() = 0;

    /* Return manipulated wave */
    //virtual void manipulate(int8_t value1, int8_t value2, byte wave[]) = 0;

    /* Perform manipulation by 1 */
    virtual void increment1(byte wave[]) = 0;
    virtual void decrement1(byte wave[]) = 0;
    virtual void increment2(byte wave[]) = 0;
    virtual void decrement2(byte wave[]) = 0;

};
