/* Jitter Manipulator */
// Random roll to move value -1, 0, +1

#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "manipulator.h"

class Jitter: public Manipulator {
  private:
    const char name[7] = "Jitter";
    // Number of rolls per jitter
    byte rolls = 1;
  public:
    Jitter();

    /* Name of the Manipulator (used by the UI) */
    const char* getName();

    /* Jitter does not store history, so no op */
    void reset();

    /* Manipulate given wave */
    void increment1(byte wave[]);
    void decrement1(byte wave[]);
    void increment2(byte wave[]);
    void decrement2(byte wave[]);
};
