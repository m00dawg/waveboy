/* Wrap */
#include "wrap.h"

Wrap::Wrap() { }

const char* Wrap::getName() {
    return name;
}

void Wrap::reset() {
    initial = true;
}

/* Set the initial direction based on whether the wave is above or below center 
 * True = up, False = down 
 * (or might be better to say forward and reverse since decrement would flip the logic)
 * 
 * This is used for when we "wrap" which means we need to keep the "momentum" (direction)
 * the same.
*/
void Wrap::evaluateDirection(byte wave[]) {
    // Only evaluate when initial is true
    if(initial) {
        for(byte count = 0; count < WAVE_SLOTS; ++count) {
            if(wave[count] < WAVE_CENTER)
                direction[count] = false;
            else
                direction[count] = true;
        }
        initial = false;
    }
}

void Wrap::increment1(byte wave[]) {
    // If we haven't scanned the wave yet to determine the initial direction
    // do so now and unset the initial flag.
    evaluateDirection(wave);

    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        if(direction[count]) {
            ++wave[count];
            if(wave[count] > WAVE_RANGE_MAX)
                wave[count] = 0;
        }
        else {
            --wave[count];
            if(wave[count] > WAVE_RANGE_MAX)
                wave[count] = WAVE_RANGE_MAX;
        }
    }
}

void Wrap::decrement1(byte wave[]) {
    evaluateDirection(wave);

    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        if(direction[count]) {
            --wave[count];
            if(wave[count] > WAVE_RANGE_MAX)
                wave[count] = WAVE_RANGE_MAX;
        }
        else {
            ++wave[count];
            if(wave[count] > WAVE_RANGE_MAX)
                wave[count] = 0;
        }
    }
}

void Wrap::increment2(byte wave[]) {
    increment1(wave);
}

void Wrap::decrement2(byte wave[]) {
    decrement1(wave);
}