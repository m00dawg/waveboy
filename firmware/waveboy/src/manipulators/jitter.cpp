/* Jitter */
#include "jitter.h"

Jitter::Jitter() { }

const char* Jitter::getName() {
    return name;
}

void Jitter::reset() {
    rolls = 1;
}

void Jitter::increment1(byte wave[]) {
    int8_t rand;
    byte slot;
    byte value;

    // Randomly move a value up to what the rolls value is set to.
    for(byte count = 0; count < rolls; ++count) {
        slot = random(0, WAVE_SLOTS);
        rand = 0;

        // Roll until we get -1 or 1.        
        while(rand == 0)
            rand = random(-1, 2);   // inclusive to exclusive

        value = wave[slot];
        wave[slot] = wave[slot] + rand;

        // If we +1'd the wave, check on if we need to wrap to 0
        if(rand != 0) {
            if(wave[slot] > value && wave[slot] > WAVE_RANGE_MAX)
                wave[slot] = 0;
            // But if we were at 0 and -1's, we go back over and need 
            // to go to the wave max.
            else if(value == 0 && wave[slot] > value)
                wave[slot] = WAVE_RANGE_MAX;
        }
    }
    /*
    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        rand = random(-1, 2);   // inclusive to exclusive
        value = wave[count];
        wave[count] = wave[count] + rand;

        // If we +1'd the wave, check on if we need to wrap to 0
        if(rand != 0) {
            if(wave[count] > value && wave[count] > WAVE_RANGE_MAX)
                wave[count] = 0;
            // But if we were at 0 and -1's, we go back over and need 
            // to go to the wave max.
            else if(value == 0 && wave[count] > value)
                wave[count] = WAVE_RANGE_MAX;
        }
    }
    */
}

void Jitter::decrement1(byte wave[]) {
    increment1(wave);
}

void Jitter::increment2(byte wave[]) {
    ++rolls;
}

void Jitter::decrement2(byte wave[]) {
    --rolls;
    // Catch the rollover to set the floor always to zero
    if(rolls == 255)
        rolls = 0;
}

