/* Clip Manipulator */
// Attenuate or amplify wave. For amplify, clip when at the wave edges.

#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "manipulator.h"

class Clip: public Manipulator {
  private:
    const char name[5] = "Clip";
  public:
    Clip();

    /* Name of the Manipulator (used by the UI) */
    const char* getName();

    /* Clip does not store history, so no op */
    void reset();

    /* Manipulate given wave */
    // void manipulate(int8_t value1, int8_t value2, byte wave[]);
    
    void increment1(byte wave[]);
    void decrement1(byte wave[]);
    void increment2(byte wave[]);
    void decrement2(byte wave[]);
};
