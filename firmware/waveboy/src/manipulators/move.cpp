/* Move */
#include "move.h"

Move::Move() { }

const char* Move::getName() {
    return name;
}

void Move::reset() {
}

/* Move Up */
void Move::increment1(byte wave[]) {
    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        ++wave[count];
        if(wave[count] > WAVE_RANGE_MAX)
            wave[count] = 0;
    }
}

/* Move Down */
void Move::decrement1(byte wave[]) {
    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        --wave[count];
        if(wave[count] > WAVE_RANGE_MAX)
            wave[count] = WAVE_RANGE_MAX;
    }
}

/* Move Right */
// There is a more memory efficient way to do this 
// I think, but this doesn't need to be performant
// so this what we got.
void Move::increment2(byte wave[]) {
    byte countY = WAVE_SLOTS - 1;
    byte originalWave[WAVE_SLOTS];
    memcpy(originalWave, wave, WAVE_SLOTS);

    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        wave[count] = originalWave[countY];
        ++countY;
        if(countY == WAVE_SLOTS)
            countY = 0;
    }
}

/* Move Left */
void Move::decrement2(byte wave[]) {
    byte originalWave[WAVE_SLOTS];
    memcpy(originalWave, wave, WAVE_SLOTS);
    
    for(byte count = 0; count < WAVE_SLOTS; ++count) {
        wave[count] = originalWave[(count + 1) % WAVE_SLOTS];
    }
}