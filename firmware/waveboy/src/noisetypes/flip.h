/* Flip random bits */
#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "noisetype.h"


class Flip: public NoiseType {
  private:
    volatile uint16_t bits = 0;
    char name[16] = "Bitflip"; 

  public:
    Flip();
    uint16_t next();
    uint16_t current();
    void reset();
    void incKnobParam();
    void decKnobParam();
    byte getKnobParam();
    void setCVParam(byte value);
    byte getCVParam();
    void gate();
    char* getName();
};