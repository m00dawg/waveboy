/* Flip random bits */
#include "flip.h"

Flip::Flip() { 
	reset();
}

uint16_t Flip::next() {
	uint16_t rng = random(0, 16);	// Upper bound is _exclusive_
	bits = bits ^ ((uint16_t)1 << rng);
	return bits;
}

// Grab current value
// Used for the step sequencer
uint16_t Flip::current() {
  return bits;
}

// Re-roll the random seed
void Flip::reset() { 
	bits = rand();
}

// Increase parameter - this has no effect on this noisemaker
// So this does nothing.
void Flip::incKnobParam() { }

// Decrease parameter - this has no effect on this noisemaker
// So this does nothing.
void Flip::decKnobParam() { }

// Return parameter - 0 as there is no param for this noisemaker
byte Flip::getKnobParam() {
	return 0;
}

// Set CV parameter - this has no effect on this noisemaker
// So this does nothing.
void Flip::setCVParam(byte value) { }

// Set CV parameter - this has no effect on this noisemaker
// So return 0
byte Flip::getCVParam() {
	return 0;
}

// For this noise type, the gate doesn't do anything.
void Flip::gate() { }

char* Flip::getName() {
  return name;
}
