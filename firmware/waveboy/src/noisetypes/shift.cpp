/* Shift bits */
#include "shift.h"

Shift::Shift() { 
	// can't reset here because the ADC isn't setup when object is created
	//reset();
}

// See: https://blog.regehr.org/archives/1063
uint16_t Shift::next() {
	uint16_t roll = random(0x0000, 0x0002);	// Upper bound is _exclusive_
	//bits = (bits << 1) | (bits >> 15);
	bits = (bits << 1) | roll;
	return bits;
}

// Grab current value
// Used for the step sequencer
uint16_t Shift::current() {
  return bits;
}

// Re-roll the random seed
void Shift::reset() { 
	randomSeed(readADC0(RANDOM_SOURCE));
	bits = random(0x0000, 0xffff);
}

// Increase parameter - this has no effect on this noisemaker
// So this does nothing.
void Shift::incKnobParam() { }

// Decrease parameter - this has no effect on this noisemaker
// So this does nothing.
void Shift::decKnobParam() { }

// Return parameter - 0 as there is no param for this noisemaker
byte Shift::getKnobParam() {
	return 0;
}

// Set CV parameter - this has no effect on this noisemaker
// So this does nothing.
void Shift::setCVParam(byte value) { }

// Set CV parameter - this has no effect on this noisemaker
// So return 0
byte Shift::getCVParam() {
	return 0;
}

// For this noise type, the gate doesn't do anything.
void Shift::gate() { }

char* Shift::getName() {
  return name;
}
