/* Arduino built in Random */
#include "rng.h"

RNG::RNG() { }

/* From: https://aselker.github.io/gameboy-sound-chip/ 
 * At least as best as I understand it.
 */
uint16_t RNG::next() {
	rng = (uint16_t)random();
	return rng;
}

// Grab current value (without updating lfsr)
// Used for the step sequencer
uint16_t RNG::current() {
  return rng;
}

// Re-roll the random seed
void RNG::reset() { 
//	randomSeed(readADC0(RANDOM_SOURCE));
}

// Increase parameter - this has no effect on this noisemaker
// So this does nothing.
void RNG::incKnobParam() { }

// Decrease parameter - this has no effect on this noisemaker
// So this does nothing.
void RNG::decKnobParam() { }

// Return parameter - 0 as there is no param for this noisemaker
byte RNG::getKnobParam() {
	return 0;
}

// Set CV parameter - this has no effect on this noisemaker
// So this does nothing.
void RNG::setCVParam(byte value) { }

// Set CV parameter - this has no effect on this noisemaker
// So return 0
byte RNG::getCVParam() {
	return 0;
}

// For this noise type, the gate doesn't do anything.
void RNG::gate() { }

char* RNG::getName() {
  return name;
}
