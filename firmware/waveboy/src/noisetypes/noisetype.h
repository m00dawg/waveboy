#pragma once
///* Interface for Noise Types */
#include <Arduino.h>
#include "../../defines.h"

class NoiseType {
  public:
    NoiseType(){};
    virtual ~NoiseType(){}
    virtual uint16_t next() = 0;
    virtual uint16_t current() = 0;
    virtual void reset() = 0;
    virtual void incKnobParam() = 0;
    virtual void decKnobParam() = 0;
    virtual byte getKnobParam() = 0;
    virtual void setCVParam(byte value) = 0;
    virtual byte getCVParam() = 0;
    virtual void gate() = 0;
    virtual char* getName() = 0;
    //virtual bool processCV(); = 0;
    //virtual bool processKnob(); = 0;
};
