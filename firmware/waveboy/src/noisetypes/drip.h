/* "Drips" onto bits which spread out */
#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "noisetype.h"

#define DRIP_MAX_BIT_POSITION 15


class Drip: public NoiseType {
  private:
		byte dripPosition = 0;	// Bit position of the drip
		byte dripChance = 9;		// Dice (1dX) size of sorts for the change a new drip happens
		byte splashCount = 255; 	// Current position to left/right of drip
		byte splashMax = 16;			// How many bit positions to "splash"
		byte splashLeft = 0;	  // Actual bit position of left splash (it can wrap)	
		byte splashRight = 0;   // Actual bit position of right splash (it can wrap)
    volatile uint16_t bits = 0;
    char name[16] = "Drip"; 
		void findNeighbors();

  public:
    Drip();
    uint16_t next();
    uint16_t current();
    void reset();
    void incKnobParam();
    void decKnobParam();
    byte getKnobParam();
    void setCVParam(byte value);
    byte getCVParam();
    void gate();
    char* getName();
};