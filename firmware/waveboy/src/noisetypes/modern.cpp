/* GameBoy Buzzy Noise LFSR */
#include "modern.h"

Modern::Modern() {
	lfsr = random();
}

/* From: https://en.wikipedia.org/wiki/Linear-feedback_shift_register */
uint16_t Modern::next() {
  bit = (lfsr ^ (lfsr >> 1) ^ (lfsr >> 3) ^ (lfsr >> 12)) & 1;
  lfsr = (lfsr >> 1) | (bit << 15);
  return lfsr;
}

// Grab current value (without updating lfsr)
// Used for the step sequencer
uint16_t Modern::current() {
  return lfsr;
}

// Re-roll the random seed
void Modern::reset() {
  lfsr = random();
}

// Increase parameter - this has no effect on this noisemaker
// So this does nothing.
void Modern::incKnobParam() { }

// Decrease parameter - this has no effect on this noisemaker
// So this does nothing.
void Modern::decKnobParam() { }

// Return parameter - 0 as there is no param for this noisemaker
byte Modern::getKnobParam() {
	return 0;
}

// Set CV parameter - this has no effect on this noisemaker
// So this does nothing.
void Modern::setCVParam(byte value) { }

// Set CV parameter - this has no effect on this noisemaker
// So return 0
byte Modern::getCVParam() {
	return 0;
}

// For this noise type, the gate doesn't do anything.
void Modern::gate() { }

char* Modern::getName() {
  return name;
}
