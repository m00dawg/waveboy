/* "Drips" onto bits which spread out */
#include "drip.h"

Drip::Drip() { 
	reset();
}

// Find neighbors from the original drip
// or the neighbor's neighbor's
void Drip::findNeighbors() {
	--splashLeft;
	++splashRight;

	// If left is greater than our max, we went past zero (wrap)
	if(splashLeft > DRIP_MAX_BIT_POSITION)
		splashLeft = 0;
	// If right is greater than our max, we need to wrap
	if(splashRight > DRIP_MAX_BIT_POSITION)
		splashRight = 0;
}

uint16_t Drip::next() {
	// If splash count and max equal, roll to see if we do another drop
	if(splashCount >= splashMax)
	{
		// Removed until we have a means to control 2 params
		// Roll a dice to see if a new drip happens
		if(random(1, dripChance) == 1)
		{
			// If so, pick a random bit position
			// and reset for a new drip sequence
			dripPosition = random(0, DRIP_MAX_BIT_POSITION);
			splashCount = 0;
			splashLeft = dripPosition;
			splashRight = dripPosition;
			findNeighbors();
			bits = bits ^ ((uint16_t)1 << dripPosition);
			return bits;
		}
		// If we didn't roll a 1, return current state
		else
			return bits;
	}

	// Flip splashed bits
	++splashCount;
	findNeighbors();
	// These could probably be combined into 1 statement
	bits = bits ^ ((uint16_t)1 << splashLeft);
	bits = bits ^ ((uint16_t)1<< splashRight);
	return bits;
}

// Grab current value
// Used for the step sequencer
uint16_t Drip::current() {
  return bits;
}

// Clean bits so drip has a blank slate
void Drip::reset() { 
	bits = 0;
	splashLeft = 0;
	splashRight = 0;
	splashCount = 255;
}

// Increase drip chance
void Drip::incKnobParam() { 
	++dripChance;
}

// Decrease drip chance
void Drip::decKnobParam() { 
	--dripChance;
}

byte Drip::getKnobParam() {
	return dripChance;
}

// Set CV parameter - this sets the splash max
void Drip::setCVParam(byte value) { 
	splashMax = value;
}

// Return CV parameter
byte Drip::getCVParam() { 
	return splashMax;
}

// For this noise type, the gate doesn't do anything.
void Drip::gate() { }

char* Drip::getName() {
  return name;
}
