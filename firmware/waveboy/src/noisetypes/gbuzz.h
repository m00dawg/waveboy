/* GameBoy Buzzy Noise LFSR */
#pragma once

#include <Arduino.h>
#include "../../defines.h"
#include "noisetype.h"


class GBuzz: public NoiseType {
  private:
    volatile byte bit = 0;
    volatile uint16_t lfsr = 0;
    char name[16] = "GameBoy"; 

  public:
    GBuzz();
    uint16_t next();
    uint16_t current();
    void reset();
    void incKnobParam();
    void decKnobParam();
    byte getKnobParam();
    void setCVParam(byte value);
    byte getCVParam();
    void gate();
    char* getName();
};