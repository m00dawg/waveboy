/* GameBoy Buzzy Noise LFSR */
#include "gbuzz.h"

GBuzz::GBuzz() {
	lfsr = rand();
}

/* From: https://aselker.github.io/gameboy-sound-chip/ 
 * At least as best as I understand it.
 */
uint16_t GBuzz::next() {
  bit = (lfsr >> 2) ^ lfsr;
  lfsr = (lfsr >> 1) | (bit << 15);
  return lfsr;
}

// Grab current value (without updating lfsr)
// Used for the step sequencer
uint16_t GBuzz::current() {
  return lfsr;
}

// Re-roll the random seed
void GBuzz::reset() {
  lfsr = rand();
}

// Increase parameter - this has no effect on this noisemaker
// So this does nothing.
void GBuzz::incKnobParam() { }

// Decrease parameter - this has no effect on this noisemaker
// So this does nothing.
void GBuzz::decKnobParam() { }

// Return parameter - 0 as there is no param for this noisemaker
byte GBuzz::getKnobParam() {
	return 0;
}

// Set CV parameter - this has no effect on this noisemaker
// So this does nothing.
void GBuzz::setCVParam(byte value) { }

// Set CV parameter - this has no effect on this noisemaker
// So return 0
byte GBuzz::getCVParam() {
	return 0;
}

// When the gate triggers, re-roll the seed
void GBuzz::gate() { 
  reset();
}

char* GBuzz::getName() {
  return name;
}
