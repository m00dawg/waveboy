// Gate functions
#include "gate.h"

Gate::Gate(Patches* patches, Noise* noise, Sampler* sampler, Volume* volume) 
{
  this->patches = patches;
  this->noise = noise;
  this->sampler = sampler;
  this->volume = volume;
}

// This is now a gate, which will be important for the sampler
// and envelopes
void Gate::isr() {
//  noInterrupts();

  if (!digitalRead(TRIGGER) && !trigger)
  {
    trigger = true;
    volume->setGate(trigger);
    volume->start();

    switch (mode) {
      case GATE_MODE_WAVE:
        switch (direction) 
        {
          // Forward
          case 'F':
            currentStep = ++currentStep % WAVE_SLOTS;
            //Serial.print("Current Step Debug: ");
            //Serial.println(currentStep);
            break;
          // Backward (Reverse)
          case 'B':
            currentStep = --currentStep % WAVE_SLOTS;
            if(currentStep == 255)
              currentStep = WAVE_SLOTS;
            break;
          // Ping-Pong
          case 'P':
            // Forward
            if(pingPongDirection) {
              currentStep = ++currentStep;
              if(currentStep == WAVE_SLOTS)
                pingPongDirection = false;
            }
            // Backward
            else {
              currentStep = --currentStep;
              
              if(currentStep == 0)
                pingPongDirection = true;
            }
            break;
          // Random
          case 'R':
            currentStep = random(0, WAVE_SLOTS);
        }
        // Magic writing to the DAC
        // See https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
        // To save cycles, writing to the step output is best effort, so we don't wait
        // This is to help avoid audio artifacts on the main audio out
        //while (DAC->SYNCBUSY.bit.DATA1);
        DAC->DATA[1].reg = patches->currentWave[currentStep];
        return;

      case GATE_MODE_SAMPLER:
        sampler->start();
        return;
    
      case GATE_MODE_NOISE: 
        // If we're in Noise mode, grab the current value
        // and also fire an event to the current noise type in case it needs to do anything.
        noise->gate();
        while (DAC->SYNCBUSY.bit.DATA1);
        DAC->DATA[1].reg = noise->current();
        return;

      // Default being gate is disabled.
      default:
        return;
    }
  }
  else if (digitalRead(TRIGGER) && trigger) {
    trigger = false;
    volume->setGate(trigger);
    if (mode == GATE_MODE_SAMPLER)
      sampler->stop();
  } 

}

void Gate::setStepDirection(char direction) {
  this->direction = direction;
}

char Gate::cycleStepDirection() {
  switch (direction) {
    // Forward
    case 'F':
      direction = 'B';
      break;
    case 'B':
      direction = 'P';
      break;
    case 'P':
      direction = 'R';
      break;
    case 'R':
      direction = 'F';
  }
  patches->updateStepDirection(direction);
  return direction;
}