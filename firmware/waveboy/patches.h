#pragma once
#include <Arduino.h>
#include <SdFat.h>
#include "defines.h"
#include "functions.h"

// Now the name of the patch is the filename before .txt
//#define PATCHES_FILE_NAME "patches.txt" // Filename for patch storage in SPI flash

#define PATCH_CHAR_MIN    32            // Min ASCII value we allow in a patch name
#define PATCH_CHAR_MAX    122           // Max ASCII value we allow in a patch name

// Struct for a single patch
struct Patch {
  //char name[PATCH_NAME_LENGTH];
  String name;
  char stepDirection;
  byte waves[NUM_WAVES][WAVE_SLOTS];
};

class Patches {
  private:
    SdFs* sd;
    FsFile patchFile;
    byte frameCV = 0;
    // Reset patch to default values (useful to have a blank say if a patch fails to load off the SD card)
    void initPatch();
    // Scale given byte up to the range of the audio DAC
    uint16_t scaleValue(byte value);


  public:
    //volatile uint16_t scaledWaves[WAVE_FRAMES][WAVE_SLOTS];
    Patch patch;
    byte currentWaveNumber = 0;
    
    // Buffer for the current frame. This is here because we may need to manipulate
    // the wave (e.g. volume envelope) while still being able to recall the 
    // unmodified version
    uint16_t currentWave[WAVE_SLOTS];
    
    // Constructor
    Patches(SdFs* sd);

    // Evaluate CV input and see if we need to update the wave-frame
    void checkCV();

    // Set the frame to a specific value (used when editing waves)
    void changeFrame(byte frame);
  
    // Load patch from SD card into RAM
    //bool loadPatch(char patchName[PATCH_NAME_LENGTH]);
    bool loadPatch(String patchName);

    // Save patch from RAM onto SD card
    bool savePatch();

    // Update the current patch's step direction
    void updateStepDirection(char direction);

    // Increment value at given wave position by one, wrapping around
    void incWaveValue(byte position);

    // Decrement value at given wave position by one, wrapping around
    void decWaveValue(byte position);

    // Update the current wave buffer from currently selected patch/wave
    // scale and apply volume
    void updateWave();

    // Initialize/Begin
    void begin();
};


