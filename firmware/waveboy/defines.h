// Version
#define VERSION "0.55"

// Board Variant
//#define PRERELEASE 1
#define PRODUCTION 1        // WaveBoy PCB 1.31

// Debug
// #define DEBUG 1

// Audio Filter Methods
//#define NO_FILTER 1
//#define MEDIAN 1
#define TWO_POLE 1

// Universal Constants
#define MICROSECONDS 1000000
#define BYTE_MAX 255
#define UINT16T_MAX 65535

// Pins
#define SD_CARD 2
#define RANDOM_SOURCE 4
#define RGB_LED_CLOCKPIN   6
#define RGB_LED_DATAPIN    8
#define ENCODER_UP 9
#define ENCODER_DOWN 7
#define ENCODER_LEFT 12

// The pre-release used a different pin for the right encoder
// Pre-Release
#ifdef PRERELEASE
    #define ENCODER_RIGHT 13
#endif
// Production
#ifdef PRODUCTION
    #define ENCODER_RIGHT 0
#endif

#define BUTTON_MENU 10
#define BUTTON_BACK 11

#define TRIGGER A5

// DACs
#define AUDIO_DAC A0
#define STEP_DAC A1
// ADCs
#define VOCT A2
#define FRAME A3
#define TUNE A4

// Devices
#define OLED_SCREEN_X   128
#define OLED_SCREEN_Y   64
#define OLED_RESET      -1    // No reset pin

// https://github.com/adafruit/Adafruit_SSD1306/blob/master/Adafruit_SSD1306.cpp
// 32-bit UCs and many OLEDs can run faster, so let's go faster.
#define OLED_CLOCK      1000000     // (1MHz)
//#define OLED_CLOCK       400000 // 400Khz (Spec)
#define SCREEN_ADDRESS  0x3C  // Found on the device

//SD Card
#define SPI_DRIVER_SELECT 0

// Timers
// From: https://github.com/Dennis-van-Gils/SAMD51_InterruptTimer/blob/master/SAMD51_InterruptTimer.cpp#L12
//#define GCLK1_HZ 48000000
#define GCLK1_HZ (48000000UL)
//#define GCLK1_HZ 12000000
//#define TIMER_PRESCALER_DIV 1024

// How often to update CVs, Pitch and things
//#define UPDATE_FREQUENCY                1       // Hz
//#define UPDATE_FREQUENCY                24       // Hz
//#define UPDATE_FREQUENCY                50       // Hz

//#define UPDATE_FREQUENCY                4096       // Hz
//#define UPDATE_FREQUENCY                8192       // Hz
//#define UPDATE_FREQUENCY                3072       // Hz
//#define UPDATE_FREQUENCY                2048       // Hz
#define UPDATE_FREQUENCY                400       // Hz
//#define UPDATE_FREQUENCY                12000       // 12kHz
//#define UPDATE_FREQUENCY                20000       // 20kHz


#define PATCH_NAME_LENGTH 16            // Max name of patches (limited by display)


// Noise
#define NOISE_NAME_LENGTH	16

// Config
#define CONFIG_FILE_NAME "config.txt"   // Filename for patch storage in SPI flash
#define TUNING_FILE_NAME "tuning.txt"   // Stores pitch/tune data

// Audio Out
#define DAC_RESOLUTION        		 12       // 12-bit
#define AUDIO_DAC_MAX         		 4095     // 12-bit
//#define AUDIO_DAC_CENTER             AUDIO_DAC_MAX / 2
#define AUDIO_DAC_CENTER             2048
#define AUDIO_DAC_INIT        		 128      // Magic # to init the DAC for fancy writes, I think?  

// V/Oct
//#define PITCH_POLLING_FREQUENCY      60       // Hz
//#define PITCH_POLLING_FREQUENCY      50       // Hz
#define ADC_RESOLUTION        		 12       // 12-bit

//#define ADC_RESOLUTION        		 16       // 12-bit
#define ADC_MAX               		 4095     // 12-bit

//#define TUNE_RESOLUTION       		 170      // 2 Octaves
//#define TUNE_RESOLUTION       		 341.3

#define TUNE_RESOLUTION       		 170.66      // (ADC Range (4096) / Note Range, 2 Octaves


#define TUNE_NOTE_OFFSET             18         // How many notes to add as the min tuning

//#define TUNE_NOTE_OFFSET             6         // How many notes to add as the min tuning

//#define TUNE_RESOLUTION       		 128      // (ADC Range (4096) / Note Range, 3 Octaves
//#define VOCT_NOTE_RANGE       		 60   // 5 octaves (60 notes)
//#define VOCT_STEPS_PER_NOTE   		 68.2666  // ADC Range (4096) / 60 notes
//Moved to configurable variable (instead of tuning adj)
//#define VOCT_STEPS_PER_NOTE   		 70.62  // ADC Range (4096) / 58 notes
//#define VOCT_STEPS_PER_NOTE   		 67.14754  // ADC Range (4096) / 61 notes
//#define TUNING_ADJUSTMENT_INTERVAL   0.0001   // Tuning adjustment when making on module changes
#define STEPS_PER_NOTE_ADJUSTMENT_INTERVAL   0.05  // Tuning adjustment when making on module changes
//#define STEPS_PER_NOTE_ADJUSTMENT_INTERVAL   0.1   // Tuning adjustment when making on module changes
//#define STEPS_PER_NOTE_ADJUSTMENT_INTERVAL   0.001   // Tuning adjustment when making on module changes

// Envelopes
#define NUMBER_OF_ENVELOPE_TYPES 		4
#define NONE_ENVELOPE         			0
#define DECAY_ENVELOPE         			1
#define AD_ENVELOPE         			2
#define ADSR_ENVELOPE                   3

// #define ENV_MAX						    AUDIO_DAC_CENTER // Since waves are signed but stored as unsigned
// For generic envs, we should give it the full 16-bit range and map down as needed (e.g. for volume)
#define ENV_MAX                         UINT16T_MAX
#define ENV_MAX_SPEED                   BYTE_MAX

// Helps give a reasonable range for the envs over 1 byte (from several seconds to chirps)
// Lower values = faster
#define VOLUME_TIMER_DIVISOR                30000
// Higher values = faster
#define VOLUME_ENV_DEFAULT_SPEED_MULTIPLIER 64

// For doing bit-shifts to scale up/down from 16-bit to 8-bit
// (e.g. for the volume map)
#define ENV_TO_VOL_RANGE                8
#define VOLUME_ENVELOPE_MAX             AUDIO_DAC_CENTER

// Frame CV
// Need to add an option to make this bipolar
//#define FRAME_ADC_MAX         2048    // Positive Only
//#define FRAME_ADC_MIN          128
#define FRAME_ADC_MAX         1984    // Positive Only
#define FRAME_ADC_MIN          192

// Waves
#define WAVES_DIRECTORY     "/waves"
#define WAVE_SLOTS        32             // Number of samples per wave
#define WAVE_RESOLUTION   4              // Wave bits (4-bit)
#define WAVE_RANGE_MAX    0xF            // Max wave range (4-bit)
#define WAVE_CENTER       0x8            // Center-ish of wave
#define NUM_WAVES         16             // Number of Frames (waves) per patch
#define MAX_WAVE          NUM_WAVES - 1 // 0 to WAVE_FRAMES - 1

// Samples
#define SAMPLE_NAME_LENGTH 16

// UI Module Mappings
#define NUM_UI_MODULES          20
#define SHOW_WAVE 	            0
#define MAIN_MENU 	            1
#define WAVE_MENU		        2
#define WAVE_LOAD_PATCH 	    3
#define WAVE_SAVE_PATCH 	    4
#define BLANK 			        5
#define CONFIG_MENU             6
#define NOISE_MODULE	        7
#define SAMPLER_MODULE          8
#define TRACKER_MODULE          9
#define VOLUME_MODULE           10
#define VOLUME_NONE_MODULE      11
#define VOLUME_DECAY_MODULE     12
#define VOLUME_AD_MODULE        13
#define VOLUME_ADSR_MODULE      14
#define EDIT_WAVE               15
#define TUNING_MODULE           16
#define WAVE_MANIPULATORS       17
#define MANIPULATE_WAVE         18
#define SAMPLER_PATCH_MODULE    19

#define UI_PAUSE	1500 //ms

// Buttons
#define BUTTON_DEBOUNCE      20    //ms
#define ENCODER_DEBOUNCE     2     //ms
#define ENCODER_ERROR_DELAY  50   //ms
#define ENCODER_TURNED_LEFT  1
#define ENCODER_TURNED_RIGHT 2
#define BUTTON_LONG_PRESS    500 	// ms

// Characters
#define CHAR_SPACE          32
#define CHAR_BANG           33
#define CHAR_SLASH          48
#define CHAR_ZERO           48
#define CHAR_NINE           57
#define CHAR_COLON          58
#define CHAR_AT             64
#define CHAR_UPPER_A        65
#define CHAR_UPPER_Z        90
#define CHAR_LEFT_BRACKET   91
#define CHAR_BACKTICK       96
#define CHAR_LOWER_A        97
#define CHAR_LOWER_Z        122

/* Global Variables */
// Byte to track which module we are in
extern byte currentModuleId;
// Byte to track previous module (so we can go "backwards" in the UI)
extern byte previousModuleId;

