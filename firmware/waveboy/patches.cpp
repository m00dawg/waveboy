#include "patches.h"

Patches::Patches(SdFs* sd) { 
  this->sd = sd;
  initPatch();
}

// Private Methods
void Patches::initPatch() {
  for (byte count = 0; count < PATCH_NAME_LENGTH; ++count) {
    // Prefill with null terminators.
    // This is so if a user creates a new patch, the resulting name
    // will already be null terminated.
    patch.name[count] = '\0';
  }
  patch.stepDirection = 'F';
  for (byte frameCount = 0; frameCount < NUM_WAVES; ++frameCount)  
    for (byte waveSlotCount = 0 ; waveSlotCount < WAVE_SLOTS ; waveSlotCount++)
        patch.waves[frameCount][waveSlotCount] = WAVE_CENTER;
}

// Scale the given value up to the AUDIO_DAC resolution
// by bit-shifting and placing in our waves buffer.
uint16_t Patches::scaleValue(byte value) {
  return map(value, 0, WAVE_RANGE_MAX, 0, AUDIO_DAC_MAX);
}

// Update the current wave buffer from currently selected patch/wave
// scale and apply volume
void Patches::updateWave() {
  for (byte waveSlotCount = 0 ; waveSlotCount < WAVE_SLOTS ; ++waveSlotCount) {
    currentWave[waveSlotCount] = scaleValue(patch.waves[currentWaveNumber][waveSlotCount]);
  }
}

// Public Methods

// Load a given patch file from SD
bool Patches::loadPatch(String patchName) {
  FsFile patchFile;
  byte unscaledValue;

  sd->chdir();
  sd->chdir(WAVES_DIRECTORY);

  // Assign the patch name to the current patch
  /*
  for (byte count = 0; count < PATCH_NAME_LENGTH; ++count) {
    if(patchName[count] == '\0' || patchName[count] == '\n' || patchName[count] == 0)
      patch.name[count] = '\0';
    else
      patch.name[count] = patchName[count];
  }
  */
  patch.name = patchName;

  patchFile = sd->open(patch.name, FILE_READ);
  if(!patchFile) {
    Serial.println("Patch not found");
    Serial.print("Patch Name: ");
    Serial.println(patch.name);
    initPatch();
    return false;
  }

  // First byte is loop type for step output
  patch.stepDirection = patchFile.read();
  patchFile.read(); // Newline

  // Then the next 16 lines are each of the wave frames
  // and scale them up to the audio DAC range
  for (byte waveCount = 0; waveCount < NUM_WAVES; ++waveCount) {
    for (byte waveSlotCount = 0 ; waveSlotCount < WAVE_SLOTS ; ++waveSlotCount) {
      patch.waves[waveCount][waveSlotCount] = charToHex(patchFile.read());
    }
    patchFile.read(); // Newline
  }
  patchFile.close();

  updateWave();
  return true;
}

// Save patch from RAM onto SD card
bool Patches::savePatch() {
  FsFile patchFile;
  byte unscaledValue;
  
  
  //Add null terminator
  for (byte count ; count < PATCH_NAME_LENGTH ; ++count)
  {
    if(patch.name[count] == 0) {
      patch.name[count] = '\0';
      break;
    }
  }

  sd->chdir();
  sd->chdir(WAVES_DIRECTORY);

  Serial.print("Patch name: ");
  Serial.println(patch.name);

  // Remove file first if exists.
  // This is because FILE_WRITE will append by default
  // and we want to overwrite.
  sd->remove(patch.name);

  patchFile = sd->open(patch.name, FILE_WRITE);
  if(!patchFile) {
    return false;
  }

  // First byte is loop type for step output
  patchFile.print(patch.stepDirection);
  // We use this over println because we use Unix terminated files
  patchFile.print('\n'); // Newline

  // Then the next 16 lines are each of the wave frames
  for (byte waveCount = 0; waveCount < NUM_WAVES; ++waveCount) {
    for (byte waveSlotCount = 0 ; waveSlotCount < WAVE_SLOTS ; ++waveSlotCount) {
      patchFile.print(hexToChar(patch.waves[waveCount][waveSlotCount]));
    }
    patchFile.print('\n'); // Newline
  }

  patchFile.close();
  return true;
}

void Patches::checkCV() {
  // Only grab positive CV values (need to make that configurable)

  int cv = readADC0(FRAME);
  byte frameCV;
  if(cv <= FRAME_ADC_MAX) {
    cv = constrain(cv, FRAME_ADC_MIN, FRAME_ADC_MAX);
    frameCV = map(cv, FRAME_ADC_MAX, FRAME_ADC_MIN, 0, MAX_WAVE);
    if(frameCV != currentWaveNumber) {
      currentWaveNumber = frameCV;
      updateWave();
    }
  }
}

void Patches::changeFrame(byte frame) {
  if(frame > NUM_WAVES)
    frame = NUM_WAVES;
  currentWaveNumber = frame;
  updateWave();
}

// Update current patch's step direction
void Patches::updateStepDirection(char direction) {
  patch.stepDirection = direction;
}

// Increment value at given wave position by one, wrapping around
void Patches::incWaveValue(byte position) {
  patch.waves[currentWaveNumber][position] = 
    ++patch.waves[currentWaveNumber][position] % (WAVE_RANGE_MAX + 1);
  updateWave();
}

// Decrement value at given wave position by one, wrapping around
void Patches::decWaveValue(byte position) {
  patch.waves[currentWaveNumber][position] = 
    --patch.waves[currentWaveNumber][position] % (WAVE_RANGE_MAX + 1);
  updateWave();
}
