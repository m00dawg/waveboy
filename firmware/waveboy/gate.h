// Gate
#pragma once
#include <Arduino.h>
#include "patches.h"
#include "src/ui/noise.h"
#include "sampler.h"
#include "volume.h"

#define GATE_MODE_OFF 0
#define GATE_MODE_WAVE 0
#define GATE_MODE_NOISE 1
#define GATE_MODE_SAMPLER 2

class Gate {
  private:
    Patches* patches;
    Noise* noise;
    Sampler* sampler;
    Volume* volume;

    bool pingPongDirection = true;
    
  public:
    bool trigger = false;
    byte currentStep = 0;
    char direction = 'F';
    byte mode = GATE_MODE_WAVE;  // Wave mode default

    // Constructor
    Gate(Patches* patches, Noise* noise, Sampler* sampler, Volume* volume);

    // Check for trigger and update step sequence
    void update();

    // Set the step direction
    void setStepDirection(char direction);

    // Cycle through the step direction options and return the chosen one
    char cycleStepDirection();
    void isr();
  
};

