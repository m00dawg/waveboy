/* Load and save configuration options from the file on the SD card */
#pragma once
#include <Arduino.h>
#include <SimpleRotary.h>
#include <Adafruit_SSD1306.h>
#include "defines.h"

extern SimpleRotary upDownEncoder;
extern Adafruit_SSD1306 display;

// Convert hex character to int
byte charToHex(char chr);

// Convert byte to hex character
char hexToChar(byte value);

// Constrain characters to alphanumeric
char characterLookup(byte char_value);

bool compareCharArray(char *arr1, char *arr2, byte length);

byte checkGotoModule(byte module);
byte checkBackToPreviousModule();

// Display drawing helpers
void drawPixel(byte x, byte y);
void drawWave(byte wave[WAVE_SLOTS]);

// ADC fuctions
uint16_t readADC0(byte pin);
// Nothing seems to be connected to ADC1 that we use.
//uint16_t readADC1(byte pin);